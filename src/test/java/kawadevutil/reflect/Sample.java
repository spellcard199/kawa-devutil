/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.reflect;

public class Sample {
    private static String privateStaticField = "privateStaticString";
    private String privateInstanceField = "privateInstanceString";
}
