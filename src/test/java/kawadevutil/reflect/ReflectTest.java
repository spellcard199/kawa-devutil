/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.reflect;


import org.testng.annotations.Test;

public class ReflectTest {

    @Test
    public void testGetDeclaredFieldInstance() {
        // Arrange
        Sample sample = new Sample();
        String expected = "privateInstanceString";
        // Act
        String actual = null;
        try {
            actual = new Reflecting<String>().getDeclaredFieldValue(sample, "privateInstanceField");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new Error(e);
        }
        // Assert
        org.testng.Assert.assertEquals(actual, expected);
    }

    @Test
    public void testClsGetDeclaredFieldStatic() {
        // Arrange
        String expected = "privateStaticString";
        // Act
        String actual = null;
        try {
            actual = new Reflecting<String>().getDeclaredFieldValue(Sample.class, "privateStaticField");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new Error(e);
        }
        // Assert
        org.testng.Assert.assertEquals(actual, expected);
    }
}
