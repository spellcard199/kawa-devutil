/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.eval;

import gnu.mapping.Environment;
import kawa.standard.Scheme;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class EvalTest {

    @Test
    public void evalToStringTest() {
        // Arrange
        Eval eval = new Eval();
        gnu.expr.Language lang = kawa.standard.Scheme.getInstance();
        gnu.mapping.Environment env = lang.getEnvironment();
        String code = "(+ 1 1)";
        String expected = "2";
        EvalResult actual = null;
        // Act
        try {
            actual = eval.eval(lang, env, code);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        assert actual != null;
        assertEquals(actual.getResultAsString(false), expected);
    }

    @Test
    public void evalSupplierCatchingOutErrTest() {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        String codeStr = "(display (+ 1 1))";

        Eval evaluator = new Eval();
        EvalResultAndOutput resAndOut =
                evaluator.evalSupplierCatchingOutErr(
                        () -> evaluator.eval(scheme, env, codeStr)
                );

        String outErrStr = resAndOut.getOutErr().getOutAndErrInPrintOrder();

        assertEquals(outErrStr, "2");
    }
}
