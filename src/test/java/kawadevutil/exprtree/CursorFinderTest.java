/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.exprtree;

import gnu.expr.ModuleExp;
import gnu.expr.QuoteExp;
import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.compile.Compile;
import kawadevutil.compile.CompileResult;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CursorFinderTest {

    @Test
    public void init() throws Exception {
        // Arrange
        Scheme scheme = Scheme.getInstance();
        Environment env = scheme.getEnvironment();
        CompileResult compRes = Compile.compileToState13(
                "(java.lang.String:__cursor_placeholder__ 'hi)",
                scheme,
                env
        );
        ModuleExp mexp = compRes.getCompilation().getModule();
        ExprWrap rootExprWrap = new ExprWrap(mexp, compRes);
        // Act
        CursorFinder cursorFinder =
                CursorFinder.make(rootExprWrap, "__cursor_placeholder__");
        ExprWrap cursorExprWrap =
                cursorFinder.getCursorMatch().getCursorExprWrap();
        Object cursorElem = cursorExprWrap.getExpr();
        // Assert
        assertEquals(cursorElem.getClass(), QuoteExp.class);
        assertEquals(((QuoteExp) cursorElem).getValue(), "__cursor_placeholder__");
    }

}