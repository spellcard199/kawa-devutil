/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.expr.Language;
import gnu.expr.ModuleExp;
import gnu.kawa.io.CharArrayOutPort;
import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.compile.CompileResult;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.Assert.assertTrue;

public class GnuExprExpressionTest {

    @Test
    public void testFormat() throws IOException {
        // Arrange
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        String expectedTrailing = "(Module/// () (Quote 6 ::integer))";
        // Act
        CompileResult compRes = kawadevutil.compile.Compile.compileToState13("(+ 3 3)", scheme, env);
        ModuleExp mexp = compRes.getCompilation().getModule();
        CharArrayOutPort outPort = new CharArrayOutPort();
        Language saveLang = Language.setSaveCurrent(scheme);
        try {
            kawa.lib.ports.display(mexp, outPort);
        } finally {
            Language.restoreCurrent(saveLang);
        }
        outPort.flush();
        // Assert
        assertTrue(outPort.toString().endsWith(expectedTrailing));
    }
}