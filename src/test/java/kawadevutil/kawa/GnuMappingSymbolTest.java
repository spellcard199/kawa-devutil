/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.bytecode.Type;
import gnu.expr.CompiledProc;
import gnu.mapping.Environment;
import gnu.mapping.Symbol;
import kawa.standard.Scheme;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.Assert.assertEquals;

public class GnuMappingSymbolTest {

    @Test
    public void testGetDeclaration() {
        /*
        TODO : does not work
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        Symbol displaySym = env.getSymbol("display");
        Optional<Declaration> declMaybe = kawadevutil.util.GnuMappingSymbol.getDeclaration(displaySym, scheme, env);
        System.out.println(declMaybe.get());
        */
    }

    @Test
    public void testGetDeclaredType() throws IOException {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        Symbol displaySym = env.getSymbol("display");
        Type type = GnuMappingSymbol.getDeclaredType("display", scheme, env);
        assertEquals(CompiledProc.class, type.getReflectClass());
    }

}