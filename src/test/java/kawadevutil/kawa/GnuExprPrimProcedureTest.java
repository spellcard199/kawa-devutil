/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.bytecode.Method;
import gnu.expr.PrimProcedure;
import kawa.standard.Scheme;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class GnuExprPrimProcedureTest {

    @Test
    public void testGetMethod() throws Throwable {
        Scheme scheme = new kawa.standard.Scheme();
        PrimProcedure formatPrimProcedure = null;
        formatPrimProcedure = (PrimProcedure) scheme.eval("(*:getMethod java.lang.String:format 0)");
        Method m = GnuExprPrimProcedure.getMethod(formatPrimProcedure);
        assertEquals(3, m.getParameterTypes().length);
    }
}