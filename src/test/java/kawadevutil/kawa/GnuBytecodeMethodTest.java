/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

public class GnuBytecodeMethodTest {
    // Used while exploring.
    // @Test
    // public void testGetParamNames() {
    //     System.out.println(" vvv vvv vvv ");
    //     for (Method m : String.class.getMethods()) {
    //         List<ParamData> paramData = GnuBytecodeMethod.getParamData(m);
    //     }
    //     System.out.println(" ^^^ ^^^ ^^^ ");
    //     Method equalsM = String.class.getMethods()[0];
    //     System.out.println(equalsM);
    //     // Scheme scheme = new Scheme();
    //     // scheme.runAsApplication(new String[]{"--server", "37146"});
    // }

    // @Test
    // public void testGetReflectMethod() throws NoSuchMethodException {
    //     // This works.
    //     ClassType jlstringClassType = GnuBytecodeClassType.makeFrom(String.class);
    //     gnu.bytecode.Method[] jlStringGBMethods = jlstringClassType.getMethods(
    //             method -> true, true);
    //     for (gnu.bytecode.Method m : jlStringGBMethods) {
    //         org.testng.Assert.assertNotNull(GnuBytecodeMethod.getReflectMethod(m));
    //     }
    //     // TODO: This fails for java.lang.Byte.<init>()
    //     ClassType jlByteClassType = GnuBytecodeClassType.makeFrom(Byte.class);
    //     gnu.bytecode.Method[] jlByteGBMethods = jlByteClassType.getMethods(
    //             method -> true, true);
    //     for (gnu.bytecode.Method m : jlByteGBMethods) {
    //         org.testng.Assert.assertNotNull(GnuBytecodeMethod.getReflectMethod(m));
    //     }
    // }
}
