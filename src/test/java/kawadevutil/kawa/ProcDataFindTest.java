/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.expr.GenericProc;
import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.data.ParamData;
import kawadevutil.data.ProcDataGeneric;
import kawadevutil.data.ProcDataNonGeneric;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;

public class ProcDataFindTest {

    public static List<String> paramDataListMapGetName(List<ParamData> paramData) {
        return paramData.stream().map(ParamData::getName).collect(Collectors.toList());
    }

    @Test
    public static void testForCompiledProcWithDisplay() {
        ProcDataNonGeneric procDataNonGeneric = ProcDataNonGeneric.makeForCompiledProc(kawa.lib.ports.display);
        List<String> requiredParamNames = paramDataListMapGetName(procDataNonGeneric.getRequiredParams());
        List<String> optionalParamNames = paramDataListMapGetName(procDataNonGeneric.getOptionalParams());
        assertEquals(requiredParamNames, Collections.singletonList("value"));
        assertEquals(optionalParamNames, Collections.singletonList("out"));
    }

    @Test
    public void testForGenericProc() {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        GenericProc cdddr = (GenericProc) env.get(env.getSymbol("cdddr"));
        ProcDataGeneric procDataGeneric = ProcDataGeneric.makeForGenericProc(cdddr);
        List<ProcDataNonGeneric> subProcs = procDataGeneric.getProcDataNonGenericList();
        assertEquals("cdddr", subProcs.get(0).getProc().getName());
    }
}