/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.mapping.Environment;
import gnu.mapping.NamedLocation;
import kawa.standard.Scheme;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.assertTrue;

public class GnuMappingLocationTest {

    @Test
    public void testBaseLocationToModuleName() {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        ArrayList<String> moduleNames = new ArrayList<>();
        env.enumerateAllLocations().forEachRemaining(
                (NamedLocation l) -> moduleNames.add(
                        GnuMappingLocation.baseLocationToModuleName(l.getBase()))
        );
        assertTrue(moduleNames.contains("(kawa lib ports)"));
    }
}