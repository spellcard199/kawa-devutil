/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.autodoc;

import gnu.mapping.Environment;
import kawa.standard.Scheme;
import org.testng.annotations.Test;

import java.util.Optional;

import static org.testng.Assert.assertEquals;

public class AutodocTest {

    public Scheme scheme = new Scheme();
    public Environment env = scheme.getEnvironment();

    @Test
    public void testAutodoc1() throws Throwable {
        String codeStr = "(string-concatenate )";
        int cursorIndex = 20;
        Optional<AutodocData> autodocDataMaybe
                = Autodoc.autodoc(codeStr, cursorIndex, false, scheme, env);
        assertEquals(autodocDataMaybe.get().getCursorArgIndex(), 0);
    }

    @Test
    public void testAutodoc2() throws Throwable {
        String codeStr = "(display 'hi )";
        int cursorIndex = 13;
        Optional<AutodocData> autodocDataMaybe
                = Autodoc.autodoc(codeStr, cursorIndex, false, scheme, env);
        assertEquals(autodocDataMaybe.get().getCursorArgIndex(), 1);
    }
}