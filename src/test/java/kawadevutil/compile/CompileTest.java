/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.compile;

import gnu.expr.*;
import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.exprtree.ExprWrap;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;

public class CompileTest {

    @Test
    public void testCompileStr1() throws Exception {
        // Arrange
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        // Act
        CompileResult compRes = Compile.compileToState13("display", scheme, env);
        ModuleExp mexp = compRes.getCompilation().getModule();
        ReferenceExp referenceExp = (ReferenceExp) mexp.body;
        QuoteExp quoteExp = (QuoteExp) referenceExp.getBinding().getValue();
        Object displayAgain = quoteExp.getValue();
        // Assert
        assertEquals(displayAgain.getClass(), CompiledProc.class);
        assertEquals(((CompiledProc) displayAgain).getName(), "display");
    }

    @Test
    public void testCompileStr2() throws IOException {
        // Testing if compilation of modules recursively works.

        // Arrange
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        ModuleInfo fooMinfo;
        ModuleInfo barMinfo;
        ExprWrap exprWrap;
        List<ExprWrap> applyExpGrandChildren;
        ApplyExp applyExpStringFormat;
        String codeStr =
                "(define-library (foo)"
                        + " (import (kawa base))"
                        + " (define-library (bar)"
                        + "  (import (kawa base))"
                        + "  (java.lang.String:format \"%s\" \"hi\")"
                        + " )"
                        + ")";

        // Act
        CompileResult compRes = Compile.compileToState13(codeStr, scheme, env);
        List<ModuleInfoWrap> flattenedMinfoWrappers = compRes.getMinfoWrap().getFlattenedTree();
        fooMinfo = flattenedMinfoWrappers.get(1).getModuleInfo();
        barMinfo = flattenedMinfoWrappers.get(2).getModuleInfo();
        exprWrap = new ExprWrap(barMinfo.getModuleExp(), compRes);
        Function<ExprWrap, Boolean> isWrappingApplyExp
                = ((ExprWrap aew) -> aew.getExpr().getClass().equals(ApplyExp.class));
        applyExpGrandChildren =
                exprWrap
                        .getChildren()
                        .stream()
                        .filter(isWrappingApplyExp::apply)
                        .flatMap(ewOut -> ewOut.getChildren().stream().filter(isWrappingApplyExp::apply))
                        .collect(Collectors.toList());

        // Assert
        assertEquals(3, flattenedMinfoWrappers.size());
        assertEquals("foo", fooMinfo.getClassName());
        assertEquals("bar", barMinfo.getClassName());
        assertEquals(1, applyExpGrandChildren.size());
        applyExpStringFormat = (gnu.expr.ApplyExp) applyExpGrandChildren.get(0).getExpr();
        assertEquals(java.lang.String.class, applyExpStringFormat.getType().getReflectClass());
    }

    /*
     * @Test
     * public void testCompileStr3() throws IOException {
     *     TODO:
     *       - Testing that declaration numbers don't increment after each compilation.
     *       - Is it possible? How?
     *         - Pushing new module and restoring compilation (as the rewriteForm does)
     *           does not have this effect
     *     Scheme scheme = new Scheme();
     *     Environment env = scheme.getEnvironment();
     *     String codeStr = "(define x ::<java.lang.String> (java.lang.String 'hi))";
     *     CompileResult compileResult1 = Compile.compileToState(codeStr, scheme, env);
     *     CompileResult compileResult2 = Compile.compileToState(codeStr, scheme, env);
     *     ExprWrap exprWrap1 = new ExprWrap(compileResult1);
     *     ExprWrap exprWrap2 = new ExprWrap(compileResult2);
     *     String formattedMexp1 = exprWrap1.formatElem(true);
     *     String formattedMexp2 = exprWrap2.formatElem(true);
     *     System.out.println(formattedMexp1);
     *     System.out.println(formattedMexp2);
     *     assertEquals(formattedMexp1, formattedMexp2);
     * }
     */
}