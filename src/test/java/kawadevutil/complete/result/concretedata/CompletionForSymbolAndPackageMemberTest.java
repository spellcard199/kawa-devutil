/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.result.concretedata;

import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.complete.find.CompletionFindGeneric;
import kawadevutil.complete.result.abstractdata.CompletionData;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class CompletionForSymbolAndPackageMemberTest {

    @Test
    public void testGetNames() {
        Scheme scheme = new kawa.standard.Scheme();
        Environment env = scheme.getEnvironment();
        String codeStr = "(co)";
        int cursorIndex = 3;
        CompletionData complData
                = CompletionFindGeneric
                .find(codeStr, cursorIndex, scheme, env)
                .get();
        List<String > names = complData.getNames();

        // Should contain both Symbol and PackageMember names.
        // If you wanted to separate them you would have to cast
        // CompletionData to CompletionForSymbolAndPackageMember.
        assertTrue(names.containsAll(
                Arrays.asList("cons", "com")
        ));
    }
}