/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.astmatch;

import gnu.expr.ApplyExp;
import gnu.expr.ModuleInfo;
import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.complete.result.concretedata.CompletionForMethod;
import kawadevutil.exprtree.ExprWrap;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class CompletionForNestedModulesTest {
    @Test
    public static void testCompleteInnerLibraries() throws IOException {
        // Testing if compilation of modules recursively works.

        // Arrange
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        ModuleInfo fooMinfo;
        ModuleInfo barMinfo;
        ExprWrap barMexpWrapper;
        List<ExprWrap> applyExpGrandChildren;
        ApplyExp applyExpStringFormat;
        String codeStr =
                "(define-library (foo)"
                        + " (import (kawa base))"
                        + " (define-library (bar)"
                        + "  (import (kawa base))"
                        + "  ((java.lang.String 'hi):)"
                        + " )"
                        + ")";
        int cursorIndex = codeStr.indexOf("'hi):") + "'hi):".length();
        // Act
        CompletionForMethod complData = Util.getAndCastToMethodData(
                codeStr, cursorIndex, scheme, env
        );
        assertTrue(complData.getNames().contains("getChars"));
        assertTrue(complData.getNames().contains("indexOf"));

        /*
        TODO: delete or move this
        codeStr = "(((java.lang.String 'hi):getClass):)";
        cursorIndex = codeStr.indexOf("Class):") + "Class):".length();
        complDataMaybe
                = CompletionFindGeneric.find(codeStr, cursorIndex, scheme, env, (String name) -> true);
        System.out.println(complDataMaybe.get().getNames());
        */
    }
}
