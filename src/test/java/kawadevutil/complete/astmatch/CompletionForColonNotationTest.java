/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.astmatch;

import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.complete.find.CompletionFindGeneric;
import kawadevutil.complete.find.classmembers.ModifierEnum;
import kawadevutil.complete.result.abstractdata.CompletionData;
import kawadevutil.complete.result.concretedata.CompletionForField;
import kawadevutil.complete.result.concretedata.CompletionForMethod;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static kawadevutil.complete.astmatch.Util.getAndCastToFieldData;
import static kawadevutil.complete.astmatch.Util.getAndCastToMethodData;
import static org.testng.Assert.*;

public class CompletionForColonNotationTest {

    @Test
    public static void staticMethod1() throws Throwable {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        CompletionForMethod staticMethodAll = CompletionFindGeneric.find(
                "(java.lang.String:)", 18, scheme, env
        ).get().castTo().complForMethod();
        CompletionForMethod staticMethod = CompletionFindGeneric.find(
                "(java.lang.String:jo)", 20, scheme, env
        ).get().castTo().complForMethod();

        assertTrue(staticMethodAll.getNames().contains("join"));
        assertTrue(staticMethodAll.getNames().contains("format"));
        assertTrue(staticMethod.getNames().contains("join"));
    }

    @Test
    public static void staticMethod2() {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        String codeStr1 = "(java.lang.String:)";
        int cursorIndex1 = codeStr1.indexOf("String:") + "String:".length();
        CompletionForMethod complData = getAndCastToMethodData(
                codeStr1, cursorIndex1, scheme, env
        );
        Assert.assertFalse(complData.getNames().contains("getDeclaredFields"));
    }

    @Test
    public static void staticMethod3() {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        String codeStr2 = "(java.lang.Class:)";
        int cursorIndex2 = codeStr2.indexOf("Class:") + "Class:".length();
        CompletionForMethod complData = getAndCastToMethodData(
                codeStr2, cursorIndex2, scheme, env
        );
        assertTrue(complData.getNames().contains("getDeclaredFields"));
    }

    @Test
    public static void instanceMethod() throws Throwable {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        scheme.eval("(define s ::<java.lang.String> (java.lang.String \"foobar\"))");

        CompletionForMethod instanceMethodAll = getAndCastToMethodData(
                "(s:)", 3, scheme, env
        );

        CompletionForMethod instanceMethod = getAndCastToMethodData(
                "(s:con)", 6, scheme, env
        );
        CompletionForMethod instanceMethodApplyExp1 = getAndCastToMethodData(
                "((string-trim \"hi\"):)", 20, scheme, env
        );
        CompletionForMethod instanceMethodApplyExp2 = getAndCastToMethodData(
                "((java.lang.String 'hi):)", 24, scheme, env
        );
        assertTrue(instanceMethodAll.getNames().contains("getChars"));
        assertTrue(instanceMethodAll.getNames().contains("compareTo"));
        assertTrue(instanceMethodAll.getNames().contains("indexOf"));
        assertTrue(instanceMethod.getNames().contains("contentEquals"));
        assertTrue(instanceMethod.getNames().contains("concat"));
        assertTrue(instanceMethod.getNames().contains("contains"));
    }

    @Test
    public static void instanceMethodLet() {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();

        String codeStr = "((let ((x (java.lang.String 'hi))) x):)";
        int cursorIndex = codeStr.indexOf("x):") + "x):".length();
        CompletionForMethod complData = getAndCastToMethodData(
                codeStr, cursorIndex, scheme, env
        );

        assertEquals(complData.getOwnerClass(), java.lang.String.class);
        assertTrue(!complData
                .getModifierMask()
                .getRequired()
                .contains(ModifierEnum.STATIC));
    }

    @Test
    public static void staticMethodLet() {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();

        String codeStr = "((let ((x java.lang.String)) x):)";
        int cursorIndex = codeStr.indexOf("x):") + "x):".length();
        Optional<CompletionData> complData = CompletionFindGeneric.find(
                codeStr, cursorIndex, scheme, env
        );
        // It doesn't make sense to complete
        // if we don't know which class we are completing for.
        // At compile time Kawa may know that it's a class
        // but except in specific cases, it don't know which one.
        assertEquals(complData, Optional.empty());
    }

    @Test
    public static void complForStaticFieldsExtra1() throws Throwable {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();

    }

    @Test
    public static void staticField() throws Throwable {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        CompletionForField staticField = getAndCastToFieldData(
                "java.lang.String:CAS", 20, scheme, env);
        assertEquals(staticField.getNames(), Collections.singletonList("CASE_INSENSITIVE_ORDER"));

        String codeStr = "(static-field " +
                "kawadevutil.complete.find.classmembers.CompletionFindClassMember " +
                "')";
        int cursorIndex = 80;

        Optional<CompletionData> complDataMaybe;
        complDataMaybe = CompletionFindGeneric.find(codeStr, cursorIndex, scheme, env);
        CompletionForField complForFields
                = complDataMaybe.get().castTo().complForField();

        assertEquals(
                complForFields.getCompletionType(),
                CompletionData.CompletionType.FIELDS);
        assertTrue(complForFields
                .getModifierMask()
                .getRequired()
                .contains(ModifierEnum.STATIC)
        );

    }

    @Test
    public static void instanceField() throws Throwable {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        scheme.eval(
                "(define s ::<java.lang.String> "
                        + "(java.lang.String \"foobar\"))");
        CompletionForField instanceField = getAndCastToFieldData(
                "s:CAS", 5, scheme, env
        );
        assertEquals(instanceField.getNames(), Collections.singletonList("CASE_INSENSITIVE_ORDER"));

        // Initially these weren't working because I had only considered the case
        // of a ReferenceExp before the colon.
        // These examples, instead, have an ApplyExp before the colon.
        CompletionForField instanceFieldApplyExp1 = getAndCastToFieldData(
                "(string-trim \"hi\"):", 19, scheme, env
        );
        CompletionForField instanceFieldApplyExp2 = getAndCastToFieldData(
                "(java.lang.String 'hi):", 23, scheme, env
        );

        ///////////////////////////////////////////////////////////////////

        scheme.eval("(define evaluator ::<kawadevutil.eval.Eval> " +
                "(kawadevutil.eval.Eval))");
        Optional<CompletionData> x;
        x = CompletionFindGeneric.find(
                "(evaluator:setSystemOutRedirPolicy " +
                        "kawadevutil.eval.Eval:SystemOutRedirectPolicy:)",
                81,
                scheme,
                env
        );
        CompletionForField complData = x.get().castTo().complForField();
        assertEquals(
                complData.getCompletionType(),
                CompletionData.CompletionType.FIELDS);
    }

    @Test
    public static void testCompleteDynamicClassLoader() throws Throwable {
        // Arrange
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        // To try that the matcher works, we need a sample class loaded by a dynamic ClassLoader
        Class schemeClzFromAppClassLoader = CompletionForColonNotationTest.class.getClassLoader().loadClass("kawa.standard.Scheme");
        URL schemeRes = schemeClzFromAppClassLoader.getResource("Scheme.class");
        URL jarFileUrl = new URL(schemeRes.getFile().replace("!/kawa/standard/Scheme.class", ""));
        URLClassLoader ucl = new URLClassLoader(new URL[]{jarFileUrl}, null);
        Class schemeClzFromURLClassLoader = ucl.loadClass("kawa.standard.Scheme");
        // (Just testing this test) 2 classes are equals only if they are loaded by the same ClassLoader,
        // so these should be different
        Assert.assertNotEquals(schemeClzFromAppClassLoader, schemeClzFromURLClassLoader);
        Object schemeInstanceFromURLClassLoader = schemeClzFromURLClassLoader.getConstructor().newInstance();
        scheme.define("dynSchemeClass", schemeClzFromURLClassLoader);
        scheme.define("dynSchemeInstance", schemeInstanceFromURLClassLoader);

        // Act
        CompletionForMethod dynamicallyLoadedClassStaticMethod
                = (CompletionForMethod)
                CompletionFindGeneric.find(
                        "(dynSchemeClass:)", 16, scheme, env).get();
        CompletionForMethod dynamicallyLoadedClassInstance
                = (CompletionForMethod)
                CompletionFindGeneric.find(
                        "(dynSchemeInstance:)", 19, scheme, env).get();
        CompletionForMethod checkingForNullPointerException
                = (CompletionForMethod)
                CompletionFindGeneric.find(
                        "(java.lang.Quo:)", 15, scheme, env).get();

        // Assert
        // Testing the test here
        assertEquals(scheme.eval("dynSchemeClass"), schemeClzFromURLClassLoader);
        assertEquals(scheme.eval("dynSchemeInstance"), schemeInstanceFromURLClassLoader);

        assertEquals(dynamicallyLoadedClassStaticMethod.getOwnerClass().getName(), "kawa.standard.Scheme");
        assertFalse(dynamicallyLoadedClassStaticMethod.getNames().contains("getFormat"));

        assertEquals(dynamicallyLoadedClassInstance.getOwnerClass().getName(), "kawa.standard.Scheme");
        assertTrue(dynamicallyLoadedClassInstance.getNames().contains("getFormat"));
    }
}
