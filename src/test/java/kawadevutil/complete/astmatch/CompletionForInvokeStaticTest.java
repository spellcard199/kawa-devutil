/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.astmatch;

import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.complete.find.classmembers.CompletionFindClassMember;
import kawadevutil.complete.result.abstractdata.CompletionForClassMember;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CompletionForInvokeStaticTest {

    @Test
    public void testInvokeStatic() throws IOException {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();

        String codeStr1 = "(invoke-static java.lang.String ')";
        String findMe1 = "String '";
        int cursorIndex1 = codeStr1.indexOf(findMe1) + findMe1.length();
        CompletionForClassMember complData1 = CompletionFindClassMember.find(
                codeStr1, cursorIndex1, scheme, env, false
        ).get();

        String codeStr2 = "(invoke-static java.lang.String \"\")";
        String findMe2 = "String \"";
        int cursorIndex2 = codeStr2.indexOf(findMe2) + findMe2.length();
        CompletionForClassMember complData2 = CompletionFindClassMember.find(
                codeStr2, cursorIndex2, scheme, env, false
        ).get();

        assertEquals(complData1.getNames(), complData2.getNames());
        assertTrue(complData1
                .getNames()
                .containsAll(Arrays.asList("valueOf", "format"))
        );
    }
}