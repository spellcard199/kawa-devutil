/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.astmatch;

import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.complete.find.classmembers.CompletionFindClassMember;
import kawadevutil.complete.result.abstractdata.CompletionForClassMember;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class CompletionForSlotRefStaticTest {

    @Test
    public void testFind() throws IOException {
        /*
         * Interestingly, this expression and colon notation get
         * translated to the same AST. That's why we don't have
         * a matcher specifically for (field [classname] "...").
         *
         * I.e.:
         * (Apply [tailcall] line:1:1-56
         *   (Quote
         *     ClassType java.lang.Object gnu.kawa.reflect.SlotGet.getSlotValue(boolean,java.lang.Object,java.lang.String,java.lang.String,java.lang.String,java.lang.String,gnu.expr.Language)
         *     ::gnu.expr.PrimProcedure)
         *   (Quote #t ::boolean)
         *   (Quote class java.lang.String ::class)
         *   (Quote "__cursor_placeholder__" ::java.lang.String)
         *   (Quote "__cursor_placeholder__" ::java.lang.String)
         *   (Quote "get__cursor_placeholder__" ::java.lang.String)
         *   (Quote "is__cursor_placeholder__" ::java.lang.String)
         *   (Quote kawa.standard.Scheme@740fb309 ::kawa.standard.Scheme))
         */
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();

        String codeStr1 = "(static-field java.lang.String ')";
        String findMe1 = "String '";
        int cursorIndex1 = codeStr1.indexOf(findMe1) + findMe1.length();
        CompletionForClassMember complData1 = CompletionFindClassMember.find(
                codeStr1, cursorIndex1, scheme, env, false
        ).get();

        String codeStr2 = "(static-field java.lang.String \"\")";
        String findMe2 = "String \"";
        int cursorIndex2 = codeStr2.indexOf(findMe2) + findMe2.length();
        CompletionForClassMember complData2 = CompletionFindClassMember.find(
                codeStr2, cursorIndex2, scheme, env, false
        ).get();

        assertEquals(
                complData1.getNames(),
                complData2.getNames()
        );
        assertEquals(
                complData1.getNames(),
                Collections.singletonList("CASE_INSENSITIVE_ORDER"));
    }
}