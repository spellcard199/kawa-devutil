/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.astmatch;

import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.complete.find.CompletionFindGeneric;
import kawadevutil.complete.result.concretedata.CompletionForField;
import kawadevutil.complete.result.concretedata.CompletionForMethod;

public class Util {

    public static CompletionForMethod
    getAndCastToMethodData(String codeStr, int cursorIndex,
                           Scheme scheme, Environment env) {
        return CompletionFindGeneric
                .find(codeStr, cursorIndex, scheme, env)
                .get()
                .castTo()
                .complForMethod();
    }

    public static CompletionForField
    getAndCastToFieldData(String codeStr, int cursorIndex,
                          Scheme scheme, Environment env) {
        return CompletionFindGeneric
                .find(codeStr, cursorIndex, scheme, env)
                .get()
                .castTo()
                .complForField();
    }
}
