/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.astmatch;

import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.complete.find.CompletionFindGeneric;
import kawadevutil.complete.find.classmembers.CompletionFindClassMember;
import kawadevutil.complete.result.abstractdata.CompletionForClassMember;
import kawadevutil.complete.result.concretedata.CompletionForField;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class CompletionForSlotRefInstanceTest {

    @Test
    public void testMatchSlotRefOnInstance() throws IOException {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();

        String codeStr1 = "(field (java.lang.String 'hi) ')";
        String findMe1 = "'hi) '";
        int cursorIndex1 = codeStr1.indexOf(findMe1) + findMe1.length();
        CompletionForClassMember complData1 = CompletionFindClassMember.find(
                codeStr1, cursorIndex1, scheme, env, false
        ).get();

        String codeStr2 = "(field (java.lang.String 'hi) \"\")";
        String findMe2 = "'hi) \"";
        int cursorIndex2 = codeStr2.indexOf(findMe2) + findMe2.length();
        CompletionForClassMember complData2 = CompletionFindClassMember.find(
                codeStr2, cursorIndex2, scheme, env, false
        ).get();

        String codeStr3 = "(slot-ref (java.lang.String 'hi) \"\")";
        String findMe3 = "'hi) \"";
        int cursorIndex3 = codeStr3.indexOf(findMe3) + findMe3.length();
        CompletionForClassMember
                complData3
                = CompletionFindClassMember.find(codeStr3, cursorIndex3, scheme, env, false)
                .get();

        assertEquals(complData1.getNames(), complData2.getNames());
        assertEquals(complData1.getNames(), complData3.getNames());
        assertEquals(complData1.getNames(), Collections.singletonList("CASE_INSENSITIVE_ORDER"));
    }

    @Test
    public void testMatchSlotRefOnClass() throws IOException {
        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();

        String codeStr1 = "(field java.lang.String ')";
        String findMe1 = "java.lang.String '";
        int cursorIndex1 = codeStr1.indexOf(findMe1) + findMe1.length();
        CompletionForField complData1 = CompletionFindGeneric.find(
                codeStr1, cursorIndex1, scheme, env, false
        ).get().castTo().complForField();

        assertEquals(complData1.getOwnerClass(), Class.class);
        assertEquals(complData1.getNames(), Collections.emptyList());
    }
}