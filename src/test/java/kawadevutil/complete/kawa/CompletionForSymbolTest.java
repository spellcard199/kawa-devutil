/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.kawa;

import gnu.expr.CommandCompleter;
import gnu.expr.Language;
import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.complete.find.symbols.CompletionFindSymbols;
import kawadevutil.kawa.GnuExprCommandCompleter;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CompletionForSymbolTest {

    @Test
    public void testCompletionFindSymbols() {
        Scheme scheme = new kawa.standard.Scheme();
        Environment env = scheme.getEnvironment();

        String codeStr = "(dis";
        int cursorIndex = 4;

        Language saveLang = Language.setSaveCurrent(scheme);
        CommandCompleter commandCompleter = null;
        try {
            commandCompleter = GnuExprCommandCompleter.makeFor(
                    codeStr, cursorIndex, scheme
            ).get();
        } finally {
            Language.restoreCurrent(saveLang);
        }

        CommandCompleter commandCompleterAgain =
                CompletionFindSymbols.find(commandCompleter)
                        .get().getCommandCompleter();

        List<String> candidates = commandCompleter.candidates;
        String word = commandCompleter.word;
        int wordCursor = commandCompleter.wordCursor;

        assertEquals(commandCompleter, commandCompleterAgain);
        assertEquals(wordCursor, 3);
        assertEquals(word, "dis");
        assertTrue(candidates.contains("display"));
        assertTrue(candidates.contains("disassemble"));
    }
}