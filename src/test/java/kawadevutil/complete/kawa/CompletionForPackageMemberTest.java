/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.kawa;

import gnu.mapping.Environment;
import kawa.standard.Scheme;
import kawadevutil.complete.find.CompletionFindGeneric;
import kawadevutil.complete.result.concretedata.CompletionForSymbolAndPackageMember;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CompletionForPackageMemberTest {
    @Test
    public static void testCompletePackage() throws IOException {

        Scheme scheme = new Scheme();
        Environment env = scheme.getEnvironment();
        Environment saveEnv = Environment.setSaveCurrent(env);

        CompletionForSymbolAndPackageMember complData1;
        CompletionForSymbolAndPackageMember complData2;
        CompletionForSymbolAndPackageMember complData3;
        CompletionForSymbolAndPackageMember complData4;
        try {

            complData1 = CompletionFindGeneric.find(
                    "(co)", 3, scheme, env
            ).get().castTo().complForSymAndPackageMember();

            complData2 = CompletionFindGeneric.find(
                    "(java.lang.St)", 13,
                    scheme, env
            ).get().castTo().complForSymAndPackageMember();

            complData3 = CompletionFindGeneric.find(
                    "(java.lang.St", 13,
                    scheme, env,
                    true
            ).get().castTo().complForSymAndPackageMember();

            complData4 = CompletionFindGeneric.find(
                    "(java.lang.St", 13,
                    scheme, env,
                    false
            ).get().castTo().complForSymAndPackageMember();
        } catch (Exception e) {
            throw e;
        } finally {
            Environment.restoreCurrent(saveEnv);
        }

        assertTrue(
                complData1
                        .getCompletionForPackageMember()
                        .get()
                        .getChildClassInfoList()
                        .isEmpty()
                        || complData1 // Work-around for when you run this test in IntelliJ
                        .getCompletionForPackageMember()
                        .get()
                        .getChildClassInfoList()
                        .getNames()
                        .equals(Collections.singletonList("FormPreviewFrame"))
        );
        assertTrue(complData1
                .getCompletionForSymbol()
                .get()
                .getNames()
                .containsAll(Arrays.asList(
                        "cosh", "cond-expand", "constant-vector", "cons",
                        "cond", "command-parse", "complex?", "cos",
                        "command-line-arguments", "command-line",
                        "compile-file", "copy-file", "constant-fold")
                )
        );
        assertTrue(complData1
                .getCompletionForPackageMember()
                .get()
                .getChildPackageInfoList()
                .getNames()
                .containsAll(Arrays.asList("com", "gnu", "java", "kawa", "kawadevutil"))
        );
        assertTrue(complData2
                .getCompletionForPackageMember()
                .get()
                .getChildClassInfoList()
                .getNames()
                .containsAll(Arrays.asList("java.lang.CharSequence", "java.lang.String"))
        );
        assertEquals(complData1.getCouldMakeExprTree(), Optional.of(true));
        assertEquals(complData2.getCouldMakeExprTree(), Optional.of(true));
        assertEquals(complData3.getCouldMakeExprTree(), Optional.of(true));
        assertEquals(complData4.getCouldMakeExprTree(), Optional.of(false));
    }
}
