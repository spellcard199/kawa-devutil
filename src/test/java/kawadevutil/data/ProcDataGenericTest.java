/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.data;

import gnu.bytecode.Type;
import gnu.mapping.Procedure1or2;
import org.testng.annotations.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class ProcDataGenericTest {

    @Test
    public void testMakeForProcedure() {
        // Arrange
        Procedure1or2 exampleProc = new Procedure1or2() {
            @Override
            public Object apply1(Object o) throws Throwable {
                return null;
            }

            @Override
            public Object apply2(Object o, Object o1) throws Throwable {
                return null;
            }
        };
        // Act
        ProcDataGeneric pdg = ProcDataGeneric.makeForProcedure(exampleProc);
        ProcDataNonGeneric pdng = pdg.getProcDataNonGenericList().get(0);
        // Assert
        assertEquals(1, pdng.getRequiredParams().size());
        assertEquals(1, pdng.getOptionalParams().size());
        assertFalse(pdng.getRestParam().isPresent());
    }

    @Test
    public void testMakeForConstructors() {
        ProcDataGeneric pdg = ProcDataGeneric.makeForConstructors(String.class);

        ProcDataNonGeneric thirdConstructor = pdg.getProcDataNonGenericList().get(2);
        // The reason we are using the first constructor is because successive constructors
        // depend on specific java version, while the first one seems to have always type "[]".
        // Example: pdg.getProcDataNonGenericList().get(2) 's type on:
        // - java 8 : "[Type byte[], Type int]"
        // - java 11: "[Type char[]]"
        thirdConstructor = pdg.getProcDataNonGenericList().get(0);
        List<Type> paramTypes =
                thirdConstructor
                        .getRequiredParams()
                        .stream()
                        .map(ParamData::getType)
                        .collect(Collectors.toList());

        assertEquals(paramTypes.toString(), "[]");
    }

    // @Test
    // public void testMakeVagueData() throws Throwable {
    //     // Never again try to do:
    //     //   Invoke invokeProc = Invoke.invoke;
    //     //   ProcDataGeneric.makeForProcedure(invokeProc);
    //     // Same thing happens if you make a new Invoke object:
    //     //   Invoke invokeProc = new Invoke("invoke", '*');
    //     //   ProcDataGeneric.makeForProcedure(invokeProc);
    //     // Last time it messed up half of the tests.
    //     // Maybe it's a bug in Kawa?

    //     // TODO:
    //     //  Find another procedure to test makeVagueProcData with.
    //     //  I tried to use invoke because it's a Procedure
    //     //  but it's also not one of the subclasses I hardcoded
    //     //  in ProcDataNonGeneric.

    //     // Invoke invokeProc = Invoke.invoke;
    //     // ProcDataGeneric procDataGeneric
    //     //         = ProcDataGeneric.makeForProcedure(invokeProc);
    //     // ProcDataNonGeneric procDataNonGeneric
    //     //         = procDataGeneric.getProcDataNonGenericList().get(0);

    //     // assertEquals(procDataNonGeneric.getRequiredParams().size(), 2);
    //     // assertEquals(procDataNonGeneric.getOptionalParams().size(), 0);
    //     // assertTrue(procDataNonGeneric.getRestParam().isPresent());
    // }
}