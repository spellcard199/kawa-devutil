/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.data;

import gnu.expr.CompiledProc;
import kawa.standard.Scheme;
import org.testng.annotations.Test;

public class ProcDataNonGenericTest {

    @Test
    public void testMakeForCompiledProc() throws Throwable {
        Scheme scheme = new Scheme();
        CompiledProc evalCompProc = (CompiledProc) scheme.eval("eval");
        ProcDataNonGeneric pdng = ProcDataNonGeneric.makeForCompiledProc(evalCompProc);
        org.testng.Assert.assertEquals(pdng.getModule(), kawa.lib.scheme.eval.class);
        org.testng.Assert.assertEquals(pdng.getRequiredParams().size(), 2);
    }
}