/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Reflecting<T> {
    /*
     *  If you want a return type of getDeclaredFieldValue more specialized than Object
     *  you can instantiate this class and pass a type parameter
     */

    private static Reflecting<Object> defaultInstance = new Reflecting<>();

    public static Reflecting<Object> getDefaultInstance() {
        return defaultInstance;
    }

    public Field
    getDeclaredField(Object obj, String fieldName)
            throws NoSuchFieldException {
        // Works for both classes and objects
        boolean isClassObject = obj.getClass() == Class.class;
        Field fieldReflect;
        if (isClassObject) {
            Class classObj = (Class) obj;
            fieldReflect = classObj.getDeclaredField(fieldName);
        } else {
            fieldReflect = obj.getClass().getDeclaredField(fieldName);
        }
        return fieldReflect;
    }

    public T
    getDeclaredFieldValue(Object obj, String fieldName)
            throws NoSuchFieldException, IllegalAccessException {
        // Boilerplate to access java private fields.
        // Works for both:
        // - objects and instance fields
        // - classes and static fields
        // Returns Optional.empty() when we get IllegalAccessException

        boolean isClassObject = obj.getClass() == Class.class;
        Field fieldReflect = getDeclaredField(obj, fieldName);

        // To be Returned:
        T fieldValue = null;
        // Instance fields should have a non-Class-object owner
        // Static fields should have a Class-object owner
        if (isClassObject == Modifier.isStatic(fieldReflect.getModifiers())
                && fieldReflect != null) {
            // On java 9 isAccessible is deprecated and replaced by canAccess:
            // boolean saveFieldAccess =
            //         isClassObject
            //                 ? fieldReflect.canAccess(null)
            //                 : fieldReflect.canAccess(obj);
            boolean saveFieldAccess = fieldReflect.isAccessible();
            try {
                fieldReflect.setAccessible(true);
                fieldValue =
                        isClassObject
                                ? (T) fieldReflect.get(null)
                                : (T) fieldReflect.get(obj);
            } finally {
                fieldReflect.setAccessible(saveFieldAccess);
            }
        }
        return fieldValue;
    }
}
