/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.util;

import java.io.*;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipExtractor {

    public static void unzip(File archive, Path destDir) throws IOException {
        ZipFile zipFile = new ZipFile(archive);
        if (!destDir.toFile().exists()) {
            destDir.toFile().mkdirs();
        }
        Enumeration<? extends ZipEntry> enumer = zipFile.entries();
        for (ZipEntry zipEntry = enumer.nextElement();
             enumer.hasMoreElements();
             zipEntry = enumer.nextElement()) {
            extractEntry(zipFile, zipEntry, destDir);
        }
        zipFile.close();
    }

    private static void
    extractEntry(ZipFile zipFile, ZipEntry zipEntry, Path destDir) throws IOException {
        boolean isZipEntryDir = zipEntry.getName().endsWith("/");
        File destFile
                = destDir
                .resolve(zipEntry.getName())
                .toAbsolutePath().toFile();
        if (isZipEntryDir) {
            destFile.mkdirs();
        } else {
            if (destFile.getParentFile() != null) {
                destFile.getParentFile().mkdirs();
            }
            InputStream is = zipFile.getInputStream(zipEntry);
            FileOutputStream fos = new FileOutputStream(destFile);
            byte[] bytes = new byte[1024];
            for (Integer length = is.read(bytes); length >= 0; length = is.read(bytes)) {
                fos.write(bytes, 0, length);
            }
            is.close();
            fos.close();
        }
    }
}
