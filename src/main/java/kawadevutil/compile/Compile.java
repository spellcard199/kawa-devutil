/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.compile;

import gnu.expr.*;
import gnu.kawa.io.CharArrayInPort;
import gnu.mapping.Environment;
import gnu.text.Lexer;
import gnu.text.SourceMessages;
import kawa.lang.Translator;

import java.io.IOException;
import java.util.ArrayList;

public class Compile {

    public static CompileResult
    compileToState13(String codeStr)
            throws IOException {
        return compileToState(codeStr, 13);
    }

    public static CompileResult
    compileToState13(String codeStr, Language lang, Environment env)
            throws IOException {
        return compileToState(codeStr, lang, env, 13);
    }

    public static CompileResult
    compileToState(String codeStr, int wantedState)
            throws IOException {
        return compileToState(
                codeStr, Language.getDefaultLanguage(), Environment.user(), wantedState);
    }

    public static CompileResult
    compileToState(String codeStr, Language lang, Environment env, int wantedState)
            throws IOException {
        CharArrayInPort inp = new CharArrayInPort(codeStr);
        SourceMessages messages = new SourceMessages();
        Lexer lexer = lang.getLexer(inp, messages);
        int opts = Language.PARSE_FOR_EVAL;
        Compilation comp;
        Language saveLang = Language.setSaveCurrent(lang);
        Environment saveEnv = Environment.setSaveCurrent(env);
        try {
            comp = lang.parse(lexer, opts, null);
            compileSubModulesRecursively(comp, wantedState);
        } finally {
            Language.restoreCurrent(saveLang);
            Environment.restoreCurrent(saveEnv);
        }
        return new CompileResult(comp, messages);
    }

    private static void
    compileSubModulesRecursively(Compilation comp, int wantedState) {
        ArrayList<ModuleInfo> minfosToCompile = new ArrayList<>();
        minfosToCompile.add(comp.getMinfo());
        for (int i = 0; i < minfosToCompile.size(); i++) {
            Compilation compForMinfo = minfosToCompile.get(i).getCompilation();
            // subModuleMap is not populated until the outer Module is compiled
            compForMinfo.process(wantedState);
            if (compForMinfo.subModuleMap != null) {
                minfosToCompile.addAll(compForMinfo.subModuleMap.values());
            }
        }
    }

    public static Expression
    rewriteForm(Object sexp,
                Language lang,
                Environment env
    ) {
        // UNUSED
        //
        // copied and translated to java from
        // gnu/kawa/slib/syntaxutils.scm/rewrite-form
        // because it's not exported.
        //
        // Note: to avoid errors like... :
        //   (Error define is only allowed in a <body>)
        // ... wrap sexp in a (begin ...) form.

        NameLookup nameLookup = NameLookup.getInstance(env, lang);
        SourceMessages messages = new SourceMessages();
        Translator translator = (Translator) lang.getCompilation(messages, nameLookup);
        ModuleExp module = translator.pushNewModule(null);
        Compilation savedComp = Compilation.setSaveCurrent(translator);
        Expression rewritten;
        try {
            rewritten = translator.rewrite(sexp);
        } finally {
            Compilation.restoreCurrent(savedComp);
        }
        return rewritten;
    }
}
