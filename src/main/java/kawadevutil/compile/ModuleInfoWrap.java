/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.compile;

import gnu.expr.Compilation;
import gnu.expr.ModuleExp;
import gnu.expr.ModuleInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ModuleInfoWrap {

    ModuleInfo moduleInfo;
    Optional<ModuleInfoWrap> parent;

    public ModuleInfoWrap(ModuleInfo moduleInfo, ModuleInfo parentMinfo) {
        this.moduleInfo = moduleInfo;
        this.parent = parentMinfo != null
                ? Optional.of(new ModuleInfoWrap(parentMinfo, null))
                : Optional.empty();
    }

    public ModuleInfo getModuleInfo() {
        return moduleInfo;
    }

    public Optional<ModuleInfoWrap> getParent() {
        return parent;
    }

    public ModuleExp getModuleExp() {
        return getModuleInfo().getModuleExp();
    }

    public List<ModuleInfoWrap> getChildren() {
        Compilation comp = this.moduleInfo.getCompilation();
        ArrayList<ModuleInfoWrap> subModuleWrappers = new ArrayList<>();
        if (comp.subModuleMap != null) {
            for (ModuleInfo minfo : comp.subModuleMap.values()) {
                subModuleWrappers.add(new ModuleInfoWrap(minfo, this.moduleInfo));
            }
        }
        return subModuleWrappers;
    }

    public List<ModuleInfoWrap> getFlattenedTree() {
        ArrayList<ModuleInfoWrap> flattenedTree = new ArrayList<>();
        flattenedTree.add(this);
        for (int i = 0; i < flattenedTree.size(); i++) {
            flattenedTree.addAll(flattenedTree.get(i).getChildren());
        }
        return flattenedTree;
    }
}
