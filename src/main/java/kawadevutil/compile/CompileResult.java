/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.compile;

import gnu.expr.Compilation;
import gnu.text.SourceMessages;

import java.util.Optional;

public class CompileResult {

    Compilation compilation;
    SourceMessages messages;

    public CompileResult(Compilation compilation, SourceMessages messages) {
        if (compilation == null) {
            throw new IllegalArgumentException("compilation cannot be null");
        }
        this.compilation = compilation;
        this.messages = messages;
    }

    public Compilation getCompilation() {
        return compilation;
    }

    Optional<SourceMessages> getMessages() {
        return messages != null
                ? Optional.of(messages)
                : Optional.empty();
    }

    public Optional<String> messagesToStrings() {
        return getMessages().map(
                msgs -> msgs.toString(Integer.MAX_VALUE));
    }

    public ModuleInfoWrap getMinfoWrap() {
        return new ModuleInfoWrap(
                this.compilation.getMinfo(),
                null);
    }
}
