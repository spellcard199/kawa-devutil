/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.eval;

public abstract class StackTraceFormatter {

    /*
     *  Used to allow customization of stacktrace formatting.
     *  A stack trace formatter is an instance of this class
     *  that implements the format method.
     */
    abstract String format(Throwable tr);

    public static StackTraceFormatter defaultStackTraceFormatter = new StackTraceFormatter() {
        public String format(Throwable tr) {
            // Default stack trace formatter: just print as java would.
            java.io.StringWriter sw = new java.io.StringWriter();
            tr.printStackTrace(new java.io.PrintWriter(sw));
            return sw.toString();
        }
    };

}
