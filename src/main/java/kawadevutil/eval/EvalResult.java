/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.eval;

import gnu.kawa.io.OutPort;
import gnu.text.SourceMessages;

import java.io.StringWriter;

public class EvalResult {
    // Just a wrapper for the data our custom eval method obtains.
    boolean success;
    Object result;
    Throwable throwed;
    SourceMessages messages;

    EvalResult(boolean success,
               Object result,
               Throwable throwed,
               SourceMessages messages) {
        this.success = success;
        this.result = result;
        this.throwed = throwed;
        this.messages = messages;
    }

    // getters
    public boolean isSuccess() {
        return success;
    }

    public Object getResult() {
        return result;
    }

    public Throwable getThrowed() {
        return throwed;
    }

    public SourceMessages getMessages() {
        return messages;
    }

    @Override
    public String toString() {
        return "EvalResult("
                + "success=" + isSuccess() + ","
                + "result=" + getResult() + ","
                + "throwed=" + getThrowed() + ","
                + "messages=" + getMessages() + ","
                + ")";
    }

    public String getResultAsString(boolean prettyPrint) {
        StringWriter sw = new StringWriter();
        OutPort out = new OutPort(sw, prettyPrint, false);
        kawa.lib.ports.display(getResult(), out);
        out.flush();
        out.close();
        return sw.toString();
    }
}
