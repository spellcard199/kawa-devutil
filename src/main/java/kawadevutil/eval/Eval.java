/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.eval;

import gnu.expr.Language;
import gnu.kawa.io.InPort;
import gnu.kawa.lispexpr.LispReader;
import gnu.lists.LList;
import gnu.lists.PairWithPosition;
import gnu.mapping.Environment;
import gnu.text.SourceMessages;
import gnu.text.SyntaxException;
import kawadevutil.redirect.redirectors.*;

import java.io.IOException;
import java.util.function.Supplier;

public class Eval {
    // Default to not redirect Java output
    private SystemOutRedirectPolicy systemOutRedirPolicy
            = SystemOutRedirectPolicy.BIDIRECT;
    // Allow customizing pretty printing
    private boolean prettyPrintOutput = true;
    private boolean prettyPrintResult = true;

    public Eval() {
        // Use defaults.
    }

    public Eval(SystemOutRedirectPolicy systemOutRedirPolicy,
                boolean prettyPrintOutput,
                boolean prettyPrintResult) {
        this.systemOutRedirPolicy = systemOutRedirPolicy;
        this.prettyPrintOutput = prettyPrintOutput;
        this.prettyPrintResult = prettyPrintResult;
    }

    public SystemOutRedirectPolicy getSystemOutRedirPolicy() {
        return systemOutRedirPolicy;
    }

    public void setSystemOutRedirPolicy(SystemOutRedirectPolicy systemOutRedirPolicy) {
        this.systemOutRedirPolicy = systemOutRedirPolicy;
    }

    public boolean isPrettyPrintOutput() {
        return prettyPrintOutput;
    }

    public boolean isPrettyPrintResult() {
        return prettyPrintResult;
    }

    public void setPrettyPrintResult(boolean prettyPrintResult) {
        this.prettyPrintResult = prettyPrintResult;
    }

    public void setPrettyPrintOutput(boolean prettyPrintOutput) {
        this.prettyPrintOutput = prettyPrintOutput;
    }

    // The Kawa repl prints the output produced before the exception was raised
    // after the stack trace. If we printed the stack trace at the time it was
    // raised it would have a different behavior than kawa repl, which may
    // or may not be wanted.
    private boolean printStackTrace = false;

    public boolean isPrintStackTrace() {
        return printStackTrace;
    }

    public void setPrintStackTrace(boolean printStackTrace) {
        this.printStackTrace = printStackTrace;
    }

    // Allow customizing how formatStackTrace works
    private StackTraceFormatter stackTraceFormatter =
            StackTraceFormatter.defaultStackTraceFormatter;

    public void setStackTraceFormatter(StackTraceFormatter stackTraceFormatter) {
        this.stackTraceFormatter = stackTraceFormatter;
    }

    public StackTraceFormatter getStackTraceFormatter() {
        return stackTraceFormatter;
    }

    private Object read(gnu.kawa.io.InPort port) throws IOException, gnu.text.SyntaxException {
        // port of read in kawa.lib.ports.scm
        LispReader lexer = new LispReader(port);
        lexer.setReturnMutablePairs(true);
        try {
            Object result = lexer.readObject();
            if (lexer.seenErrors()) {
                throw new gnu.text.SyntaxException(lexer.getMessages());
            } else {
                return result;
            }
        } catch (gnu.text.SyntaxException e) {
            e.setHeader("syntax error in read:");
            throw e;
        } catch (IOException e) {
            // read in kawa.lib.ports.scm doesn't have this catch clause
            throw e;
        }
    }

    public Object readString(String codeStr) throws IOException, gnu.text.SyntaxException {
        return read(new gnu.kawa.io.CharArrayInPort(codeStr));
    }

    public String formatStackTrace(Throwable th) {
        // stackTraceFormatter is customizable by user, if he wants.
        return stackTraceFormatter.format(th);
    }

    public EvalResultAndOutput
    evalCatchingOutErr(Environment env, Object sexpr) {
        return evalCatchingOutErr(Language.getDefaultLanguage(), env, sexpr);
    }

    public EvalResultAndOutput
    evalCatchingOutErr(Environment env, String codeStr) {
        return evalCatchingOutErr(Language.getDefaultLanguage(), env, codeStr);
    }


    public EvalResultAndOutput
    evalCatchingOutErr(Language lang, Environment env, Object sexpr) {
        return evalSupplierCatchingOutErr(() -> eval(lang, env, sexpr));
    }

    public EvalResultAndOutput
    evalCatchingOutErr(Language lang, Environment env, String codeStr) {
        return evalSupplierCatchingOutErr(() -> eval(lang, env, codeStr));
    }

    public EvalResultAndOutput
    evalSupplierCatchingOutErr(Supplier<EvalResult> evalSupplier) {
        // This method's parameter is a Supplier so that it can
        // be generalized to work with any `eval...' method.

        OutErrRedirector<EvalResult> redirector = null;
        if (systemOutRedirPolicy.equals(SystemOutRedirectPolicy.REDIRECT)) {
            redirector = new RedirectKawaRedirectSystemout<>();
        } else if (systemOutRedirPolicy.equals(SystemOutRedirectPolicy.BIDIRECT)) {
            redirector = new RedirectKawaBidirectSystemout<>();
        } else if (systemOutRedirPolicy.equals(SystemOutRedirectPolicy.NO_REDIRECT)) {
            redirector = new RedirectKawa<>();
        }

        return new EvalResultAndOutput(
                redirector.callSupplierRedirecting(
                        evalSupplier,
                        isPrettyPrintOutput()
                )
        );
    }

    public EvalResult
    eval(Language lang, Environment env, String codeStr) {
        // This method was originally written to work around the kawa bug that
        // made (makes?) (this) followed by double colon unquoteable and therefore
        // unable to be passed to the eval that takes a sexpr.

        // Reading is an additional point of failure other than execution.
        Throwable throwed;
        EvalResult evalResult;
        try {
            evalResult = eval(lang, env, readString(codeStr));
        } catch (IOException | SyntaxException e) {
            throwed = e;
            if (isPrintStackTrace()) {
                kawa.lib.ports.display(formatStackTrace(e));
            }
            // success=false : if we get here it means reading raised an exception
            evalResult = new EvalResult(
                    false,
                    null,
                    throwed,
                    null);
        }
        return evalResult;
    }

    public EvalResult
    eval(Language lang, Environment env, Object sexpr) {
        // Pasted findMethods evalForm$X in kawa.lang.Eval.java
        PairWithPosition body;
        if (sexpr instanceof PairWithPosition)
            body = new PairWithPosition((PairWithPosition) sexpr,
                    sexpr, LList.Empty);
        else {
            body = new PairWithPosition(sexpr, LList.Empty);
            body.setFile(InPort.evalPathname);
        }
        // These are the data that will be:
        // 1. wrapped in an EvalResult object
        // 2. returned
        boolean success;
        Object result = null;
        Throwable throwed = null;
        SourceMessages messages = new SourceMessages();
        Language saveLang = Language.setSaveCurrent(lang);
        // `prettyPrintOutput' is an instance field, not a local var
        try {
            result = kawa.lang.Eval.evalBody(body, env, messages);
            success = true;
        } catch (Throwable th) {
            success = false;
            throwed = th;
            if (isPrintStackTrace()) {
                kawa.lib.ports.display(formatStackTrace(th));
            }
        } finally {
            Language.restoreCurrent(saveLang);
        }

        return new EvalResult(
                success,
                result,
                throwed,
                messages);
    }

    /*
    // TODO: consider if this should be kept or deleted
    // This uses kawa.Shell.run instead of kawa.lang.Eval.evalBody
    public EvalResult evalToString(Language lang, Environment env, String code) {
        // "Returns the string representation of the result of evaluation."

        // There are 2 reasons we don't use kawa.standard.Scheme.eval:
        // - It doesn't capture warnings
        // - As of 2019-11-09 it has an issue with the ! macro.
        //   For example, this crashes the JVM:
        //     ((kawa.standard.Scheme:getInstance):eval
        //       "(! foobar (java.lang.String))")
        //     ((kawa.standard.Scheme:getInstance):eval
        //       "foobar")
        //
        // Then there is kawa.Shell.run. Differently findMethods
        // kawa.standard.Scheme.eval, kawa.Shell.run does not return the
        // actual value, but stores the result's string representation in a
        // Consumer or in an OutPort. If we really needed the result value
        // we would have to write our own eval-like procedure, but since in
        // Geiser we don't need the actual value of the result but just a
        // representation of it that we can give to emacs, kawa.Shell.run
        // works enough for our purposes.

        // These are the data that will be:
        // 1. wrapped in an EvalResult object
        // 2. returned
        boolean success;
        Object result = null;
        Throwable throwed = null;
        SourceMessages messages = new SourceMessages();
        RedirectedOutErr outerr;

        // Setting up variables needed to run code
        CharArrayInPort inp = new CharArrayInPort(code);
        // RedirectKawaRedirectSystemout.startRedirectingAndSaving(prettyPrintOutput);
        // These are modified by the above line
        OutPort pout = gnu.kawa.io.OutPort.outDefault();
        OutPort perr = gnu.kawa.io.OutPort.errDefault();
        Language saveLang = Language.setSaveCurrent(lang);

        Throwable maybeThrowable;
        try {
            maybeThrowable = kawa.Shell.run(lang, env, inp, pout, perr, messages);
            success = true;
        } catch (Throwable th) {
            success = false;
            throwed = th;
        } finally {
            Language.restoreCurrent(saveLang);
        }
        RedirectKawaRedirectSystemout.startRedirectingAndSaving(prettyPrintOutput);
        outerr = RedirectKawaRedirectSystemout.stopRedirectingAndGetOutErr();

        return new EvalResult(
                success,
                result,
                throwed,
                messages,
                outerr.getOut(),
                outerr.getErr(),
                outerr.getOutAndErrInPrintOrder());

        // // toString() without args just represents the SourceMessages object
        // String msgsToPrint = messages.toString(100000);

        // perr.flush();
        // perr.close();
        // pout.flush();
        // pout.close();

        // String errStr = swerr.toString();
        // String resStr = swout.toString();

        // if (msgsToPrint != null)
        //     kawa.lib.ports.display(msgsToPrint);
        // if (errStr.length() > 0)
        //     kawa.lib.ports.display(errStr);
        // if (maybeThrowable != null)
        //     throw maybeThrowable;

        // return resStr;
    }
    */

    public enum SystemOutRedirectPolicy {
        NO_REDIRECT, REDIRECT, BIDIRECT
    }
}
