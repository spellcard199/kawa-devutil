/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.eval;

import kawadevutil.redirect.result.RedirectedOutErr;
import kawadevutil.redirect.result.RedirectionResult;

public class EvalResultAndOutput extends RedirectionResult<EvalResult> {
    // EvalResultAndOutput is just a RedirectionResult specialized
    // for EvalResult. It's only rationale is to improve the expressiveness
    // of the name of the type.

    public EvalResultAndOutput(EvalResult runnableResult, RedirectedOutErr outErr) {
        super(runnableResult, outErr);
    }

    public EvalResultAndOutput(RedirectionResult<EvalResult> resOutErr) {
        super(resOutErr.getResultOfSupplier(), resOutErr.getOutErr());
    }
}
