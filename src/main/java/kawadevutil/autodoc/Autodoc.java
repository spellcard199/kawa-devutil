/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.autodoc;

import gnu.expr.Language;
import gnu.mapping.Environment;
import kawadevutil.exprtree.CursorFinder;
import kawadevutil.autodoc.exprmatch.DefaultMatchersAutodoc;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class Autodoc {

    public static Optional<AutodocData>
    autodoc(String code, Integer cursorIndex, boolean tryToFixSyntax
    ) throws IOException {
        Language lang = Language.getDefaultLanguage();
        Environment env = Environment.user();
        return autodoc(code, cursorIndex, tryToFixSyntax, lang, env);
    }

    public static Optional<AutodocData>
    autodoc(String code,
            Integer cursorIndex,
            boolean tryToFixSyntax,
            Language lang,
            Environment env
    ) throws IOException {
        return autodoc(
                code, cursorIndex,
                tryToFixSyntax, lang,
                env, DefaultMatchersAutodoc.getDefaultMatchersComplete()
        );
    }

    public static Optional<AutodocData>
    autodoc(String codeStr,
            Integer cursorIndex,
            boolean tryToFixSyntax,
            Language lang,
            Environment env,
            List<Function<CursorFinder, Optional<AutodocData>>>
                    autodocMatchers
    ) throws IOException {
        CursorFinder cursorFinder =
                CursorFinder.make(codeStr, cursorIndex, tryToFixSyntax, lang, env);
        Optional<AutodocData> autodocDataMaybe = Optional.empty();
        for (Function<CursorFinder, Optional<AutodocData>>
                autodocMatcher : autodocMatchers) {
            autodocDataMaybe = autodocMatcher.apply(cursorFinder);
            if (autodocDataMaybe.isPresent()) {
                break;
            }
        }
        return autodocDataMaybe;
    }
}
