/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.autodoc;

import kawadevutil.data.ProcDataGeneric;

public class AutodocData {

    /*
     * Data about:
     * - procedure being called: as ProcDataGeneric
     * - cursor position in procedure arguments
     *
     */

    private ProcDataGeneric procDataGeneric;
    private int cursorArgIndex;
    protected StackTraceElement[] matcherStackTrace;

    public ProcDataGeneric getProcDataGeneric() {
        return procDataGeneric;
    }

    public int getCursorArgIndex() {
        return cursorArgIndex;
    }

    public StackTraceElement[] getMatcherStackTrace() {
        return matcherStackTrace;
    }

    public AutodocData(ProcDataGeneric procDataGeneric,
                       int cursorArgPos,
                       StackTraceElement[] matcherStackTrace) {
        this.procDataGeneric = procDataGeneric;
        this.cursorArgIndex = cursorArgPos;
        this.matcherStackTrace = matcherStackTrace;
    }
}
