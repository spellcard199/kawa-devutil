/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.autodoc.exprmatch;

import kawadevutil.exprtree.CursorFinder;
import kawadevutil.autodoc.AutodocData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;

public class DefaultMatchersAutodoc {

    // A "matcher" is a BiFunction that:
    // - takes:
    //     - CursorFinder
    //     - Function filtering names:
    //         - takes: a name (String)
    //         - returns:
    //             - true : if Field/Method should be included in completion data
    //             - false: if Field/Method should be filtered out
    // - returns:
    //     - If AST matches: ProcDataGeneric
    //     - If AST does not match: Optional.empty().

    public static
    ArrayList
            <Function<CursorFinder, Optional<AutodocData>>>
            defaultMatchersComplete = new ArrayList<>(
            Arrays.asList(
                    MatchAutodoc::matchAutodoc
            )
    );

    public static ArrayList<Function<CursorFinder, Optional<AutodocData>>>
    getDefaultMatchersComplete() {
        return defaultMatchersComplete;
    }

}
