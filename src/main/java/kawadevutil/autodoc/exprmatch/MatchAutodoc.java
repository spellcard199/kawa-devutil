/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.autodoc.exprmatch;

import gnu.expr.ApplyExp;
import gnu.expr.Expression;
import gnu.mapping.Procedure;
import kawadevutil.autodoc.AutodocData;
import kawadevutil.data.ProcDataGeneric;
import kawadevutil.exprtree.CursorFinder;
import kawadevutil.exprtree.ExprWrap;

import java.util.Objects;
import java.util.Optional;

public class MatchAutodoc {

    /*
     *  TODO: should class be renamed?
     *
     */

    private static int getArgIndex(ApplyExp applyExp, Object argToFind) {
        Expression[] args = applyExp.getArgs();
        Integer argIndex = null;
        for (int i = 0; i < args.length; i++) {
            if (args[i] == argToFind) {
                argIndex = i;
                break;
            }
        }
        Objects.requireNonNull(argIndex);
        return argIndex;
    }

    public static Optional<AutodocData>
    matchAutodoc(CursorFinder cursorFinder) {
        ExprWrap cursorWrap = cursorFinder.getCursorMatch().getCursorExprWrap();
        Optional<ExprWrap> parentWrapMaybe = cursorWrap.getParent();
        // Parent should be ApplyExp and ApplyExp.getFunctionValue() should be != null
        boolean isMatch =
                parentWrapMaybe
                        .map(ExprWrap::getExpr)
                        .map(parentElem ->
                                parentElem.getClass().equals(ApplyExp.class)
                                        && ((ApplyExp) parentElem).getFunctionValue() != null)
                        .orElse(false);

        if (isMatch) {
            ApplyExp applyExp = (ApplyExp) parentWrapMaybe.get().getExpr();
            int cursorArgIndex = getArgIndex(applyExp, cursorWrap.getExpr());
            if (applyExp.getFunctionValue() != null) {
                ProcDataGeneric procDataGeneric = ProcDataGeneric
                        .makeForProcedure((Procedure) applyExp.getFunctionValue());
                return Optional.of(
                        new AutodocData(
                                procDataGeneric,
                                cursorArgIndex,
                                Thread.currentThread().getStackTrace()
                        )
                );
            } else {
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }

    }
}
