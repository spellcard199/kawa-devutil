/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.exprtree;

import gnu.expr.*;
import gnu.lists.IString;
import gnu.mapping.Environment;
import gnu.mapping.SimpleSymbol;
import gnu.mapping.Symbol;
import kawadevutil.compile.Compile;
import kawadevutil.compile.CompileResult;
import kawadevutil.compile.ModuleInfoWrap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class CursorFinder {

    // At instantiation `cursorName' is searched.
    // If it's not found an exception is raised.

    private CursorMatch cursorMatch;
    private ExprWrap rootExprWrap;

    public CursorFinder(CursorMatch cursorMatch) {
        this.cursorMatch = cursorMatch;
        this.rootExprWrap = cursorMatch.getCursorExprWrap().getRoot();
    }

    public static CursorFinder make(
            ExprWrap rootExprWrap,
            String cursorPlaceholder) throws IOException {
        Objects.requireNonNull(rootExprWrap);
        Objects.requireNonNull(cursorPlaceholder);
        Optional<CursorMatch> cursorMatchMaybe =
                findCursorInDeclaration(rootExprWrap, cursorPlaceholder);
        if (cursorMatchMaybe.isPresent()) {
            return new CursorFinder(cursorMatchMaybe.get());
        } else {
            // No point in making the object if cursorExprWrap is not found.
            throw new IOException(
                    String.format("cursorName `%s' not found in `%s'",
                            cursorPlaceholder, rootExprWrap.formatElem(true)));
        }
    }

    public static CursorFinder make(
            String codeStr,
            int cursorIndex,
            boolean tryToFixSyntax
    ) throws IOException {
        Language lang = Language.getDefaultLanguage();
        Environment env = Environment.user();
        return make(codeStr, cursorIndex, tryToFixSyntax, lang, env);
    }

    public static CursorFinder make(
            String codeStr,
            int cursorIndex,
            boolean tryToFixSyntax,
            Language lang,
            Environment env
    ) throws IOException {

        if (cursorIndex > codeStr.length()) {
            throw new IllegalArgumentException(
                    "`cursorIndex' cannot be larger than code's size.");
        }

        String cursorPlaceholder = "__cursor_placeholder__";
        String codeStrWithCP =
                insertCursorPlaceholder(codeStr, cursorIndex, cursorPlaceholder);

        // If you try to apply fixes before adding __cursor_placeholder__,
        // you may end with unwanted syntax errors.
        // Example: the parens after the ' is a syntax error:
        //   (static-field some-class ')
        // This is not a syntax error:
        //   (static-field some-class '__cursor_placeholder__)
        String codeStrWithCPAndFixes =
                tryToFixSyntax
                        ? SyntaxFixes.applyFixes(
                                codeStrWithCP, cursorIndex, lang, env)
                        : codeStrWithCP;
        return make(codeStrWithCPAndFixes, cursorPlaceholder, lang, env);
    }

    public static CursorFinder make(
            String codeWithCursorPlaceholder,
            String cursorPlaceholder
    ) throws IOException {
        Language lang = Language.getDefaultLanguage();
        Environment env = Environment.user();
        return make(codeWithCursorPlaceholder, cursorPlaceholder, lang, env);
    }

    public static CursorFinder make(
            String codeWithCursorPlaceholder,
            String cursorPlaceholder,
            Language lang,
            Environment env
    ) throws IOException {

        // This maker method searches in all Expression trees (modules)
        // produced by compilation and sets 'this.rootExprWrap` to the
        // first one that matches.

        CompileResult compileResult =
                Compile.compileToState13(codeWithCursorPlaceholder, lang, env);

        ModuleInfoWrap topMinfoWrapper = compileResult.getMinfoWrap();
        Optional<CursorMatch> cursorMatchMaybe;
        for (ModuleInfoWrap minfoWrapper : topMinfoWrapper.getFlattenedTree()) {
            ExprWrap mexpWrapper = new ExprWrap(
                    minfoWrapper.getModuleExp(),
                    compileResult
            );
            cursorMatchMaybe = findCursorInDeclaration(mexpWrapper, cursorPlaceholder);
            if (cursorMatchMaybe.isPresent()) {
                return new CursorFinder(cursorMatchMaybe.get());
            }
        }
        // No point in making the object if cursorExprWrap is not found.
        throw new IOException(
                String.format("cursorName `%s' not found in `%s'",
                        cursorPlaceholder, codeWithCursorPlaceholder));
    }

    public static String
    insertCursorPlaceholder(
            String codeStr,
            int cursorIndex,
            String cursorPlaceholder) {
        // Insert a placeholder in the sexp that can later be found in the AST.
        return codeStr.substring(0, cursorIndex)
                + cursorPlaceholder
                + codeStr.substring(cursorIndex);
    }

    public ExprWrap getRootExprWrap() {
        return rootExprWrap;
    }

    public CursorMatch getCursorMatch() {
        return cursorMatch;
    }

    public static Optional<CursorMatch>
    findCursorInDeclaration(
            ExprWrap rootWrapper,
            String cursorName) {
        // The reason this is public is so that I can debug issues
        // here from other projects.

        List<ExprWrap> flattenedAst = rootWrapper.getDescendants();
        Optional<CursorMatch> cursorMatch = Optional.empty();

        for (ExprWrap exprWrap : flattenedAst) {

            Object elem = exprWrap.getExpr();
            if (elem.getClass().equals(QuoteExp.class)) {
                cursorMatch = findCursorInQuoteExp(
                        (QuoteExp) elem, cursorName, exprWrap);
            } else if (elem.getClass().equals(ReferenceExp.class)) {
                cursorMatch = findCursorInReferenceExp(
                        (ReferenceExp) elem, cursorName, exprWrap);
            } else if (elem.getClass().equals(Declaration.class)) {
                cursorMatch = findCursorInDeclaration(
                        (Declaration) elem, cursorName, exprWrap);
            }

            if (cursorMatch.isPresent()) {
                break;
            }
        }
        return cursorMatch;
    }

    private static Optional<CursorMatch>
    findCursorInReferenceExp(ReferenceExp refExp,
                             String cursorName,
                             ExprWrap exprWrap) {

        String declName = refExp.getBinding().getName();
        if (declName != null && declName.contains(cursorName)) {
            return Optional.of(
                    new CursorMatch(exprWrap, cursorName, declName));
        } else {
            return Optional.empty();
        }
    }

    private static Optional<CursorMatch> findCursorInQuoteExp(
            QuoteExp quoteExp,
            String cursorName,
            // TODO: refactor bad design: exprWrap already contains quoteExp in elem field
            ExprWrap exprWrap) {
        // In this procedure we expect cursorExprWrap to be a value inside a
        // QuoteExp that can be either:
        // - a SimpleSymbol
        // - a java.lang.String

        Optional<CursorMatch> cursorMatch = Optional.empty();

        Object value = quoteExp.getValue();

        if (value == null) {
            return Optional.empty();

        } else if (value.getClass() == SimpleSymbol.class) {
            // If Kawa is not using reflection, value is a
            // gnu.mapping.SimpleSymbol
            String valueAsString = ((SimpleSymbol) value).getName();
            if (valueAsString.contains(cursorName)) {
                cursorMatch = Optional.of(new CursorMatch(exprWrap, cursorName, valueAsString));
            }

        } else if (value.getClass() == IString.class) {
            String valueAsString = ((IString) value).toString();
            if (valueAsString.contains(cursorName)) {
                cursorMatch = Optional.of(
                        new CursorMatch(exprWrap, cursorName, valueAsString));
            }

        } else if (value.getClass() == String.class) {
            // If Kawa is using reflection value is a
            // java.lang.String
            String valueAsString = (String) value;
            // is... and get... get inserted by Kawa when using reflection,
            // so it's not the symbol that was inserted by the user.
            if (valueAsString.contains(cursorName)
                    &&
                    !(valueAsString.startsWith("is" + cursorName)
                            // TODO: why did I put a not (!) here?
                            || valueAsString.startsWith("get" + cursorName))) {
                cursorMatch = Optional.of(new CursorMatch(exprWrap, cursorName, valueAsString));
            }
        }

        return cursorMatch;
    }

    private static List<Declaration> getModuleExpDecls(ModuleExp mexp) {
        // We want to find cursor in code, not in module's declarations.
        ArrayList<Declaration> decls = new ArrayList<>();
        for (Declaration d = mexp.firstDecl(); d != null; d = d.nextDecl()) {
            decls.add(d);
            // HS: not even reached
        }
        return decls;
    }

    private static Optional<CursorMatch> findCursorInDeclaration(
            Declaration decl,
            String cursorName,
            // TODO: refactor bad design: exprWrapper already contains decl in elem field
            ExprWrap exprWrap) {

        Optional<CursorMatch> cursorMatch = Optional.empty();

        // Do not match cursor when in the list of ModuleExp's Declarations.
        // We are only interested to cursor in the body of the ModuleExp.
        boolean cond1 = !exprWrap.getParent()
                .map(ExprWrap::getExpr)
                .map(parentElem ->
                        parentElem.getClass().equals(ModuleExp.class)
                                &&
                                getModuleExpDecls((ModuleExp) parentElem).contains(decl)
                ).orElse(false);

        // If decl's symbol is null it's not our cursor.
        if (cond1 && decl.getSymbol() != null) {
            // decl.getSymbol() can return either a java.lang.Object or a
            // gnu.mapping.Symbol (it's written in the comment for the
            // field `symbol' in kawa's Declaration.java source file)
            Object declSym = decl.getSymbol();
            String symName;
            if (declSym.getClass() == String.class) {
                symName = (String) declSym;
            } else if (Symbol.class.isAssignableFrom(declSym.getClass())) {
                symName = ((Symbol) declSym).getName();
            } else {
                throw new Error("Bug spotted");
            }
            if (symName.contains(cursorName)) {
                cursorMatch = Optional.of(new CursorMatch(exprWrap, cursorName, symName));
            }
        }

        return cursorMatch;
    }

}
