/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.exprtree;

import gnu.expr.Language;
import gnu.kawa.io.CharArrayInPort;
import gnu.kawa.lispexpr.LispLanguage;
import gnu.kawa.lispexpr.LispReader;
import gnu.kawa.lispexpr.ReaderParens;
import gnu.mapping.Environment;
import gnu.text.SourceMessages;
import gnu.text.SyntaxException;

import java.io.IOException;

public class SyntaxFixes {

    public static String applyFixes(
            String codeStr, int cursorIndex,
            Language lang, Environment env) {
        if (lang instanceof LispLanguage) {
            return applyLispLanguageFix(
                    codeStr, cursorIndex, (LispLanguage) lang, env);
        } else {
            return codeStr;
        }
    }

    public static String applyLispLanguageFix(
            String codeStr, int cursorIndex,
            LispLanguage lang, Environment env) {

        return lispLanguageAddMissingClosingParentheses(codeStr, lang);
    }

    public static String lispLanguageAddMissingClosingParentheses(
            String codeStr, LispLanguage lang) {

        CharArrayInPort caip = new CharArrayInPort(codeStr);
        SourceMessages messages = new SourceMessages();
        LispReader lispReader = new LispReader(caip, messages);

        StringBuilder newCodeSB = new StringBuilder(codeStr);
        for (; ; ) {
            try {
                Object x = ReaderParens.readList(
                        lispReader, null, 0, 1, -1, -1);
                break;
            } catch (SyntaxException se) {
                newCodeSB.append(')');
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
        return newCodeSB.toString();

    }
}
