/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.exprtree;

public class CursorMatch {

    // QuoteExp, Declaration, ...
    ExprWrap cursorExprWrap;
    String beforeCursor;
    String cursorName;
    String afterCursor;

    CursorMatch(ExprWrap cursorExprWrap, String cursorName, String stringContainingCursor) {

        this.cursorExprWrap = cursorExprWrap;
        this.cursorName = cursorName;
        String[] splitted = stringContainingCursor.split(cursorName);
        this.beforeCursor = splitted.length > 0
                ? splitted[0]
                : "";
        this.afterCursor = splitted.length > 1
                ? splitted[1]
                : "";

        // Validating data to protect me against myself.
        if (!stringContainingCursor.contains(cursorName)) {
            throw new Error(String.format(
                    "`stringContainingCursor does not contain `cursorName':\n"
                            + "- stringContainingCursor: %s"
                            + "- cursorName: %s",
                    stringContainingCursor,
                    cursorName));
        } else if (this.cursorExprWrap == null) {
            throw new Error("this.cursorExprWrap object can't be null.");
        } else if (this.beforeCursor == null) {
            throw new Error("this.beforeCursor can't be null.");
        } else if (this.afterCursor == null) {
            throw new Error("this.afterCursor can't be null.");
        }
    }

    public ExprWrap getCursorExprWrap() {
        return cursorExprWrap;
    }

    public String getBeforeCursor() {
        return beforeCursor;
    }

    public String getCursorName() {
        return cursorName;
    }

    public String getAfterCursor() {
        return afterCursor;
    }
}
