/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.exprtree;

import gnu.expr.*;
import gnu.kawa.functions.Format;
import gnu.kawa.io.OutPort;
import gnu.mapping.SimpleSymbol;
import kawadevutil.reflect.Reflecting;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

public class ExprUtil {

    // Note: what constitutes a child or if some "child" should be included
    // or not is pretty arbitrary atm. I'm adding things as I need them.

    public static List<Object> getChildren(Object elem) {

        // elem can be a subclass of either Expression or
        // Declaration. Declaration is not a subclass of Expression.

        if (ApplyExp.class.isAssignableFrom(elem.getClass())) {
            // We use isAssignableFrom to catch also various kawa functions
            // implemented in java (e.g. gnu.kawa.functions.GetNamedPart)
            return getChildrenOfApplyExp((ApplyExp) elem);
        } else if (ClassExp.class.isAssignableFrom(elem.getClass())) {
            // Catches both ClassExp and ObjectExp, its subclass.
            // This must come before LambdaExp, which being a superclass of
            // ClassExp would also match with is (*:isAssignableFrom ...)
            // clause.
            return getChildrenOfClassExp((ClassExp) elem);
        } else if (ScopeExp.class.isAssignableFrom(elem.getClass())) {
            // Catches LetExp and LambdaExp subclasses. ModuleExp is also
            // catched because it's a subclass of LambdaExp.
            return getChildrenOfScopeExp((ScopeExp) elem);
        } else if (QuoteExp.class == elem.getClass()) {
            return getChildrenOfQuoteExp((QuoteExp) elem);
        } else if (Declaration.class == elem.getClass()) {
            return getChildrenOfDeclaration((Declaration) elem);
        } else if (BeginExp.class == elem.getClass()) {
            return getChildrenOfBeginExp((BeginExp) elem);
        } else if (ReferenceExp.class == elem.getClass()) {
            return getChildrenOfReferenceExp((ReferenceExp) elem);
        } else if (SetExp.class == elem.getClass()) {
            return getChildrenOfSetExp((SetExp) elem);
        } else if (String.class == elem.getClass()) {
            throw new Error(
                    "OK, VERIFIED: "
                            + "IT'S ACTUALLY POSSIBLE TO GET HERE. "
                            + "ADJUST CODE ACCORDINGLY."
            );
        } else if (SimpleSymbol.class == elem.getClass()) {
            return getChildrenOfSimpleSymbol((SimpleSymbol) elem);
        } else if (IfExp.class == elem.getClass()) {
            return getChildrenOfIfExp((IfExp) elem);
        } else if (TryExp.class == elem.getClass()) {
            return getChildrenOfTryExp((TryExp) elem);
        } else if (CaseExp.class == elem.getClass()) {
            return getChildrenOfCaseExp((CaseExp) elem);
        } else if (CaseExp.CaseClause.class == elem.getClass()) {
            return getChildrenOfCaseClause((CaseExp.CaseClause) elem);
        } else if (ThisExp.class == elem.getClass()) {
            return getChildrenOfThisExp((ThisExp) elem);
        } else if (ErrorExp.class == elem.getClass()) {
            return getChildrenOfErrorExp((ErrorExp) elem);
        } else {
            throw new Error("[getChildren] Not implemented for type: " + elem.getClass().toString());
        }

        // TODO :
        // ThisExp
        // SynchronizedExp
        // BlockExp
        // ExitExp
        // FluidLetExp
        // LangExp
    }

    public static List<Object> getChildrenOfApplyExp(ApplyExp elem) {
        ArrayList<Object> children = new ArrayList<>();
        children.add(elem.getFunction());
        children.addAll(java.util.Arrays.asList(elem.getArgs()));
        return children;
    }

    public static List<Object> getChildrenOfClassExp(ClassExp elem) {
        ArrayList<Object> supers = new ArrayList<>(java.util.Arrays.asList(elem.supers));
        ArrayList<Object> classChildren = new ArrayList<>();
        for (LambdaExp child = elem.firstChild; child != null; child = child.nextSibling) {
            classChildren.add(child);
        }
        ArrayList<Object> children = new ArrayList<>();
        children.addAll(supers);
        children.addAll(classChildren);
        return children;
    }

    public static List<Object> getChildrenOfScopeExp(ScopeExp scopeExp) {
        ArrayList<Object> children = new ArrayList<>();
        for (Declaration decl = scopeExp.firstDecl(); decl != null; decl = decl.nextDecl()) {
            children.add(decl);
        }

        // ScopeExp, which is the immediate superclass of both LetExp and LambdaExp, does not
        // have a `body' field, so we have to cast for each subclass.
        Expression body = null;
        Class declClass = scopeExp.getClass();
        if (declClass == LetExp.class) {
            body = ((LetExp) scopeExp).getBody();
        } else if (LambdaExp.class.isAssignableFrom(declClass)) {
            body = ((LambdaExp) scopeExp).body;
        }
        if (body != null) {
            children.add(body);
        }

        return children;
    }

    public static List<Object> getChildrenOfQuoteExp(QuoteExp elem) {
        return Collections.emptyList();
    }

    public static List<Object> getChildrenOfDeclaration(Declaration elem) {
        Expression initValue = elem.getInitValue();
        return (initValue != null)
                ? Collections.singletonList(initValue)
                : Collections.emptyList();
    }

    public static List<Object> getChildrenOfBeginExp(BeginExp elem) {
        // actually ArrayList<Expression>, but all these
        // getChildren methods should have the same signature.
        ArrayList<Object> children = new ArrayList<>();
        for (Expression e : elem.getExpressions()) {
            if (e != null) {
                children.add(e);
            }
        }
        return children;
    }

    public static List<Object> getChildrenOfReferenceExp(ReferenceExp elem) {
        return Collections.emptyList();
    }

    public static List<Object> getChildrenOfSetExp(SetExp elem) {
        return Arrays.asList(elem.getBinding(), elem.getNewValue());
    }

    public static List<Object> getChildrenOfSimpleSymbol(SimpleSymbol elem) {
        return Collections.emptyList();
    }

    public static List<Object> getChildrenOfIfExp(IfExp elem) {
        Expression test = elem.getTest();
        Expression thenClause = elem.getThenClause();
        Expression elseClause = elem.getElseClause();

        ArrayList<Object> children = new ArrayList<>();
        children.add(test);
        children.add(thenClause);
        if (elseClause != null) {
            // elseClause is optional, so it can be #!null
            children.add(elseClause);
        }
        return children;
    }

    public static List<Object> getChildrenOfTryExp(TryExp elem) {
        // TryExp class does not have a public getter for the
        // provate field `try_clause'. We have to modify access
        // permissions using reflection.

        ArrayList<Object> children = new ArrayList<>();

        Reflecting<TryExp> reflectOnTryExp = new Reflecting<>();
        try {
            children.add(reflectOnTryExp.getDeclaredFieldValue(elem, "try_clause"));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        CatchClause catchClauses = elem.getCatchClauses();
        Expression finallyClause = elem.getFinallyClause();

        // Get catch clauses from the CatchClause object
        // and put them into a list.
        ArrayList<Expression> clauseExprList = new ArrayList<>();
        clauseExprList.add(catchClauses.getBody());

        for (CatchClause cc = catchClauses.getNext();
             cc != null;
             cc = cc.getNext()) {
            clauseExprList.add(cc.getBody());
        }

        children.addAll(clauseExprList);
        if (finallyClause != null) {
            children.add(finallyClause);
        }
        return children;
    }

    public static List<Object> getChildrenOfCaseExp(CaseExp elem) {
        ArrayList<Object> children = new ArrayList<>();
        Reflecting<CaseExp.CaseClause[]> reflectOnClause = new Reflecting<CaseExp.CaseClause[]>();
        try {
            children.add(reflectOnClause.getDeclaredFieldValue(elem, "clauses"));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return children;
    }

    public static List<Object> getChildrenOfCaseClause(CaseExp.CaseClause elem) {
        Reflecting<Expression[]> reflectOnDatum = new Reflecting<Expression[]>();
        ArrayList<Object> children = new ArrayList<>();
        try {
            children.add(reflectOnDatum.getDeclaredFieldValue(elem, "datums"));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        Reflecting<Expression> reflectOnExp = new Reflecting<Expression>();
        Optional<Expression> exp = Optional.empty();
        try {
            children.add(reflectOnExp.getDeclaredFieldValue(elem, "exp"));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return children;
    }

    public static List<Object> getChildrenOfThisExp(ThisExp elem) {
        return Collections.emptyList();
    }

    public static List<Object> getChildrenOfErrorExp(ErrorExp elem) {
        // TODO: see if this ir right;
        return Collections.emptyList();
    }

    public static String formatElem(
            Object elem,
            Optional<Object> parentMaybe,
            boolean pretty) {
        // If expr is Declaration and parent's class is SetExp the result
        // changes the same way kawa prints it.

        // Output port constructor with pretty printing and autoflush:
        // public OutPort(Writer base, boolean printPretty, boolean autoflush)

        StringWriter sw = new StringWriter();
        OutPort op = new OutPort(sw, pretty, true);

        // kawa.lib.ports.display needs a default Language to be set or else it will raise an exception.
        Optional<Language> saveLangMaybe = Optional.empty();
        if (Language.getDefaultLanguage() == null) {
            saveLangMaybe = Optional.ofNullable(Language.setSaveCurrent(new kawa.standard.Scheme()));
        }

        try {
            if (Expression.class.isAssignableFrom(elem.getClass())) {
                // If expr is subclass of Expression, use `print' method
                ((Expression) elem).print(op);
            } else if (Declaration.class.isAssignableFrom(elem.getClass())) {
                // If expr is a Declaration and has a SetExp as parent it
                // has a different formatting than if the parent was a
                // different subclass of Expression
                Declaration decl = (Declaration) elem;
                boolean isChildOfSetExp =
                        parentMaybe.isPresent()
                                && parentMaybe.get().getClass() == SetExp.class
                                && ((SetExp) parentMaybe.get()).getBinding() == elem;
                if (isChildOfSetExp) {
                    // TODO: does this also work?: DisplayFormat.schemeDisplayFormat.write
                    kawa.lib.ports.display("/", op);
                    kawa.lib.ports.display(Format.format("~A", decl), op);
                } else {
                    kawa.lib.ports.display("(", op);
                    decl.printInfo(op);
                    kawa.lib.ports.display(" = ", op);
                    kawa.lib.ports.display(
                            Format.format("~A", decl.getInitValue()), op);
                    kawa.lib.ports.display(")", op);
                }
            }
        } finally {
            saveLangMaybe.ifPresent(Language::restoreCurrent);
        }

        op.flush();
        op.close();
        try {
            sw.close();
        } catch (IOException e) {
            throw new Error(e);
        }
        return sw.toString();
    }
}
