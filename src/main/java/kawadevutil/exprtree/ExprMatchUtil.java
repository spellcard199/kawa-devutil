/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.exprtree;

import gnu.expr.*;
import gnu.kawa.reflect.Invoke;
import gnu.mapping.Procedure;
import gnu.mapping.Symbol;

import java.util.Optional;

public class ExprMatchUtil {

    /*
     *  Utility functions that take an AST or a part of it and return a result.
     */

    public static boolean
    isApplyExp(ExprWrap exprWrap) {
        return exprWrap.getExpr().getClass().equals(ApplyExp.class);
    }

    public static boolean
    isApplyToArgs(ExprWrap exprWrap) {
        boolean result = false;
        if (isApplyExp(exprWrap)) {
            Expression func = ((ApplyExp) exprWrap.getExpr()).getFunction();
            // func can also be a QuoteExp
            if (func.getName() != null
                    && func.getName().equals("applyToArgs")) {
                result = true;
            }
        }
        ;
        return result;
    }

    public static boolean
    isParentApplyToArgs(ExprWrap exprWrap) {
        return exprWrap
                .getParent()
                .map(ExprMatchUtil::isApplyToArgs)
                .orElse(false);
    }

    public static boolean
    isQuoteExp(ExprWrap exprWrap) {
        return exprWrap.getExpr().getClass().equals(QuoteExp.class);
    }

    public static boolean
    isQuoteExpWithValueClassEqualTo(ExprWrap exprWrap, Class clz) {
        return isQuoteExp(exprWrap)
                && ((QuoteExp) exprWrap.getExpr()).getValue().getClass().equals(clz);
    }

    public static boolean
    isReferenceExp(ExprWrap exprWrap) {
        return exprWrap.getExpr().getClass().equals(ReferenceExp.class);
    }

    public static Class
    getReferenceExpBindingClass(ReferenceExp refExp) {
        return refExp.getBinding().getType().getReflectClass();
    }

    public static boolean
    isReferenceExpValueNull(ReferenceExp refExp) {
        return refExp.getBinding().getValue() == null;
    }

    public static boolean
    isReferenceExpWithBindingClass(ExprWrap exprWrap, Class clz) {
        return isReferenceExp(exprWrap)
                && getReferenceExpBindingClass((ReferenceExp) exprWrap.getExpr()).equals(clz);
    }

    public static boolean
    isGetNamedExp(ExprWrap exprWrap) {
        // GetNamedExp is not accessible, comparing strings instead.
        return exprWrap.getExpr().getClass().getName().equals("gnu.kawa.functions.GetNamedExp");
    }

    public static boolean
    isGrandParentApplyToArgs(CursorFinder cursorFinder) {
        return cursorFinder.getCursorMatch().getCursorExprWrap()
                .getParent().flatMap(ExprWrap::getParent)
                .map(ExprMatchUtil::isApplyToArgs).orElse(false);
    }

    public static boolean
    isCursorInApplyOfGetSlotValue(CursorFinder cursorFinder) {
        ExprWrap cursorWrap = cursorFinder.getCursorMatch().getCursorExprWrap();
        Optional<ExprWrap> parentWrapperMaybe = cursorWrap.getParent();
        Optional<ExprWrap> firstSiblingWrapperMaybe = cursorWrap.getFirstSibling();
        Optional<ExprWrap> previousSiblingWrapperMaybe = cursorWrap.getPreviousSibling();

        boolean cond1 = parentWrapperMaybe.map(ExprMatchUtil::isApplyExp).orElse(false);
        boolean cond2 = firstSiblingWrapperMaybe
                .map(exprWrap ->
                        isQuoteExpWithValueClassEqualTo(exprWrap, PrimProcedure.class))
                .orElse(false);
        boolean cond3 = previousSiblingWrapperMaybe
                .map(exprWrap ->
                        isQuoteExpWithValueClassEqualTo(exprWrap, Class.class))
                .orElse(false);
        return cond1 && cond2 && cond3;
    }

    public static Optional<Class>
    getClassForGetSlotValue(CursorFinder cursorFinder) {
        Optional<ExprWrap> prevSiblingWrap =
                cursorFinder.getCursorMatch().getCursorExprWrap().getPreviousSibling();
        return prevSiblingWrap.map(
                exprWrap -> (Class) ((QuoteExp) exprWrap.getExpr()).getValue());
    }

    public static boolean
    isCursorOnColonNotationInstance(CursorFinder cursorFinder) {
        // TODO: this is old. Express this in terms of the other methods.
        //  Name is the worst thing: not objective and wrong.
        ExprWrap cursorWrapper = cursorFinder.getCursorMatch().getCursorExprWrap();
        Optional<ExprWrap> parentWrapperMaybe = cursorWrapper.getParent();
        Optional<ExprWrap> firstSiblingWrapperMaybe = cursorWrapper.getFirstSibling();
        Optional<ExprWrap> previousSiblingWrapperMaybe = cursorWrapper.getPreviousSibling();

        boolean cond1 = parentWrapperMaybe.map(ExprMatchUtil::isGetNamedExp).orElse(false);
        boolean cond2 = firstSiblingWrapperMaybe.map(exprWrap ->
                isQuoteExpWithValueClassEqualTo(exprWrap, gnu.kawa.functions.GetNamedPart.class))
                .orElse(false);
        // Wrong. Doesn't work. Previous sibling can be at least ReferenceExp, ApplyExp, QuoteExp
        // boolean cond3 = previousSiblingWrapperMaybe.map(ps -> isReferenceExp(px)).orElse(false);
        // Keeping laxer conditions for now. Let's see how this goes.
        return cond1 && cond2;

    }

    public static boolean isApplyingProcOfClass(ApplyExp a,
                                                Class procClass) {

        Expression appliedFunc = a.getFunction();
        // TODO : this matches only a subset of the cases. Extend as needed.
        //  I only used tested this method in completion support for
        //  `invoke', `invonke-static', `slot-ref', `static-field'

        if (appliedFunc.getClass().equals(ReferenceExp.class)) {
            Expression refValue =
                    ((ReferenceExp) appliedFunc)
                            .getBinding()
                            .getValue();
            Object quoteValue = ((QuoteExp) refValue).getValue();
            if (quoteValue.getClass().equals(procClass)) {
                return true;
            } else {
                return false;
            }
        } else if (appliedFunc.getClass().equals(QuoteExp.class)) {
            Object quoteValue = ((QuoteExp) appliedFunc).getValue();

            if (quoteValue.getClass().equals(PrimProcedure.class)) {
                PrimProcedure primProc = (PrimProcedure) quoteValue;
                /*
                System.out.println("vvvv");
                System.out.println(procClass);
                System.out.println(primProc.getDeclaringClass().getReflectClass());
                System.out.println(primProc.getDeclaringClass().getReflectClass().equals(procClass));
                System.out.println("^^^^");
                 */
                return primProc.getDeclaringClass().getReflectClass().equals(procClass);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean
    isParentElemApplyExpWithFuncOfClassWithName(ExprWrap astWrap,
                                                Class procClass) {
        return astWrap.getParent()
                .flatMap((ExprWrap w) ->
                        w.getExpr().getClass().equals(ApplyExp.class)
                                ? Optional.of((ApplyExp) w.getExpr())
                                : Optional.empty())
                .map((ApplyExp a) ->
                        ExprMatchUtil.isApplyingProcOfClass
                                (a, procClass))
                .orElse(false);
    }

    public static Optional<Object>
    getPreviousSiblingValueInsideExpr(CursorFinder cursorFinder) {
        return cursorFinder
                .getCursorMatch()
                .getCursorExprWrap()
                .getPreviousSibling()
                .map(previousSiblingWrapper ->
                        getValueOfObjInsideExp(previousSiblingWrapper.getExpr()));
    }

    public static Object
    getValueOfObjInsideExp(ReferenceExp refExp) {
        return refExp.getBinding().getValue();
    }

    public static Object
    getValueOfObjInsideExp(QuoteExp quoteExp) {
        return quoteExp.getValue();
    }

    public static Object
    getValueOfObjInsideExp(Object declOrExpr) {
        Class classOfContainerExpr = declOrExpr.getClass();
        Object value = null;
        if (classOfContainerExpr.equals(ReferenceExp.class)) {
            value = getValueOfObjInsideExp((ReferenceExp) declOrExpr);
        } else if (classOfContainerExpr.equals(QuoteExp.class)) {
            value = getValueOfObjInsideExp((QuoteExp) declOrExpr);
        } else if (classOfContainerExpr.equals(ApplyExp.class)) {
            value = getValueOfObjInsideExp((Object) declOrExpr);
        }
        return value;
    }

    public static Optional<String>
    getSymbolNameInsideExpr(Object declOrExpr) {
        Class classOfContainerExpr = declOrExpr.getClass();
        String symName = null;
        if (classOfContainerExpr.equals(ReferenceExp.class)) {
            Symbol sym = (Symbol) ((ReferenceExp) declOrExpr).getSymbol();
            if (sym != null) {
                symName = sym.getName();
            }
        } else if (classOfContainerExpr.equals(QuoteExp.class)) {
            Symbol sym = (Symbol) ((QuoteExp) declOrExpr).getSymbol();
            if (sym != null) {
                symName = sym.getName();
            }
        }
        return symName != null
                ? Optional.of(symName)
                : Optional.empty();
    }

    public static Optional<Class>
    getPreviousSiblingTypeReflectClass(CursorFinder cursorFinder) {
        Optional<ExprWrap> previousSiblingWrapMaybe = cursorFinder
                .getCursorMatch()
                .getCursorExprWrap()
                .getPreviousSibling();
        return previousSiblingWrapMaybe
                .map(exprWrap ->
                {
                    Object expr = exprWrap.getExpr();
                    if (ScopeExp.class.isAssignableFrom(expr.getClass())) {
                        return ((ScopeExp) expr).getType().getReflectClass();
                    } else {
                        return getClassOfObjInsideExp(expr);
                    }
                });
    }

    public static Class
    getClassOfObjInsideExp(ReferenceExp refExp) {
        return refExp.getBinding().getType().getReflectClass();
    }

    public static Class
    getClassOfObjInsideExp(QuoteExp quoteExp) {
        return quoteExp.getValue().getClass();
    }

    public static Class
    getClassOfObjInsideExp(ApplyExp applyExp) {
        return applyExp.getType().getReflectClass();
    }

    public static Class getClassOfObjInsideExp(Object declOrExpr) {
        Class classOfContainerExpr = declOrExpr.getClass();
        Class classForCompletion = null;
        if (classOfContainerExpr.equals(ReferenceExp.class)) {
            classForCompletion = getClassOfObjInsideExp((ReferenceExp) declOrExpr);
        } else if (classOfContainerExpr.equals(QuoteExp.class)) {
            classForCompletion = getClassOfObjInsideExp((QuoteExp) declOrExpr);
        } else if (classOfContainerExpr.equals(ApplyExp.class)) {
            classForCompletion = getClassOfObjInsideExp((ApplyExp) declOrExpr);
        }
        return classForCompletion;
    }

    public static boolean
    isParentApplyExp(CursorFinder cursorFinder) {
        return cursorFinder.getCursorMatch()
                .getCursorExprWrap()
                .getParent().map(
                        parentExp -> ApplyExp.class.isAssignableFrom(
                                parentExp.getExpr().getClass()))
                .orElse(false);
    }

    public static boolean
    isFirstSiblingQuotedInvoke(CursorFinder cursorFinder) {
        ExprWrap cursorWrap = cursorFinder.getCursorMatch().getCursorExprWrap();
        Optional<ExprWrap> firstSiblingMaybe = cursorWrap.getFirstSibling();
        return firstSiblingMaybe
                .flatMap(exprWrap ->
                        {
                            if (exprWrap.getExpr().getClass().equals(QuoteExp.class)) {
                                return Optional.of((QuoteExp) exprWrap.getExpr());
                            } else {
                                return Optional.empty();
                            }
                        }
                ).flatMap((QuoteExp quoteExp) ->
                        {
                            Object v = quoteExp.getValue();
                            if (v.getClass().equals(Invoke.class)) {
                                return Optional.of(true);
                            } else {
                                return Optional.of(false);
                            }
                        }
                ).orElse(false);
    }

    public static boolean
    isSecondSiblingAQuotedClass(CursorFinder cursorFinder) {
        ExprWrap cursorWrap = cursorFinder.getCursorMatch().getCursorExprWrap();
        Optional<ExprWrap> secondSiblingMaybe
                = cursorWrap
                .getFirstSibling()
                .flatMap(exprWrap -> exprWrap.getNextSibling());
        return secondSiblingMaybe
                .flatMap(exprWrap ->
                        {
                            if (exprWrap.getExpr().getClass().equals(QuoteExp.class)) {
                                return Optional.of((QuoteExp) exprWrap.getExpr());
                            } else {
                                return Optional.empty();
                            }
                        }
                ).flatMap((QuoteExp quoteExp) ->
                        {
                            Object v = quoteExp.getValue();
                            if (v.getClass().equals(Class.class)) {
                                return Optional.of(true);
                            } else {
                                return Optional.of(false);
                            }
                        }
                ).orElse(false);
    }

    public static Optional<Object>
    getSecondSiblingQuotedValue(CursorFinder cursorFinder) {
        ExprWrap cursorWrap = cursorFinder.getCursorMatch().getCursorExprWrap();
        Optional<ExprWrap> secondSiblingMaybe
                = cursorWrap
                .getFirstSibling()
                .flatMap(exprWrap -> exprWrap.getNextSibling());
        return secondSiblingMaybe
                .flatMap(exprWrap ->
                        {
                            if (exprWrap.getExpr().getClass().equals(QuoteExp.class)) {
                                return Optional.of((QuoteExp) exprWrap.getExpr());
                            } else {
                                return Optional.empty();
                            }
                        }
                ).flatMap((QuoteExp quoteExp) -> Optional.of(quoteExp.getValue()));

    }

    public static boolean
    isParentApplyOfInvoke(CursorFinder cursorFinder, boolean forStatic) {

        Procedure invokeProc = forStatic
                ? Invoke.invokeStatic
                : Invoke.invoke;

        boolean isMatch = cursorFinder.getCursorMatch()
                .getCursorExprWrap()
                .getParent()
                .flatMap((ExprWrap wrap) ->
                {
                    return wrap.getExpr().getClass().equals(ApplyExp.class)
                            ? Optional.of((ApplyExp) wrap.getExpr())
                            : Optional.empty();
                })
                .flatMap((ApplyExp applyExp) ->
                {
                    Expression f = applyExp.getFunction();
                    return f.getClass().equals(ReferenceExp.class)
                            ? Optional.of((ReferenceExp) f)
                            : Optional.empty();
                })
                .flatMap((ReferenceExp refExp) -> {
                    Expression v = refExp.getBinding().getValue();
                    return v.getClass().equals(QuoteExp.class)
                            ? Optional.of(((QuoteExp) v).getValue())
                            : Optional.empty();
                })
                .map(o -> o.equals(invokeProc))
                .orElse(false);

        return isMatch;
    }

    public static boolean
    isParentApplyOfSlotGet(CursorFinder cursorFinder, boolean forStatic) {

        boolean isMethodGetSlotValue = cursorFinder.getCursorMatch()
                .getCursorExprWrap()
                .getParent()
                .flatMap((ExprWrap wrap) ->
                {
                    return wrap.getExpr().getClass().equals(ApplyExp.class)
                            ? Optional.of((ApplyExp) wrap.getExpr())
                            : Optional.empty();
                })
                .flatMap((ApplyExp applyExp) ->
                {
                    Expression f = applyExp.getFunction();
                    return f.getClass().equals(QuoteExp.class)
                            && ((QuoteExp) f).getValue().getClass().equals(PrimProcedure.class)

                            ? Optional.of((PrimProcedure) ((QuoteExp) f).getValue())
                            : Optional.empty();
                })
                .map((PrimProcedure primProc) -> {
                    boolean isMethodClassSlotGet =
                            primProc
                                    .getMethod()
                                    .getDeclaringClass()
                                    .getReflectClass()
                                    .equals(gnu.kawa.reflect.SlotGet.class);
                    boolean isMethodNameGetSlotValue =
                            primProc.getMethod().getName().equals("getSlotValue");
                    return isMethodClassSlotGet && isMethodNameGetSlotValue;
                })
                .orElse(false);

        // isStaticArgMatch is true if both are true:
        // 1. There is a boolean arg to slotget
        // 2. That argument is equal to `forStatic'
        boolean isStaticArgMatch = cursorFinder
                .getCursorMatch()
                .getCursorExprWrap()
                .getFirstSibling()
                .flatMap(x -> x.getNextSibling())
                .map(x -> x.getExpr())
                .flatMap(elem -> elem.getClass().equals(QuoteExp.class)
                        ? Optional.of(((QuoteExp) elem).getValue())
                        : Optional.empty()
                ).flatMap((Object v) -> v.getClass().equals(Boolean.class)
                        ? Optional.of((Boolean) v.equals(forStatic))
                        : Optional.empty()
                ).orElse(false);

        return isMethodGetSlotValue && isStaticArgMatch;
    }
}
