/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.exprtree;

import gnu.expr.Language;
import gnu.mapping.Environment;
import kawadevutil.compile.Compile;
import kawadevutil.compile.CompileResult;

import java.io.IOException;
import java.time.temporal.ValueRange;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ExprWrap {

    /*
     *  Wrapper for an element in Kawa's Expression tree.
     *
     *  expr can be:
     *  - gnu.expr.Expression
     *  - gnu.expr.Declaration
     *  - java.lang.String
     *  - gnu.mapping.Symbol, gnu.mapping.SimpleSymbol
     *  - java.lang.String
     *  - ..??
     *  It's up to what comes downstream to do reflection-based
     *  type casting if necessary.
     */

    private Object expr;
    private ExprWrap parent;
    private List<ExprWrap> children = new ArrayList<>();
    // It's useful to bring along the compilation that produced the AST
    public CompileResult compileResult;

    // posFromTom: number of expressions before `expr', reading from top to
    // bottom. In other words, the sum of:
    // - ancestors
    // - previous siblings
    private int posFromTop;
    // depth: number of ancestors
    private int depth;

    // These strReprBounds... fields contain the ranges where the
    // element appears in the string representation of the parent and
    // the root when printed:
    //   - on a single line
    //   - without any prettification
    // I'm using class java.time.temporal.ValueRange just to have a
    // pair type builtin in java. It's not like these have anything to
    // do with time measurement.
    private Optional<ValueRange> strReprBoundsInParent = Optional.empty();
    private Optional<ValueRange> strReprBoundsInRoot = Optional.empty();

    private void sharedInit(ExprWrap parent,
                            Object elem,
                            CompileResult compileResult) {
        this.parent = parent;
        this.expr = elem;
        this.compileResult = compileResult;
        for (Object childExpOrDecl : ExprUtil.getChildren(this.expr)) {
            ExprWrap wrappedChild =
                    new ExprWrap(childExpOrDecl, this);
            this.children.add(wrappedChild);
        }
    }

    public ExprWrap(CompileResult compileResult) {
        // This sets rootExprOrDecl to the module of the "root" compilation,
        // however that doesn'e mean it's the one you are always interested in.
        // This is the reason why the other constructors allow you to specify
        // a different root for the Expression tree.
        Object rootExprOrDecl = compileResult.getCompilation().getModule();
        sharedInit(null, rootExprOrDecl, compileResult);
    }

    public ExprWrap(Object rootExpOrDecl, CompileResult compileResult) {
        sharedInit(null, rootExpOrDecl, compileResult);
    }

    private ExprWrap(Object exprOrDecl, ExprWrap parent) {
        if (parent == null) {
            throw new IllegalArgumentException(
                    "Parent can't be null."
                            + "If you want to build a new tree from a root expression"
                            + "use the other constructor"
            );
        }
        sharedInit(parent, exprOrDecl, parent.getCompileResult());
    }

    public ExprWrap(String codeStr, Language lang, Environment env)
            throws IOException {
        CompileResult cr = Compile.compileToState13(codeStr, lang, env);
        Object rootExprOrDecl = cr.getCompilation().getModule();
        sharedInit(null, rootExprOrDecl, cr);
    }

    public ExprWrap(String codeStr)
            throws IOException {
        Language lang = Language.getDefaultLanguage();
        Environment env = Environment.user();
        CompileResult cr = Compile.compileToState13(codeStr, lang, env);
        Object rootExprOrDecl = cr.getCompilation().getModule();
        sharedInit(null, rootExprOrDecl, cr);
    }

    public Object getExpr() {
        return expr;
    }

    public Optional<ExprWrap> getParent() {
        return parent != null
                ? Optional.of(parent)
                : Optional.empty();
    }

    public List<ExprWrap> getChildren() {
        return children;
    }

    public boolean isRoot() {
        return !this.getParent().isPresent();
    }

    public ExprWrap getRoot() {
        ExprWrap root = null;
        for (ExprWrap w = this; ; w = w.getParent().get()) {
            if (!w.getParent().isPresent()) {
                root = w;
                break;
            }
        }
        return root;
    }

    public CompileResult getCompileResult() {
        return this.compileResult;
    }

    public Optional<ExprWrap> getFirstChild() {
        List<ExprWrap> allChildren = getChildren();
        if (!allChildren.isEmpty()) {
            return Optional.of(allChildren.get(0));
        } else {
            return Optional.empty();
        }
    }

    public Optional<ExprWrap> getLastChild() {
        List<ExprWrap> allChildren = getChildren();
        if (!allChildren.isEmpty()) {
            return Optional.of(allChildren.get(allChildren.size() - 1));
        } else {
            return Optional.empty();
        }
    }

    public List<ExprWrap> getAllSiblings() {
        Optional<ExprWrap> maybeParent = getParent();
        return maybeParent.isPresent()
                ? maybeParent.get().getChildren()
                : Collections.emptyList();
    }

    public List<ExprWrap> getSiblingsBefore() {
        ArrayList<ExprWrap> siblingsBefore = new ArrayList<>();
        for (ExprWrap sibling : getAllSiblings()) {
            if (sibling != this) {
                siblingsBefore.add(sibling);
            } else {
                break;
            }
        }
        return Collections.unmodifiableList(siblingsBefore);
    }

    public Optional<ExprWrap> getPreviousSibling() {
        List<ExprWrap> siblingsBefore = getSiblingsBefore();
        if (!siblingsBefore.isEmpty()) {
            return Optional.of(siblingsBefore.get(siblingsBefore.size() - 1));
        } else {
            return Optional.empty();
        }
    }

    public Optional<ExprWrap> getFirstSibling() {
        List<ExprWrap> siblingsBefore = getSiblingsBefore();
        if (!siblingsBefore.isEmpty()) {
            return Optional.of(siblingsBefore.get(0));
        } else {
            return Optional.empty();
        }
    }

    public List<ExprWrap> getSiblingsAfter() {
        List<ExprWrap> allSiblings = getAllSiblings();
        int indexOfThis = allSiblings.indexOf(this);
        return allSiblings.subList(indexOfThis + 1, allSiblings.size() - 1);
    }

    public Optional<ExprWrap> getNextSibling() {
        List<ExprWrap> siblingsBefore = getSiblingsAfter();
        if (!siblingsBefore.isEmpty()) {
            return Optional.of(siblingsBefore.get(0));
        } else {
            return Optional.empty();
        }
    }

    public Optional<ExprWrap> getLastSibling() {
        List<ExprWrap> siblingsBefore = getSiblingsBefore();
        if (!siblingsBefore.isEmpty()) {
            return Optional.of(siblingsBefore.get(siblingsBefore.size() - 1));
        } else {
            return Optional.empty();
        }
    }

    public int getSiblingIndex() {
        return getSiblingsBefore().size();
    }

    public List<ExprWrap> getDescendants() {
        // Flatten descendants tree into a list
        ArrayList<ExprWrap> descendants = new java.util.ArrayList<>();
        descendants.addAll(getChildren());
        // Dirty (iterating over mutating ArrayList) but compact
        for (int i = 0; i < descendants.size(); i++) {
            descendants.addAll(descendants.get(i).getChildren());
        }
        return descendants;
    }

    public String formatElem(boolean pretty) {
        Optional<ExprWrap> maybeParent = getParent();
        // Sort-of-casting to Optional<Object> to satisfy the formatter method
        Optional<Object> maybeParentAsObj =
                maybeParent.isPresent()
                        ? Optional.of(maybeParent.get())
                        : Optional.empty();
        return ExprUtil.formatElem(getExpr(), maybeParentAsObj, pretty);
    }

    public ValueRange getBoundsOfStrReprInParentStrRepr(boolean useCachedIfPresent) {
        ValueRange bounds;
        Optional<ExprWrap> maybeParent = getParent();
        if (useCachedIfPresent && this.strReprBoundsInParent.isPresent()) {
            // This method can trigger computation in sibling ExprWrap objects.
            // To avoid recomputing the answer every time it caches
            // its result in the `strReprBoundsInParent' field.
            bounds = this.strReprBoundsInParent.get();
        } else if (!maybeParent.isPresent()) {
            // If parent is not there `this' is root
            return ValueRange.of(0, formatElem(false).length() - 1);
        } else {
            // `this' is not root
            List<ExprWrap> prevSiblings = getSiblingsBefore();
            Optional<ExprWrap> maybeLastPrevSibling =
                    prevSiblings.isEmpty()
                            ? Optional.empty()
                            : Optional.of(prevSiblings.get(prevSiblings.size() - 1));
            // We know from previous condition that `maybeParent' is present
            String parentElemStrRepr = maybeParent.get().formatElem(false);
            // This is the index of the parent's string representation
            // from which we start searching from.
            int lookupOffset = Math.toIntExact(
                    maybeLastPrevSibling.map(exprWrap ->
                            exprWrap.getBoundsOfStrReprInParentStrRepr(true)
                                    .getMaximum()
                    ).orElse(0L));
            String parentReprSliceFromOffset = parentElemStrRepr.substring(lookupOffset);
            String thisElemStrRepr = formatElem(false);
            // TODO: Since the whole root repr is in a single
            //       line we can replace indexOf with +1,
            //       accounting for the only whitespace?
            int boundLeft = lookupOffset + parentReprSliceFromOffset.indexOf(thisElemStrRepr);
            int boundRight = boundLeft + thisElemStrRepr.length() - 1;
            bounds = ValueRange.of(boundLeft, boundRight);
            this.strReprBoundsInParent = Optional.of(bounds); // cache result
        }
        return bounds;
    }

    public ValueRange getBoundsOfStrReprInRootStrRepr(boolean useCachedIfPresent) {
        // 1. Get parent's left bound (int) in root's string representation
        // 2. Get this' element's bounds in parent's string representation
        //    and add to both the value we obtained in the previous step
        Optional<ExprWrap> maybeParent = getParent();
        ValueRange bounds;
        if (useCachedIfPresent && this.strReprBoundsInRoot.isPresent()) {
            bounds = this.strReprBoundsInRoot.get();
        } else if (!maybeParent.isPresent()) {
            // If root: no need to add anything to the these bounds
            ValueRange boundsInParent = getBoundsOfStrReprInParentStrRepr(true);
            bounds = boundsInParent;
        } else {
            // If not root:
            ValueRange boundsInParent = getBoundsOfStrReprInParentStrRepr(true);
            int offsetInRoot =
                    Math.toIntExact(
                            boundsInParent.getMinimum()
                                    +
                                    // We know maybeParent is not empty from previous if clause
                                    maybeParent
                                            .get()
                                            .getBoundsOfStrReprInRootStrRepr(true)
                                            .getMinimum());
            long spanLength = boundsInParent.getMaximum() - boundsInParent.getMinimum();
            bounds = ValueRange.of(offsetInRoot, offsetInRoot + spanLength);
            // cache result
            strReprBoundsInRoot = Optional.of(bounds);
        }
        return bounds;
    }
}
