/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.redirect.result;

public class RedirectedOutErr {
    private String out;
    private String err;
    private String outAndErrInPrintOrder;

    public RedirectedOutErr(String out, String err, String outerr) {
        this.out = out;
        this.err = err;
        this.outAndErrInPrintOrder = outerr;
    }

    public String getOut() {
        return out;
    }

    public String getErr() {
        return err;
    }

    public String getOutAndErrInPrintOrder() {
        return outAndErrInPrintOrder;
    }
}
