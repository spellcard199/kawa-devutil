/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.redirect.result;

public class RedirectionResult<T> {
    private T runnableResult;
    private RedirectedOutErr outErr;

    public RedirectionResult(T runnableResult, RedirectedOutErr outErr) {
        this.runnableResult = runnableResult;
        this.outErr = outErr;
    }

    public T getResultOfSupplier() {
        return runnableResult;
    }

    public RedirectedOutErr getOutErr() {
        return outErr;
    }
}
