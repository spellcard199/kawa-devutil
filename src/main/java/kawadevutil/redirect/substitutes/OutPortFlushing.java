/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.redirect.substitutes;

import gnu.kawa.io.OutPort;
import gnu.lists.Consumer;
import gnu.lists.PrintConsumer;

import java.io.Writer;
import java.lang.reflect.Field;

public class OutPortFlushing extends OutPort {

    public OutPortFlushing(Writer base,
                           boolean printPretty) {
        // We are overriding the write method so that `autoflush' has no effect.
        // Passing `true' or `false' shouldn't have any effect here, so this
        // `false' means nothing.
        super(base, printPretty, false);

        PrintConsumer newFormatter = new PrettyWriterFlushing(base, printPretty);
        // "formatter" field in OutPort is private so we use reflection
        // to access it anyway and set our own formatter.
        Field formatterField = null;
        try {
            formatterField =
                    this.getClass().getSuperclass()
                            .getDeclaredField("formatter");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        // On Java 9 we would use:
        // boolean saveAccess = formatterField.canAccess(this);
        boolean saveAccess = formatterField.isAccessible();
        formatterField.setAccessible(true);
        try {
            formatterField.set(this, newFormatter);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            formatterField.setAccessible(saveAccess);
        }
    }

    // I didn't know which methods I needed to override so I've overridden them all.

    @Override
    public void write(int c) {
        super.write(c);
        flush();
    }

    @Override
    public void write(char[] buf) {
        super.write(buf);
        flush();
    }

    @Override
    public void write(char[] str, int start, int count) {
        super.write(str, start, count);
        flush();
    }

    @Override
    public void write(String str) {
        super.write(str);
        flush();
    }

    @Override
    public void write(String str, int start, int count) {
        super.write(str, start, count);
        flush();
    }

    @Override
    public void write(CharSequence str, int start, int count) {
        super.write(str, start, count);
        flush();
    }

    @Override
    public void print(int v) {
        super.print(v);
        flush();
    }

    @Override
    public void print(long v) {
        super.print(v);
        flush();
    }

    @Override
    public void print(double v) {
        super.print(v);
        flush();
    }

    @Override
    public void print(float v) {
        super.print(v);
        flush();
    }

    @Override
    public void print(boolean v) {
        super.print(v);
        flush();
    }

    @Override
    public void print(String v) {
        super.print(v);
        flush();
    }

    @Override
    public void print(Object v) {
        super.print(v);
        flush();
    }

    @Override
    public void print(Consumer out) {
        super.print(out);
        flush();
    }

    @Override
    public void print(char c) {
        super.print(c);
        flush();
    }

    @Override
    public void print(char[] s) {
        super.print(s);
        flush();
    }
}
