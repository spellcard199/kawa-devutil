/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.redirect.substitutes;

import gnu.kawa.io.PrettyWriter;

import java.io.Writer;

public class PrettyWriterFlushing extends PrettyWriter {

    PrettyWriterFlushing(Writer out, boolean prettyPrintingMode) {
        super(out, prettyPrintingMode);
    }

    // I think these methods are the only ones that need
    // to be overridden. The other "write", "print" and "println"
    // methods call these one way or another.

    @Override
    public void write(int ch) {
        super.write(ch);
        flush();
    }

    @Override
    public void write(String str, int start, int count) {
        super.write(str, start, count);
        flush();
    }

    @Override
    public void write(char[] str, int start, int count) {
        super.write(str, start, count);
        flush();
    }

    @Override
    public void println() {
        super.println();
        flush();
    }
}
