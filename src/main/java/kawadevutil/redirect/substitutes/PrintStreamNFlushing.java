/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.redirect.substitutes;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class PrintStreamNFlushing extends java.io.PrintStream {

    // TODO: consider if it's worth to keep track of threads.

    private List<PrintStream> pss;

    private void init(List<PrintStream> printStreams) {
        this.pss = printStreams;
    }

    private PrintStreamNFlushing(List<PrintStream> printStreams) {
        // We are extending PrintStream is so we can use this class
        // where a PrintStream is needed.

        // Pass dummy ByteArrayOutputStream to super: this is actually
        // ignored here, but a PrintStream constructor needs an argument.
        // The drawback is that for this class to work we need to
        // override methods in PrintStream, because the ones in
        // PrintStream would act on the dummy ByteArrayOutputStream.
        super(new ByteArrayOutputStream());
        this.pss = printStreams;
    }

    public static PrintStreamNFlushing
    makeFromBaosList(List<ByteArrayOutputStream> baoses) {
        return new PrintStreamNFlushing(
                baoses.stream()
                        .map(PrintStream::new)
                        .collect(Collectors.toList())
        );
    }

    public static PrintStreamNFlushing
    makeFromPrintStreamList(List<PrintStream> printStreams) {
        return new PrintStreamNFlushing(printStreams);
    }

    // I didn't know which methods I needed to override so
    // I've overridden them all.

    @Override
    public void flush() {
        for (PrintStream ps : pss) {
            ps.flush();
        }
    }

    @Override
    public void close() {
        for (PrintStream ps : pss) {
            ps.close();
        }
    }

    @Override
    public void write(byte[] buf, int off, int len) {
        for (PrintStream ps : pss) {
            ps.write(buf, off, len);
        }
        flush();
    }

    @Override
    public void write(int b) {
        for (PrintStream ps : pss) {
            ps.write(b);
        }
        flush();
    }

    @Override
    public void write(byte[] b) throws IOException {
        for (PrintStream ps : pss) {
            ps.write(b);
        }
        flush();
    }

    @Override
    public void print(Object obj) {
        for (PrintStream ps : pss) {
            ps.print(obj);
        }
        flush();
    }

    @Override
    public void print(String s) {
        for (PrintStream ps : pss) {
            ps.print(s);
        }
        flush();
    }

    @Override
    public void print(char[] s) {
        for (PrintStream ps : pss) {
            ps.print(s);
        }
        flush();
    }

    @Override
    public void print(double d) {
        for (PrintStream ps : pss) {
            ps.print(d);
        }
        flush();
    }

    @Override
    public void print(float f) {
        for (PrintStream ps : pss) {
            ps.print(f);
        }
        flush();
    }

    @Override
    public void print(long l) {
        for (PrintStream ps : pss) {
            ps.print(l);
        }
        flush();
    }

    @Override
    public void print(int i) {
        for (PrintStream ps : pss) {
            ps.print(i);
        }
        flush();
    }

    @Override
    public void print(char c) {
        for (PrintStream ps : pss) {
            ps.print(c);
        }
        flush();
    }

    @Override
    public void print(boolean b) {
        for (PrintStream ps : pss) {
            ps.print(b);
        }
        flush();
    }

    @Override
    public PrintStream append(char c) {
        for (PrintStream ps : pss) {
            ps.append(c);
        }
        flush();
        return this;
    }

    @Override
    public PrintStream append(CharSequence csq) {
        for (PrintStream ps : pss) {
            ps.append(csq);
        }
        flush();
        return this;
    }

    @Override
    public PrintStream append(CharSequence csq, int start, int end) {
        for (PrintStream ps : pss) {
            ps.append(csq, start, end);
        }
        flush();
        return this;
    }

    @Override
    public PrintStream printf(String format, Object... args) {
        for (PrintStream ps : pss) {
            ps.printf(format, args);
        }
        flush();
        return this;
    }

    @Override
    public PrintStream printf(Locale l, String format, Object... args) {
        for (PrintStream ps : pss) {
            ps.printf(l, format, args);
        }
        flush();
        return this;
    }

    // We have to override all these `println' methods because
    // `PrintStream.newLine()' is private and we can't override
    // it directly.

    @Override
    public void println(String line) {
        for (PrintStream ps : pss) {
            ps.println(line);
        }
        flush();
    }

    @Override
    public void println() {
        for (PrintStream ps : pss) {
            ps.println();
        }
        flush();
    }

    @Override
    public void println(boolean x) {
        for (PrintStream ps : pss) {
            ps.println(x);
        }
        flush();
    }

    @Override
    public void println(Object x) {
        for (PrintStream ps : pss) {
            ps.println(x);
        }
        flush();
    }

    @Override
    public void println(double x) {
        for (PrintStream ps : pss) {
            ps.println(x);
        }
        flush();
    }

    @Override
    public void println(char[] x) {
        for (PrintStream ps : pss) {
            ps.println(x);
        }
        flush();
    }

    @Override
    public void println(float x) {
        for (PrintStream ps : pss) {
            ps.println(x);
        }
        flush();
    }

    @Override
    public void println(long x) {
        for (PrintStream ps : pss) {
            ps.println(x);
        }
        flush();
    }

    @Override
    public void println(char x) {
        for (PrintStream ps : pss) {
            ps.println(x);
        }
        flush();
    }

    @Override
    public void println(int x) {
        for (PrintStream ps : pss) {
            ps.println(x);
        }
        flush();
    }
}
