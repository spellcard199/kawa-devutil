/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.redirect.redirectors;

import gnu.kawa.io.OutPort;
import kawadevutil.redirect.substitutes.OutPortFlushing;
import kawadevutil.redirect.substitutes.PrintStreamNFlushing;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.Arrays;

public class RedirectKawaRedirectSystemout<T> extends RedirectKawaAndSystemout<T> {

    private boolean isRedirectOn = false;

    private PrintStream saveJavaOut;
    private PrintStream saveJavaErr;

    protected void
    startRedirecting(boolean prettyPrintOutput) {
        // If this method was called when already redirecting, we
        // would override, and therefore lose, the reference to the
        // previously saved Steams and Ports.
        // If you want to redirect when already redirecting, just
        // instantiate another RedirectKawaRedirectSystemout.
        assertRedirectOff();
        OutPort saveKawaOut = OutPort.outDefault();
        OutPort saveKawaErr = OutPort.errDefault();
        PrintStream saveJavaOut = System.out;
        PrintStream saveJavaErr = System.err;

        // Output is collected here.
        ByteArrayOutputStream outBaos = new ByteArrayOutputStream();
        ByteArrayOutputStream errBaos = new ByteArrayOutputStream();
        ByteArrayOutputStream outErrBaos = new ByteArrayOutputStream();

        // Kawa uses OutPort, java uses PrintStream.
        // The reason we are using PrintStreamNFlushing instead of
        // PrintStream (PrintStreamNFlushing extends the latter) is so that
        // we can at the same time:
        // - keep out and err in the same order they would be printed on a console
        // - not lose the information about what is output and what is err

        // So, java will write/print to these PrintBiStreams...
        PrintStreamNFlushing redirOutPBS = PrintStreamNFlushing.makeFromBaosList(
                Arrays.asList(outBaos, outErrBaos)
        );
        PrintStreamNFlushing redirErrPBS = PrintStreamNFlushing.makeFromBaosList(
                Arrays.asList(errBaos, outErrBaos)
        );
        // ... while Kawa will write/print to these OutPorts, which wrap the
        // same PrintStreamNFlushing that java is going to use.
        // The reason we use OutPortFlushing instead of Kawa's OutPort
        // is because we want to preserve the order in which Kawa and Java print.
        // If we don't do this, Java output may appear before Kawa's even when the
        // code that produces it in Java is after the one that produces it in Kawa.
        OutPortFlushing outPortFlushing = new OutPortFlushing(
                new OutputStreamWriter(redirOutPBS),
                prettyPrintOutput);
        OutPortFlushing errPortFlushing = new OutPortFlushing(
                new OutputStreamWriter(redirErrPBS),
                prettyPrintOutput);

        init(saveKawaOut,
                saveKawaErr,
                outBaos,
                errBaos,
                outErrBaos,
                outPortFlushing,
                errPortFlushing,
                true,
                saveJavaOut,
                saveJavaErr
        );

        OutPort.setOutDefault(this.outPort);
        OutPort.setErrDefault(this.errPort);

        System.setOut(redirOutPBS);
        System.setErr(redirErrPBS);
    }

}
