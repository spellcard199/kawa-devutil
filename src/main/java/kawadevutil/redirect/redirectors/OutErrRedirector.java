/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.redirect.redirectors;

import gnu.kawa.io.OutPort;
import kawadevutil.redirect.result.RedirectedOutErr;
import kawadevutil.redirect.result.RedirectionResult;
import kawadevutil.redirect.substitutes.OutPortFlushing;

import java.io.ByteArrayOutputStream;
import java.util.Objects;
import java.util.function.Supplier;

abstract public class OutErrRedirector<T> {

    protected boolean isRedirectOn;

    protected OutPort saveKawaOut;
    protected OutPort saveKawaErr;

    // These is where redirected output actually gets collected.
    protected ByteArrayOutputStream outBaos;
    protected ByteArrayOutputStream errBaos;
    protected ByteArrayOutputStream outErrBaos;

    // These are used as a connectors between Kawa and the "...Baos"es above
    protected OutPortFlushing outPort;
    protected OutPortFlushing errPort;

    protected void init(OutPort saveKawaOut,
                        OutPort saveKawaErr,
                        ByteArrayOutputStream outBaos,
                        ByteArrayOutputStream errBaos,
                        ByteArrayOutputStream outErrBaos,
                        OutPortFlushing outPortFlushing,
                        OutPortFlushing errPortFlushing,
                        boolean isRedirectOn) {
        // So I don't forget to initialize fields.

        this.saveKawaOut = saveKawaOut;
        this.saveKawaErr = saveKawaErr;

        this.outBaos = outBaos;
        this.errBaos = errBaos;
        this.outErrBaos = outErrBaos;

        this.outPort = outPortFlushing;
        this.errPort = errPortFlushing;

        this.isRedirectOn = isRedirectOn;
        checkForNulls();
    }

    abstract protected void startRedirecting(boolean prettyPrintOutput);

    abstract RedirectedOutErr stopRedirectingAndGetOutErr();

    protected void assertRedirectOff() {
        if (isRedirectOn) {
            throw new java.lang.IllegalStateException(
                    "Can't start redirecting when already doing it.");
        }
    }

    protected void assertRedirectOn() {
        if (!isRedirectOn) {
            throw new java.lang.IllegalStateException(
                    "Can't stop redirecting when not doing it.");
        }
    }

    protected void checkForNulls() {
        Objects.requireNonNull(this.outBaos);
        Objects.requireNonNull(this.outPort);
        Objects.requireNonNull(this.outErrBaos);
        Objects.requireNonNull(this.errBaos);
        Objects.requireNonNull(this.errPort);
        Objects.requireNonNull(this.saveKawaErr);
        Objects.requireNonNull(this.saveKawaErr);
    }

    // Wrapper for starting and stopping redirection.
    // Using this you don't forget to stop redirecting.
    public RedirectionResult<T>
    callSupplierRedirecting(Supplier<T> r,
                            boolean prettyPrintOutput) {
        startRedirecting(prettyPrintOutput);
        T supplierResult = r.get();
        RedirectedOutErr outErr = stopRedirectingAndGetOutErr();
        return new RedirectionResult<T>(supplierResult, outErr);
    }

    protected void flush() {
        outPort.flush();
        errPort.flush();
    }

    protected void close() {
        outPort.close();
        errPort.close();
    }

    protected RedirectedOutErr getRedirectedOutErr() {
        return new RedirectedOutErr(
                outBaos.toString(),
                errBaos.toString(),
                outErrBaos.toString());
    }

}
