/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.redirect.redirectors;

import gnu.kawa.io.OutPort;
import kawadevutil.redirect.result.RedirectedOutErr;
import kawadevutil.redirect.substitutes.OutPortFlushing;
import kawadevutil.redirect.substitutes.PrintStreamNFlushing;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

public class RedirectKawa<T> extends OutErrRedirector<T> {

    @Override
    protected void startRedirecting(boolean prettyPrintOutput) {
        assertRedirectOff();
        OutPort saveKawaOut = OutPort.outDefault();
        OutPort saveKawaErr = OutPort.errDefault();
        ByteArrayOutputStream outBaos = new ByteArrayOutputStream();
        ByteArrayOutputStream errBaos = new ByteArrayOutputStream();
        ByteArrayOutputStream outErrBaos = new ByteArrayOutputStream();
        PrintStreamNFlushing redirOutPBS = PrintStreamNFlushing.makeFromBaosList(
                Arrays.asList(outBaos, outErrBaos)
        );
        PrintStreamNFlushing redirErrPBS = PrintStreamNFlushing.makeFromBaosList(
                Arrays.asList(errBaos, outErrBaos)
        );

        OutPortFlushing outPortFlushing =
                new OutPortFlushing(
                        new java.io.OutputStreamWriter(redirOutPBS),
                        prettyPrintOutput);
        OutPortFlushing errPortFlushing =
                new OutPortFlushing(
                        new java.io.OutputStreamWriter(redirErrPBS),
                        prettyPrintOutput);

        init(saveKawaOut,
                saveKawaErr,
                outBaos,
                errBaos,
                outErrBaos,
                outPortFlushing,
                errPortFlushing,
                true
        );

        OutPort.setOutDefault(this.outPort);
        OutPort.setErrDefault(this.errPort);
    }

    @Override
    RedirectedOutErr stopRedirectingAndGetOutErr() {
        assertRedirectOn();
        OutPort.setOutDefault(this.saveKawaOut);
        OutPort.setErrDefault(this.saveKawaErr);
        flush();
        close();
        this.isRedirectOn = false;
        return getRedirectedOutErr();
    }
}
