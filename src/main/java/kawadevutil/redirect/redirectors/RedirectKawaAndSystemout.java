/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.redirect.redirectors;

import gnu.kawa.io.OutPort;
import kawadevutil.redirect.result.RedirectedOutErr;
import kawadevutil.redirect.substitutes.OutPortFlushing;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Objects;

abstract public class RedirectKawaAndSystemout<T> extends OutErrRedirector<T> {

    private PrintStream saveJavaOut;
    private PrintStream saveJavaErr;

    protected void init(
            OutPort saveKawaOut,
            OutPort saveKawaErr,
            ByteArrayOutputStream outBaos,
            ByteArrayOutputStream errBaos,
            ByteArrayOutputStream outErrBaos,
            OutPortFlushing outPortFlushing,
            OutPortFlushing errPortFlushing,
            boolean isRedirectOn,
            PrintStream saveJavaOut,
            PrintStream saveJavaErr
    ) {
        super.init(saveKawaOut,
                saveKawaErr,
                outBaos,
                errBaos,
                outErrBaos,
                outPortFlushing,
                errPortFlushing,
                isRedirectOn);
        this.saveJavaOut = saveJavaOut;
        this.saveJavaErr = saveJavaErr;
        Objects.requireNonNull(saveJavaOut);
        Objects.requireNonNull(saveJavaErr);
    }

    protected RedirectedOutErr
    stopRedirectingAndGetOutErr() {
        assertRedirectOn();
        OutPort.setOutDefault(saveKawaOut);
        OutPort.setErrDefault(saveKawaErr);
        System.setOut(saveJavaOut);
        System.setErr(saveJavaErr);
        flush();
        close();
        isRedirectOn = false;
        return getRedirectedOutErr();
    }
}
