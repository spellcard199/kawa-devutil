/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.redirect.redirectors;

import gnu.kawa.io.OutPort;
import kawadevutil.redirect.substitutes.OutPortFlushing;
import kawadevutil.redirect.substitutes.PrintStreamNFlushing;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

public class RedirectKawaBidirectSystemout<T> extends RedirectKawaAndSystemout<T> {

    private boolean isRedirectOn = false;

    private PrintStream saveJavaOut;
    private PrintStream saveJavaErr;

    @Override
    protected void startRedirecting(boolean prettyPrintOutput) {
        assertRedirectOff();
        OutPort saveKawaOut = OutPort.outDefault();
        OutPort saveKawaErr = OutPort.errDefault();
        PrintStream saveJavaOut = System.out;
        PrintStream saveJavaErr = System.err;

        // Output is collected here.
        ByteArrayOutputStream outBaos = new ByteArrayOutputStream();
        ByteArrayOutputStream errBaos = new ByteArrayOutputStream();
        ByteArrayOutputStream outErrBaos = new ByteArrayOutputStream();

        // So, java will write/print to these PrintBiStreams...
        PrintStreamNFlushing redirOutPBS = PrintStreamNFlushing.makeFromBaosList(
                Arrays.asList(outBaos, outErrBaos)
        );
        PrintStreamNFlushing redirErrPBS = PrintStreamNFlushing.makeFromBaosList(
                Arrays.asList(errBaos, outErrBaos)
        );

        // Write to both:
        // - our capturing baoses
        // - default System.out
        PrintStreamNFlushing javaOutPBS = PrintStreamNFlushing.makeFromPrintStreamList(
                Arrays.asList(redirOutPBS, saveJavaOut)
        );
        PrintStreamNFlushing javaErrPBS = PrintStreamNFlushing.makeFromPrintStreamList(
                Arrays.asList(redirErrPBS, saveJavaErr)
        );

        OutPortFlushing outPortFlushing = new OutPortFlushing(
                new java.io.OutputStreamWriter(redirOutPBS),
                prettyPrintOutput);
        OutPortFlushing errPortFlushing = new OutPortFlushing(
                new java.io.OutputStreamWriter(redirErrPBS),
                prettyPrintOutput);

        this.init(saveKawaOut,
                saveKawaErr,
                outBaos,
                errBaos,
                outErrBaos,
                outPortFlushing,
                errPortFlushing,
                true,
                saveJavaOut,
                saveJavaErr
        );

        OutPort.setOutDefault(outPort);
        OutPort.setErrDefault(errPort);

        System.setOut(javaOutPBS);
        System.setErr(javaErrPBS);
        this.isRedirectOn = true;

    }

}
