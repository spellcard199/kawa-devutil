/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.data;

import gnu.bytecode.Type;
import gnu.lists.LList;
import gnu.mapping.Symbol;

public class ParamData {

    String name;
    gnu.bytecode.Type type;

    public ParamData(String name, gnu.bytecode.Type type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public LList toLList() {
        return LList.list2(
                LList.list2(Symbol.valueOf("pname:"), getName()),
                LList.list2(Symbol.valueOf("ptype:"), getType())
        );
    }
}
