/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.data;

import gnu.bytecode.Method;
import gnu.lists.LList;
import gnu.mapping.Symbol;
import kawadevutil.kawa.GnuBytecodeMethod;

import java.util.List;
import java.util.stream.Collectors;

public class MethodData {

    private Method gbmethod;
    private String name;
    private Integer modifiers;
    private boolean paramNamesFound;
    private List<ParamData> params;
    private gnu.bytecode.Type returnType;

    public MethodData(Method m) {

        this.gbmethod = m;
        this.name = m.getName();
        this.modifiers = m.getModifiers();
        this.params = GnuBytecodeMethod.getParamData(m);
        this.returnType = m.getReturnType();

    }

    public String getName() {
        return name;
    }

    public Integer getModifiers() {
        return modifiers;
    }

    public boolean getParamNamesFound() {
        return paramNamesFound;
    }

    public List<ParamData> getParams() {
        return params;
    }

    public gnu.bytecode.Type getReturnType() {
        return returnType;
    }

    public gnu.lists.LList
    toLList() {
        LList nameLList = LList.list2(
                Symbol.valueOf("method-name:"),
                getName());
        LList modifiersLList = LList.list2(
                Symbol.valueOf("method-modifiers:"),
                getModifiers());
        LList paramNamesFoundLList = LList.list2(
                Symbol.valueOf("method-param-names-found"),
                getParamNamesFound());
        LList paramsLList = LList.list2(
                Symbol.valueOf("method-params:"),
                getParams()
                        .stream()
                        .map(paramData -> paramData.toLList())
                        .collect(Collectors.toList()));
        LList returnTypeLList = LList.list2(
                Symbol.valueOf("method-return-type:"),
                getReturnType());

        LList[] tmpArray = {
                nameLList, modifiersLList, paramNamesFoundLList,
                paramsLList, returnTypeLList
        };
        return LList.makeList(tmpArray, 0, tmpArray.length);
    }
}
