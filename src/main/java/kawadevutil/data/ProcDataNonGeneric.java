/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.data;

import gnu.bytecode.Method;
import gnu.expr.CompiledProc;
import gnu.expr.PrimProcedure;
import gnu.mapping.*;
import kawadevutil.kawa.GnuBytecodeClassType;
import kawadevutil.kawa.GnuExprCompiledProc;
import kawadevutil.kawa.GnuExprPrimProcedure;
import kawadevutil.kawa.GnuMappingProcedureX;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ProcDataNonGeneric {

    /*
     * This class represents data about procedures of the CompiledProc and PrimProcedure classes.
     * It does _not_ represent data for GenericProc or NamedPart procedures, because data about them:
     * 1. should be a sort of "collection" for other MethodProc procedures
     *    (CompiledProc, PrimProcedure, other GenericProc)
     * 2. is represented by the `ProcDataGeneric' class
     *
     */

    private Procedure proc;
    private List<ParamData> requiredParams;
    private List<ParamData> optionalParams;
    private Optional<ParamData> restParam;
    private boolean variadic;
    private Class module;

    ProcDataNonGeneric(Class module,
                       Procedure proc,
                       List<ParamData> requiredParams,
                       List<ParamData> optionalParams,
                       Optional<ParamData> restParam,
                       boolean variadic) {
        this.module = module;
        this.proc = proc;
        this.requiredParams = requiredParams;
        this.optionalParams = optionalParams;
        this.restParam = restParam;
        this.variadic = variadic;
    }

    public Procedure getProc() {
        return proc;
    }

    public List<ParamData> getRequiredParams() {
        return requiredParams;
    }

    public List<ParamData> getOptionalParams() {
        return optionalParams;
    }

    public Optional<ParamData> getRestParam() {
        return restParam;
    }

    public boolean isVariadic() {
        return variadic;
    }

    public Class getModule() {
        return module;
    }

    public static ProcDataNonGeneric
    makeForPrimProcedure(PrimProcedure proc) {
        Method method = GnuExprPrimProcedure.getMethod(proc);
        Class module = method.getDeclaringClass().getReflectClass();
        MethodData methodData = new MethodData(method);
        List<ParamData> requiredParams = methodData.getParams();
        List<ParamData> optionalParams = Collections.emptyList();
        Optional<ParamData> restParams = Optional.empty();
        boolean isVariadic = false;
        return new ProcDataNonGeneric(
                module, proc, requiredParams,
                optionalParams, restParams, isVariadic);
    }

    public static ProcDataNonGeneric
    makeFromMethods(Procedure proc, Class moduleClass, List<Method> methods) {
        if (methods.size() == 0) {
            return new ProcDataNonGeneric(
                    moduleClass,
                    proc,
                    Collections.emptyList(),
                    Collections.emptyList(),
                    Optional.empty(),
                    false);
        }

        List<MethodData> methodDataList =
                methods.stream()
                        .map(MethodData::new)
                        .collect(java.util.stream.Collectors.toList());

        MethodData mDataContainingParamNames = null;
        for (MethodData mdata : methodDataList) {
            if (mdata.getParamNamesFound()) {
                mDataContainingParamNames = mdata;
            }
        }

        Optional<MethodData> mDataWithMostParams = Optional.empty();
        Optional<MethodData> mDataWithLeastParams = Optional.empty();
        {
            Optional<MethodData> withMostParamsTmp = Optional.empty();
            int maxParams = 0;
            Optional<MethodData> withLeastParamsTmp = Optional.empty();
            int minParams = 9999;
            for (MethodData mdata : methodDataList) {
                int l = mdata.getParams().size();
                if (l > maxParams) {
                    withMostParamsTmp = Optional.of(mdata);
                    maxParams = l;
                }
                if (l < minParams) {
                    withLeastParamsTmp = Optional.of(mdata);
                    minParams = l;
                }
            }
            mDataWithMostParams = withMostParamsTmp;
            mDataWithLeastParams = withLeastParamsTmp;
        }

        {
            // TODO: This can happen. An example is class constructors.
            /*
            if (mDataContainingParamNames != null
                    && mDataContainingParamNames != mDataWithMostParams
            ) {

                throw new Error(
                        "[compiled-proc->autodoc-data] mDataContainingParamNames != mDataWithMostParams");
            }
            */

            // TODO?
            // TODO: fix this in the scheme impl
            if (mDataWithMostParams.isPresent()
                    && mDataWithMostParams.get().getName().endsWith("$V$X")) {
                throw new Error(String.format("TODO: method %s ends with %s",
                        mDataWithMostParams.get().getName(),
                        "$V$X"));
            }
        }

        List<ParamData> allParams = mDataWithMostParams.isPresent()
                ? mDataWithMostParams.get().getParams()
                : Collections.emptyList();
        int requiredParamNumber = mDataWithLeastParams
                .map(methodData -> methodData.getParams().size())
                .orElse(0);

        boolean isThereOptionalParams = allParams.size() != requiredParamNumber;
        boolean isVariadic
                = mDataWithMostParams.isPresent()
                && mDataWithMostParams.get().getName().endsWith("$V");

        List<ParamData> requiredParams;
        if (isThereOptionalParams) {
            requiredParams = allParams.subList(0, requiredParamNumber);
        } else if (isVariadic) {
            requiredParams = allParams.subList(0, allParams.size() - 1);
        } else {
            requiredParams = allParams;
        }

        List<ParamData> optionalParams;
        if (isThereOptionalParams) {
            int to = isVariadic
                    ? allParams.size() - 1
                    : allParams.size();
            optionalParams = allParams.subList(requiredParamNumber, to);
        } else {
            optionalParams = Collections.emptyList();
        }

        Optional<ParamData> restParam = isVariadic
                ? Optional.of(allParams.get(allParams.size() - 1))
                : Optional.empty();

        return new ProcDataNonGeneric(
                moduleClass, proc, requiredParams,
                optionalParams, restParam, isVariadic);
    }

    public static ProcDataNonGeneric
    makeForCompiledProc(CompiledProc proc) {
        return makeFromMethods(
                proc, proc.getModuleClass(),
                GnuExprCompiledProc.getMethods(proc));
    }

    public static ProcDataNonGeneric
    makeForProcedureX(Procedure proc) {
        ProcDataNonGeneric procDataNonGeneric = null;
        if (Procedure0or1.class.isAssignableFrom(proc.getClass())) {
            procDataNonGeneric = makeFromMethods(
                    proc,
                    proc.getClass(),
                    GnuMappingProcedureX.getMethods((Procedure0or1) proc));
        } else if (Procedure1or2.class.isAssignableFrom(proc.getClass())) {
            procDataNonGeneric = makeFromMethods(
                    proc,
                    proc.getClass(),
                    GnuMappingProcedureX.getMethods((Procedure1or2) proc));
        } else if (Procedure0.class.isAssignableFrom(proc.getClass())) {
            procDataNonGeneric = makeFromMethods(
                    proc,
                    proc.getClass(),
                    GnuMappingProcedureX.getMethods((Procedure0) proc));
        } else if (Procedure1.class.isAssignableFrom(proc.getClass())) {
            procDataNonGeneric = makeFromMethods(
                    proc,
                    proc.getClass(),
                    GnuMappingProcedureX.getMethods((Procedure1) proc));
        } else if (Procedure2.class.isAssignableFrom(proc.getClass())) {
            procDataNonGeneric = makeFromMethods(
                    proc,
                    proc.getClass(),
                    GnuMappingProcedureX.getMethods((Procedure2) proc));
        } else if (Procedure3.class.isAssignableFrom(proc.getClass())) {
            procDataNonGeneric = makeFromMethods(
                    proc,
                    proc.getClass(),
                    GnuMappingProcedureX.getMethods((Procedure3) proc));
        } else if (Procedure4.class.isAssignableFrom(proc.getClass())) {
            procDataNonGeneric = makeFromMethods(
                    proc,
                    proc.getClass(),
                    GnuMappingProcedureX.getMethods((Procedure4) proc));
        } else if (ProcedureN.class.isAssignableFrom(proc.getClass())) {
            procDataNonGeneric = makeFromMethods(
                    proc,
                    proc.getClass(),
                    GnuMappingProcedureX.getMethods((ProcedureN) proc));
        } else {
            throw new Error(
                    "FIXME - ProcDataNonGeneric.makeForProcedureX " +
                            "- Bug spotted. This shouldn't happen.");
        }
        return procDataNonGeneric;
    }

    public static ProcDataNonGeneric
    makeVagueProcData(Procedure proc) {

        ArrayList<ParamData> requiredParams = new ArrayList<>();
        for (int i = 0; i < proc.minArgs(); i++) {
            requiredParams.add(
                    new ParamData(
                            "arg" + i,
                            GnuBytecodeClassType.makeFrom(Object.class))
            );
        }
        ArrayList<ParamData> optionalParams = new ArrayList<>();
        if (proc.maxArgs() > 0) {
            for (int i = proc.minArgs(); i < proc.maxArgs(); i++) {
                optionalParams.add(
                        new ParamData(
                                "arg" + i,
                                GnuBytecodeClassType.makeFrom(Object.class)
                        )
                );
            }
        }

        Optional<ParamData> restParam;
        if (proc.maxArgs() < 0) {
            restParam = Optional.of(
                    new ParamData(
                            "#!rest",
                            gnu.bytecode.ArrayType.make(Object[].class)
                    )
            );
        } else {
            restParam = Optional.empty();
        }

        boolean isVariadic = optionalParams.size() > 0 || restParam.isPresent();

        return new ProcDataNonGeneric(
                proc.getClass(), proc, requiredParams,
                optionalParams, restParam, isVariadic
        );
    }

}
