/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.data;

import gnu.bytecode.Method;
import gnu.expr.CompiledProc;
import gnu.expr.GenericProc;
import gnu.expr.PrimProcedure;
import gnu.kawa.functions.NamedPart;
import gnu.mapping.*;
import kawadevutil.kawa.GnuBytecodeClassType;
import kawadevutil.kawa.GnuExprGenericProc;
import kawadevutil.reflect.Reflecting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ProcDataGeneric {

    /*
     *  This class is is just a wrapper for a List<ProcDataNonGeneric>.
     *  The reason this class is called ProcDataGeneric is that, differently
     *  from ProcDataNonGeneric, it can also represent data for:
     *  - procedures of the class GenericProc
     *  - procedures that are superclasses of GenericProc or their subclasses:
     *      - MethodProc: with its subclasses:
     *          - PrimProcedure
     *          - CompiledProc
     *          - GenericProc (it's recursive)
     *      - Procedure: superclass of ProcedureN, superclass of MethodProc
     *  - procedures that may use GenericProc under the hood, like NamedPart
     */

    private List<ProcDataNonGeneric> procDataNonGenericList;
    private Class module;

    public Class getModule() {
        return this.module;
    }

    public List<ProcDataNonGeneric> getProcDataNonGenericList() {
        return this.procDataNonGenericList;
    }

    private ProcDataGeneric(Class module,
                            List<ProcDataNonGeneric> procDataNonGenericList) {
        this.module = module;
        this.procDataNonGenericList = procDataNonGenericList;
    }

    private static boolean isProcedureX(Procedure proc) {
        boolean res = false;
        List<Class<? extends Procedure>> classes = Arrays.asList(
                Procedure0or1.class, Procedure1or2.class,
                Procedure0.class, Procedure1.class, Procedure2.class,
                Procedure3.class, Procedure4.class, ProcedureN.class
        );
        for (Class<? extends Procedure> clz : classes) {
            if (clz.isAssignableFrom(proc.getClass())) {
                res = true;
                break;
            }
        }
        return res;
    }

    public static ProcDataGeneric
    makeForProcedure(Procedure proc) {
        // Procedure is super-super-class of NamedPart, MethodProc.
        // Procedure is our most general case.
        if (NamedPart.class.isAssignableFrom(proc.getClass())) {
            return makeForNamedPart((NamedPart) proc);
        } else if (MethodProc.class.isAssignableFrom(proc.getClass())) {
            return makeForMethodProc((MethodProc) proc);
        } else if (isProcedureX(proc)) {
            // This clause must go last otherwise it would also match the above classes.
            // It should catch Procedure0, Procedure1, ...
            return makeForProcedureX(proc);
        } else {
            // (e.g. gnu.kawa.reflect.Invoke for the "invoke" procedure)
            // It's some other class that extends Procedure from java.
            // Since we don't know which method to search for all we can
            // do is to fallback to:
            // 1. Use just numbers for minArgs and maxArgs
            // 2. Remain vague about parameter names and types
            // I.e.:
            // - Param names: arg0, arg1...
            // - Param types: Object, Object...
            ProcDataNonGeneric vagueProcData
                    = ProcDataNonGeneric.makeVagueProcData(proc);
            return new ProcDataGeneric(
                    proc.getClass(),
                    Collections.singletonList(vagueProcData)
            );
        }
    }

    public static ProcDataGeneric
    makeForNamedPart(NamedPart proc) {
        Reflecting<MethodProc> reflecting = new Reflecting<>();
        try {
            MethodProc methodProc = reflecting.getDeclaredFieldValue(proc, "methods");
            return makeForMethodProc(methodProc);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            // As of today (2020-01-28) this can't happen, since NamedPart:
            // - is accessible
            // - does have a "methods" field
            // It wasn't the case in 2019, but keeping
            // the catch block to silence IDE warnings.
            throw new Error(e);
        }
    }

    public static ProcDataGeneric
    makeForMethodProc(MethodProc proc) {
        // MethodProc is superclass of GenericProc, PrimProcedure, CompiledProc
        Class procClass = proc.getClass();
        if (procClass == GenericProc.class) {
            return makeForGenericProc((GenericProc) proc);

        } else if (procClass == CompiledProc.class) {
            ProcDataNonGeneric compiledProcData
                    = ProcDataNonGeneric.makeForCompiledProc((CompiledProc) proc);
            return new ProcDataGeneric(
                    compiledProcData.getModule(),
                    Collections.singletonList(compiledProcData)
            );

        } else if (procClass == PrimProcedure.class) {
            ProcDataNonGeneric primProcedureData
                    = ProcDataNonGeneric.makeForPrimProcedure((PrimProcedure) proc);
            return new ProcDataGeneric(
                    primProcedureData.getModule(),
                    Collections.singletonList(primProcedureData)
            );

        } else {
            throw new Error(
                    "FIXME - Should not happen:\n"
                            + "- proc: " + proc.toString() + "\n"
                            + "- procClass: " + proc.getClass() + "\n"
            );
        }
    }

    public static ProcDataGeneric
    makeForGenericProc(GenericProc proc) {
        List<MethodProc> methodProcs = GnuExprGenericProc.getMethodProcs(proc);
        List<ProcDataNonGeneric> newList = new ArrayList<>();
        // Basically what we are doing is:
        // 1. "Unwrap" GenericProc into its "sub-procedures", which can be:
        //     - CompiledProc
        //     - PrimProcedure
        //     - GenericProc (this method is recursive via `makeForMethodProc')
        // 2. For each of these "sub-procedures" get a ProcDataGeneric object
        // 3. Flatten all the resulting ProcDataGeneric objects into a new, single one
        for (MethodProc p : methodProcs) {
            ProcDataGeneric pdg = makeForMethodProc(p);
            newList.addAll(pdg.getProcDataNonGenericList());
        }
        // TODO : does it have any sense to get the module of a GenericProc? If yes, how to do it?
        return new ProcDataGeneric(null, newList);
    }

    public static ProcDataGeneric
    makeForProcedureX(Procedure proc) {
        return new ProcDataGeneric(
                proc.getClass(),
                Collections.singletonList(ProcDataNonGeneric.makeForProcedureX(proc))
        );
    }

    public static ProcDataGeneric
    makeForConstructors(Class clz) {
        List<Method> constructorMethods = GnuBytecodeClassType.getMethodsMatchingNames(clz, Collections.singletonList("<init>"));
        List<ProcDataNonGeneric> pdngList =
                constructorMethods
                        .stream()
                        .map(cm -> ProcDataNonGeneric.makeFromMethods(null, clz, Collections.singletonList(cm)))
                        .collect(Collectors.toList());
        return new ProcDataGeneric(clz, pdngList);
    }
}
