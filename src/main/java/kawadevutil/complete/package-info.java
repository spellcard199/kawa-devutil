/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

/**
 *  Functions that try to offer completion from the AST produced by the Kawa compiler.
 */

package kawadevutil.complete;