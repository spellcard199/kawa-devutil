/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.result.abstractdata;

import kawadevutil.complete.find.classmembers.ModifierMask;
import kawadevutil.exprtree.CursorFinder;

import java.util.Optional;

public abstract class CompletionForClassMember
        extends CompletionData {

    /*
     *  Completion for fields and methods share that
     *  we find them by matching on the Expression
     *  tree produced by the Kawa compiler.
     *
     */

    protected CursorFinder cursorFinder;
    protected StackTraceElement[] matcherStackTrace;
    private Class ownerClass;
    private ModifierMask modifierMask;

    public CompletionForClassMember(
            CursorFinder cursorFinder,
            Class ownerClass,
            ModifierMask modifierMask,
            CompletionType completionType,
            StackTraceElement[] matcherStackTrace) {
        super(completionType);
        setCursorFinder(cursorFinder);
        setMatcherStackTrace(matcherStackTrace);
        this.ownerClass = ownerClass;
        this.modifierMask = modifierMask;
    }

    public Class getOwnerClass() {
        return ownerClass;
    }

    public ModifierMask getModifierMask() {
        return modifierMask;
    }

    private void
    setCursorFinder(CursorFinder cursorFinder) {
        this.cursorFinder = cursorFinder;
    }

    public void
    setMatcherStackTrace(StackTraceElement[] matcherStackTrace) {
        this.matcherStackTrace = matcherStackTrace;
    }

    public StackTraceElement[]
    getAstMatcherStackTrace() {
        return matcherStackTrace;
    }

    public Optional<String>
    getAstMatcherMethodName() {
        StackTraceElement[] st = getAstMatcherStackTrace();
        return st != null && st.length > 0
                ? Optional.of(st[1].getMethodName())
                : Optional.empty();
    }

    public CursorFinder
    getCursorFinder() {
        return cursorFinder;
    }

}
