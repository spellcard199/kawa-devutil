/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.result.abstractdata;

import kawadevutil.complete.result.concretedata.CompletionForField;
import kawadevutil.complete.result.concretedata.CompletionForMethod;
import kawadevutil.complete.result.concretedata.CompletionForSymbolAndPackageMember;

import java.util.List;

public abstract class CompletionData {

    /*
     *
     *  Rationale for this hierarchy of classes:
     *  1. Completion for these are mutually exclusive:
     *    - CompletionForMethods
     *    - CompletionForFields
     *    - CompletionForSymbolAndPackageMember
     *  2. For example, you can't put a symbol that doesn't correspond
     *     to a method or field name after colon notation
     *  3. If it's not a Method or a Field, user may want to find
     *     one or more of:
     *    - Symbol name
     *    - Class name
     *    - Package name
     *  4. We could try to guess which between Symbol, Class or Package name
     *     the user wants to find basing on the already typed word prefix
     *     but it may match more than one at the same time. For example, if
     *     the prefix is empty any of the 3 would be plausible.
     *  5. When a non specified completion is asked for the returned type
     *     will be CompletionData (this abstract class), which is the superclass
     *     for each of the 3 mutually exclusive Classes we have seen above.
     *     The reason this superclass exist is so that a developer using this
     *     library can know which are the possible concrete subclasses he may
     *     cast to using the methods in `CastTo'.
     *
     *  Instructions for developers using kawa-devutil:
     *  1. Here at compile time we don't know which of the 3 concrete
     *     subclasses will be returned by generic completion logic.
     *  2. To actually get a concrete CompletionFor<...> object you have to
     *     cast it matching one of the 3 possible values stored in
     *     `completionType'.
     *  3. `CastTo' already implements the 3 possible castings, so that other
     *     developers can explore more easily how things work using an IDE's
     *     completion.
     *  4. Because it would be illegal to cast between them, the 3 concrete
     *     subclasses override the `getCastTo' method so that it returns
     *     Optional.empty().
     *
     */

    private CastTo castTo = new CastTo(this);

    public abstract List<String> getNames();

    public CastTo castTo() {
        return this.castTo;
    }

    public enum CompletionType {METHODS, FIELDS, SYMBOLS_PLUS_PACKAGEMEMBERS}

    protected CompletionType completionType;

    public CompletionData(CompletionType completionType) {
        this.completionType = completionType;
    }

    public CompletionType
    getCompletionType() {
        return completionType;
    }

    public static class CastTo {

        private CompletionData completionData;

        public CastTo(CompletionData completionData) {
            this.completionData = completionData;
        }

        public CompletionForMethod
        complForMethod() {
            return (CompletionForMethod) this.completionData;
        }

        public CompletionForField
        complForField() {
            return (CompletionForField) this.completionData;
        }

        public CompletionForSymbolAndPackageMember
        complForSymAndPackageMember() {
            return (CompletionForSymbolAndPackageMember) this.completionData;
        }

    }
}
