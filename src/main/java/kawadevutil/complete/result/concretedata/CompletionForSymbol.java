/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.result.concretedata;

import gnu.expr.CommandCompleter;

import java.util.List;

public class CompletionForSymbol {

    private CommandCompleter commandCompleter;

    public CompletionForSymbol(CommandCompleter commandCompleter) {
        this.commandCompleter = commandCompleter;
    }

    public CommandCompleter getCommandCompleter() {
        return commandCompleter;
    }

    public List<String> getNames() {
        return this.commandCompleter.candidates;
    }
}
