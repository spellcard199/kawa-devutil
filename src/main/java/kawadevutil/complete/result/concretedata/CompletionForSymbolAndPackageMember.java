/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.result.concretedata;

import gnu.expr.CommandCompleter;
import kawadevutil.complete.find.packagemembers.CompletionFindPackageMember;
import kawadevutil.complete.find.symbols.CompletionFindSymbols;
import kawadevutil.complete.result.abstractdata.CompletionData;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CompletionForSymbolAndPackageMember extends CompletionData {

    /*
     *  This wrapper class exists because when we are not
     *  completing for a Method or a Field, either of the
     *  following may be a valid completion:
     *  - a Symbol
     *  - a Package (it's a member of a Package)
     *  - a Class   (it's a member of a Package)
     *
     *  A couple of examples:
     *  - An user may want to pass as argument to a procedure either:
     *    - a Class
     *    - a Package
     *    - a Symbol
     *  - When an user is looking for a class it's useful to
     *    have completion for parent packages.
     *
     *  Completion for Package and Class names share the fact that
     *  they are both members of a parent Package, so:
     *  1. We use the `ClassGraph' library to get both
     *  2. They have a common superclass that implements the methods
     *     where we use `ClassGraph'
     *
     *  Note that when using the generic completion method,
     *  CompletionFindGeneric.find, and you than need to know which
     *  was the reason CompletionForClassMember didn't succeed...:
     *  - Syntactic error that didn't allow the making of an
     *    Expression tree
     *  - No matchers matching on the Expression tree
     *  ..., you can use couldMakeExprTree.
     *
     *  If couldMakeExprTree is:
     *  - present and true: you know that it's just that the cursor is
     *    just not in a method or field completion position
     *  - present and false: you know that we are completing for Package,
     *    Class or Symbol names because the code wasn't even compilable
     *    into an Expression tree.
     *  - absent: who made the instance of this class didn't check
     *    if the code could be compiled into an Expression tree.
     *
     *  This can be useful for communicating to the user that the
     *  completions returned may not be the ones for methods or fields that
     *  she/he was expecting because there is some (possibly unnoticed)
     *  syntactic error.
     *
     */

    private Boolean couldMakeExprTree;
    private CommandCompleter commandCompleter;
    private CompletionForSymbol completionForSymbol;
    private CompletionForPackageMember completionForPackageMember;

    private void init(CommandCompleter commandCompleter,
                      Boolean couldMakeExprTree) {
        this.couldMakeExprTree = couldMakeExprTree;
        this.commandCompleter = commandCompleter;
        this.completionForSymbol =
                CompletionFindSymbols
                        .find(commandCompleter)
                        .orElse(null);
        this.completionForPackageMember =
                CompletionFindPackageMember
                        .find(commandCompleter)
                        .orElse(null);
    }

    @Override
    public List<String> getNames() {
        ArrayList<String> allNames = new ArrayList<>();
        getCompletionForSymbol().ifPresent(
                (CompletionForSymbol cs) -> allNames.addAll(cs.getNames())
        );
        getCompletionForPackageMember()
                .ifPresent(
                        (CompletionForPackageMember cpm) ->
                                allNames.addAll(cpm.getAllNames()));
        return allNames;
    }

    public CompletionForSymbolAndPackageMember(
            CommandCompleter commandCompleter) {
        super(CompletionType.SYMBOLS_PLUS_PACKAGEMEMBERS);
        this.init(commandCompleter, null);
    }

    public CompletionForSymbolAndPackageMember(
            CommandCompleter commandCompleter,
            Boolean couldMakeExprTree) {
        super(CompletionType.SYMBOLS_PLUS_PACKAGEMEMBERS);
        init(commandCompleter, couldMakeExprTree);
    }

    public Optional<Boolean>
    getCouldMakeExprTree() {
        return couldMakeExprTree != null
                ? Optional.of(couldMakeExprTree)
                : Optional.empty();
    }

    public CommandCompleter getCommandCompleter() {
        return commandCompleter;
    }

    public Optional<CompletionForSymbol> getCompletionForSymbol() {
        return completionForSymbol != null
                ? Optional.of(completionForSymbol)
                : Optional.empty();
    }

    public Optional<CompletionForPackageMember> getCompletionForPackageMember() {
        return completionForPackageMember != null
                ? Optional.of(completionForPackageMember)
                : Optional.empty();
    }
}
