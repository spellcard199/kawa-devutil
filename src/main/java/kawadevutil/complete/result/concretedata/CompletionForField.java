/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.result.concretedata;

import kawadevutil.complete.find.classmembers.CompletionFindClassMemberUtil;
import kawadevutil.complete.find.classmembers.ModifierMask;
import kawadevutil.complete.result.abstractdata.CompletionForClassMember;
import kawadevutil.exprtree.CursorFinder;

import java.lang.reflect.Field;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CompletionForField extends CompletionForClassMember {

    private List<Field> fields;

    public CompletionForField(Class forClass,
                              ModifierMask modifierMask,
                              CursorFinder cursorFinder,
                              Function<Field, Boolean> filterFunc,
                              StackTraceElement[] matcherStackTrace) {
        super(cursorFinder, forClass, modifierMask, CompletionType.FIELDS, matcherStackTrace);
        if (filterFunc == null) {
            filterFunc = (Field field) -> true;    // Default is getting all Fields
        }
        this.fields = CompletionFindClassMemberUtil.findFields(forClass, modifierMask, filterFunc);
    }

    public List<Field> getFields() {
        return fields;
    }

    public List<String>
    getNames() {
        return this.getFields().stream().map(Field::getName).collect(Collectors.toList());
    }

}
