/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.result.concretedata;

import kawadevutil.complete.find.classmembers.CompletionFindClassMemberUtil;
import kawadevutil.complete.find.classmembers.ModifierMask;
import kawadevutil.complete.result.abstractdata.CompletionForClassMember;
import kawadevutil.exprtree.CursorFinder;

import java.lang.reflect.Method;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CompletionForMethod extends CompletionForClassMember {

    private List<Method> methods;

    public CompletionForMethod(Class forClass,
                               ModifierMask modifierMask,
                               CursorFinder cursorFinder,
                               Function<Method, Boolean> filter,
                               StackTraceElement[] matcherStackTrace) {
        super(cursorFinder, forClass,
                modifierMask, CompletionType.METHODS,
                matcherStackTrace);
        if (filter == null) {
            filter = (Method m) -> true;  // Default is getting all methods
        }
        this.methods = CompletionFindClassMemberUtil.findMethods(forClass, modifierMask, filter);
    }

    public List<Method> getMethods() {
        return methods;
    }

    public List<String> getNames() {
        return this.getMethods().stream().map(Method::getName).collect(Collectors.toList());
    }

}
