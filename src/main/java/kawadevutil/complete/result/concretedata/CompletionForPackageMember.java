/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.result.concretedata;

import gnu.expr.CommandCompleter;
import io.github.classgraph.ClassInfoList;
import io.github.classgraph.PackageInfo;
import io.github.classgraph.PackageInfoList;

import java.util.ArrayList;
import java.util.List;

public class CompletionForPackageMember {

    /*
     *  A member of a Package can be either:
     *  - another package
     *  - a class
     *  What they share, and the reason why this abstract class
     *  exists, is that we use the ClassGraph library to get both.
     *
     *  This abstract class implements the methods that use
     *  `ClassGraph' that both share.
     *
     *  Differently from CompletionForClassMember, this is not a
     *  subclass of of CompletionData, since subclasses of this
     *  class, i.e. ...:
     *  - CompletionForPackage
     *  - CompletionForClass
     *  ... are not mutually exclusive and for this reason are
     *  returned both, wrapped by CompletionForSymbolAndPackageMember
     *  (which is a subclass of CompletionData).
     *
     */

    private CommandCompleter commandCompleter;
    private PackageInfo packageInfo;

    public CompletionForPackageMember(
            CommandCompleter commandCompleter,
            PackageInfo packageInfo) {
        this.commandCompleter = commandCompleter;
        this.packageInfo = packageInfo;
    }

    public CommandCompleter getCommandCompleter() {
        return commandCompleter;
    }

    public PackageInfo getPackageInfo() {
        return packageInfo;
    }

    public PackageInfoList getChildPackageInfoList() {
        return packageInfo.getChildren();
    }

    public ClassInfoList getChildClassInfoList() {
        return packageInfo.getClassInfo();
    }

    public List<String> getAllNames() {
        List<String> names = new ArrayList<>();
        names.addAll(this.getChildPackageInfoList().getNames());
        names.addAll(this.getChildClassInfoList().getNames());
        return names;
    }
}
