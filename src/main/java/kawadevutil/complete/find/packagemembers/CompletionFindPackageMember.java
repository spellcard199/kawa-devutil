/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.find.packagemembers;

import gnu.expr.CommandCompleter;
import kawadevutil.complete.result.concretedata.CompletionForPackageMember;

import java.util.Optional;

public class CompletionFindPackageMember {

    public static Optional<CompletionForPackageMember>
    find(CommandCompleter commandCompleter) {
        return CompletionFindPackageMemberUtil
                .getPinfoFromCommandCompleter(commandCompleter)
                .map((pinfo) ->
                        new CompletionForPackageMember(commandCompleter, pinfo)
                );
    }
}
