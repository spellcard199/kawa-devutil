/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.find.packagemembers;

import gnu.expr.CommandCompleter;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.PackageInfo;
import io.github.classgraph.PackageInfoList;
import io.github.classgraph.ScanResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class CompletionFindPackageMemberUtil {
    private static ClassGraph defaultClassGraphInstance;
    private static PackageInfoList cache;
    private static PackageInfo rootCache;

    public static void
    setCache(ScanResult scanResult) {
        cache = scanResult.getPackageInfo();
        rootCache = cache.get("");
    }

    public static Optional<PackageInfo>
    getPinfoFromCommandCompleter(CommandCompleter commandCompleter) {
        String prefix = commandCompleter.word.substring(0, commandCompleter.wordCursor);
        return CompletionFindPackageMemberUtil.getParentPinfoFromPartialName(prefix);
    }

    public static Optional<PackageInfo>
    getParentPinfoFromPartialName(String partialName) {

        Optional<PackageInfo> parentPinfoMaybe = Optional.empty();
        if (partialName.contains(".")) {

            String[] splitted = partialName.split("\\.");

            String packageName;
            if (partialName.endsWith(".")) {
                packageName = partialName.substring(0, partialName.length() - 1);
            } else {
                packageName = String.join(
                        ".",
                        Arrays.copyOfRange(splitted, 0, splitted.length - 1)
                );
            }

            parentPinfoMaybe = CompletionFindPackageMemberUtil.getPackageInfo(packageName);

        } else {

            List<String> childrenNamesOfRoot = CompletionFindPackageMemberUtil.getChildrenNamesOfRoot(false);
            Optional<PackageInfo> rootPinfoMaybe = Optional.empty();
            for (String name : childrenNamesOfRoot) {
                if (name.startsWith(partialName)) {
                    rootPinfoMaybe = Optional.of(CompletionFindPackageMemberUtil.getRootCache(true));
                    break;
                }
            }

            parentPinfoMaybe = rootPinfoMaybe;
            // If neither children of root match, leave Optional.empty() as result
        }

        return parentPinfoMaybe;

    }

    public static PackageInfo
    getRootCache(boolean initIfNull) {
        if (rootCache == null && initIfNull) {
            initCache(true);
        }
        return rootCache;
    }

    public static List<String> getChildrenNamesOfRoot(boolean reloadCache) {
        if (reloadCache) {
            initCache(true);
        }
        PackageInfo rc = getRootCache(true);
        ArrayList<String> childrenNames = new ArrayList<>();
        childrenNames.addAll(rc.getChildren().getNames());
        childrenNames.addAll(rc.getClassInfo().getNames());
        return childrenNames;
    }

    private static void
    initCache(boolean reload) {
        if ((cache == null) || reload) {
            defaultClassGraphInstance = new ClassGraph().enableSystemJarsAndModules().enableClassInfo();
            setCache(defaultClassGraphInstance.scan());
        }
    }

    public static Optional<PackageInfo>
    getPackageInfo(String packageName) {
        initCache(false);
        return getPackageInfo(packageName, cache);
    }

    private static Optional<PackageInfo>
    getPackageInfo(String packageName, PackageInfoList pinfoList) {
        Optional<PackageInfo> pinfoMaybe = Optional.empty();
        for (PackageInfo pinfo : pinfoList) {
            if (pinfo.getName().equals(packageName)) {
                pinfoMaybe = Optional.of(pinfo);
                break;
            }
        }
        return pinfoMaybe;
    }

    static Optional<List<String>>
    getPackageChildrenNames(String packageName) {
        initCache(false);
        return getPackageChildrenNames(packageName, cache);
    }

    static Optional<List<String>>
    getPackageChildrenNames(String packageName, PackageInfoList altCache) {
        Optional<PackageInfo> pinfoMaybe = getPackageInfo(packageName);
        Optional<List<String>> childrenNamesMaybe = Optional.empty();
        if (pinfoMaybe.isPresent()) {
            ArrayList<String> childrenNames = new ArrayList<>();
            childrenNames.addAll(pinfoMaybe.get().getChildren().getNames());
            childrenNames.addAll(pinfoMaybe.get().getClassInfo().getNames());
            childrenNamesMaybe = Optional.of(childrenNames);

        }
        return childrenNamesMaybe;
    }
}
