/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.find;

import gnu.expr.CommandCompleter;
import gnu.expr.Language;
import gnu.mapping.Environment;
import kawadevutil.complete.find.classmembers.CompletionFindClassMember;
import kawadevutil.complete.result.abstractdata.CompletionData;
import kawadevutil.complete.result.concretedata.CompletionForSymbolAndPackageMember;
import kawadevutil.kawa.GnuExprCommandCompleter;

import java.io.IOException;
import java.util.Optional;

public class CompletionFindGeneric {

    public static Optional<CompletionData>
    find(String code, Integer cursorIndex) {
        Language lang = Language.getDefaultLanguage();
        Environment env = Environment.user();
        return find(code, cursorIndex, lang, env);
    }

    public static Optional<CompletionData>
    find(String codeStr, Integer cursorIndex,
         Language lang, Environment env) {
        return find(codeStr, cursorIndex, lang, env, true);
    }

    public static Optional<CompletionData>
    find(String codeStr, Integer cursorIndex,
         Language lang, Environment env,
         boolean tryToFixSyntax) {

        Optional<CompletionData> completionDataMaybe = Optional.empty();

        boolean couldMakeExprTree = false;
        try {

            // map(x -> x) is equivalent to casting content to (CompletionData)
            completionDataMaybe = CompletionFindClassMember.find(
                    codeStr, cursorIndex, lang,
                    env, tryToFixSyntax
            ).map(classMember -> classMember);
            couldMakeExprTree = true;

        } catch (IOException ignored) {
        }

        if (!completionDataMaybe.isPresent()) {

            Optional<CommandCompleter> commandCompleterMaybe =
                    GnuExprCommandCompleter.makeFor(codeStr, cursorIndex, lang);

            if (commandCompleterMaybe.isPresent()) {

                CommandCompleter commandCompleter = commandCompleterMaybe.get();

                CompletionForSymbolAndPackageMember completionForSymbolAndPackageMember
                        = new CompletionForSymbolAndPackageMember(
                        commandCompleter, couldMakeExprTree);

                completionDataMaybe
                        = Optional.of(completionForSymbolAndPackageMember);

            }
        }

        return completionDataMaybe;
    }
}
