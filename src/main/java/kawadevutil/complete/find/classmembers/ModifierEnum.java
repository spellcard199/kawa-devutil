/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.find.classmembers;

public enum ModifierEnum {
    PUBLIC, PROTECTED, PRIVATE, // Access modifiers
    STATIC,                     // Restricts to one instance
    FINAL,                      // Prohibits value modification
    TRANSIENT, VOLATILE         // Field-specific modifiers
}
