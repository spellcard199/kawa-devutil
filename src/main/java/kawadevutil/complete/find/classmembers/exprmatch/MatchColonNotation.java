/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.find.classmembers.exprmatch;

import gnu.expr.Compilation;
import gnu.expr.Language;
import gnu.expr.ReferenceExp;
import kawadevutil.complete.find.classmembers.ModifierEnum;
import kawadevutil.complete.find.classmembers.ModifierMask;
import kawadevutil.complete.result.abstractdata.CompletionForClassMember;
import kawadevutil.complete.result.concretedata.CompletionForField;
import kawadevutil.complete.result.concretedata.CompletionForMethod;
import kawadevutil.exprtree.CursorFinder;
import kawadevutil.exprtree.ExprMatchUtil;
import kawadevutil.exprtree.ExprWrap;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class MatchColonNotation {

    /*
     *  TODO: change names of these matcher methods. What I've called here
     *   "matchStatic" or "matchInstance" are more matchers for the AST kawa
     *   produces for respectively things known at compile time things
     *   not known at compile time. And probably even this is not an adequate
     *   description of what happens.
     */

    public static Optional<CompletionForClassMember>
    matchColonNotationStaticMethod(CursorFinder cursorFinder) {
        /*
         * TODO:
         *  Instead of returning CompletionForMethod, write
         *  new class specifically for colon notation
         *  (CompletionForMethodColonNotation?).
         *  If before the colon comes a class, kawa can also invoke its instance
         *  methods if the first argument is an object instance of the same class
         *   (https://www.gnu.org/software/kawa/Method-operations.html).
         *  Since...:
         *  1. This is a kawa-specific behavior that does not have a java equivalent
         *  2. Just using method data does not communicate valid parameters
         *  ... I think it would be useful to have a different class for colon
         *  notation completion data.
         *  If a user of this library got a CompletionForMethodColonNotation result
         *  she/he would know to consider both cases where:
         *  - after the method name comes an instance of the same class before colon
         *  - after the method name come something else
         *
         */

        // Currently we are just returning all methods (See "TODO" above).
        // https://www.gnu.org/software/kawa/Method-operations.html
        //
        // The reason we don't want to exclude non-static methods is that
        // colon notation, differently from the java dot, can also call
        // instance methods if (both true):
        // 1. what's before colon is a class
        // 2. the first argument of the procedure application is an instance
        //    of the same class
        // Note that class objects themselves are instances of the Class class
        // so you can call instance methods of Class using:
        //   (java.lang.Class:...   class-of-interest)
        // For more info, see Method Operations:

        // There are 2 cases we want to match on.
        // Curious thing is that when I run:
        // - single test: the first case presented
        // - all tests: the second case presented
        // I don't know why Kawa's behavior changes based on how the tests are run.

        // (java.lang.String:__cursor_placeholder__)
        // Can become either one:

        // (Module/<string>/167/ ()
        //   (Apply [tailcall] line:1:1-42 (Ref/2837/applyToArgs#2)
        //     (Apply line:1:2-41
        //       (Quote
        //         ClassType java.lang.Object gnu.kawa.reflect.SlotGet.getSlotValue(boolean,java.lang.Object,java.lang.String,java.lang.String,java.lang.String,java.lang.String,gnu.expr.Language)
        //         ::gnu.expr.PrimProcedure)
        //       (Quote #t ::boolean)
        //       (Quote class java.lang.String ::class)
        //       (Quote "__cursor_placeholder__" ::java.lang.String)
        //       (Quote "__cursor_placeholder__" ::java.lang.String)
        //       (Quote "get__cursor_placeholder__" ::java.lang.String)
        //       (Quote "is__cursor_placeholder__" ::java.lang.String)
        //       (Quote kawa.standard.Scheme@7c18432b ::kawa.standard.Scheme))))

        // (Module/<string>/1/
        //   (Declarations: java.lang.String:__cursor_placeholder__#4/fl:5082b)
        //   (Apply [tailcall] line:1:1-42 (Ref/4/applyToArgs#2)
        //     (Ref/3/java.lang.String:__cursor_placeholder__#4)))


        // "((let ((x java.lang.String)) x):)" can become:

        // (Module/<string>/85/ ()
        //   (Apply [tailcall] line:1:1-56 (Ref/259/applyToArgs#2)
        //     (Apply line:1:2-55
        //       (Quote #<procedure gnu.kawa.functions.GetNamedPart>
        //         ::gnu.kawa.functions.GetNamedPart)
        //       (Let#90 line:1:2-55
        //         ((x#421/fl:8000040040(ignorable)::class =
        //           (Quote class java.lang.String ::class)))
        //         (Quote class java.lang.String ::class))
        //       (Quote __cursor_placeholder__ ::gnu.mapping.SimpleSymbol))))

        ExprWrap cursorWrap = cursorFinder.getCursorMatch().getCursorExprWrap();

        Optional<Class> classOwnerMaybe = Optional.empty();
        if (ExprMatchUtil.isCursorInApplyOfGetSlotValue(cursorFinder)
                && ExprMatchUtil.isGrandParentApplyToArgs(cursorFinder)) {
            classOwnerMaybe = ExprMatchUtil.getClassForGetSlotValue(cursorFinder);
        }

        if (!classOwnerMaybe.isPresent()) {
            Optional<String> symNameMaybe
                    = ExprMatchUtil.getSymbolNameInsideExpr(cursorWrap.getExpr());
            boolean isParentApplyToArgs
                    = ExprMatchUtil.isParentApplyToArgs(cursorWrap);
            if (symNameMaybe.isPresent() && isParentApplyToArgs) {
                try {
                    classOwnerMaybe = Optional.of(
                            Class.forName(symNameMaybe.get().split(":")[0])
                    );
                } catch (ArrayIndexOutOfBoundsException
                        | ClassNotFoundException ignored) {
                }
            }
        }

        // (Module/<string>/1/ (Declarations: :__cursor_placeholder__#379/fl:5082b)
        //   (Let#59 line:1:2-32
        //     ((x#377/fl:40040(ignorable)::class =
        //       (Quote class java.lang.String ::class)))
        //     (Apply [tailcall] line:1:1-56
        //       (Quote #<procedure make> ::gnu.kawa.reflect.Invoke)
        //       (Quote class java.lang.String ::class)
        //       (Ref/148/:__cursor_placeholder__#379))))

        if (!classOwnerMaybe.isPresent()) {
            boolean isParentApply
                    = ExprMatchUtil.isParentApplyExp(cursorFinder);
            boolean isFirstSiblingInvoke
                    = ExprMatchUtil.isFirstSiblingQuotedInvoke(cursorFinder);
            boolean isSecondSiblingAQuotedClass
                    = ExprMatchUtil.isSecondSiblingAQuotedClass(cursorFinder);
            if (isParentApply
                    && isFirstSiblingInvoke
                    && isSecondSiblingAQuotedClass) {
                Optional<Object> valueMaybe
                        = ExprMatchUtil.getSecondSiblingQuotedValue(cursorFinder);
                classOwnerMaybe = valueMaybe
                        .filter(v -> v.getClass().equals(Class.class))
                        .map(v -> (Class) v);
            }
        }

        boolean isMatch = classOwnerMaybe.isPresent();

        if (isMatch) {
            Class ownerClass = classOwnerMaybe.get();
            Function<Method, Boolean> filterFunc;
            if (ownerClass.equals(Class.class)) {
                filterFunc = (Method m) -> true;
            } else {
                filterFunc = (Method m) -> m.getDeclaringClass() != Class.class;
            }
            return Optional.of(
                    new CompletionForMethod(
                            ownerClass,
                            new ModifierMask(
                                    Collections.singletonList(ModifierEnum.PUBLIC),
                                    Collections.emptyList()
                            ),
                            cursorFinder,
                            filterFunc,
                            Thread.currentThread().getStackTrace()
                    ));
        } else {
            return Optional.empty();
        }
    }

    public static Optional<CompletionForClassMember>
    matchColonNotationStaticField(CursorFinder cursorFinder) {

        //
        // (Module/<string>/1/ ()
        // (Apply [tailcall] line:1:1-105
        //   (Quote
        //     ClassType java.lang.Object gnu.kawa.reflect.SlotGet.getSlotValue(boolean,java.lang.Object,java.lang.String,java.lang.String,java.lang.String,java.lang.String,gnu.expr.Language)
        //     ::gnu.expr.PrimProcedure)
        //   (Quote #t ::boolean)
        //   (Quote
        //     class kawadevutil.complete.find.classmembers.CompletionFindClassMember
        //     ::class)
        //   (Quote "__cursor_placeholder__" ::java.lang.String)
        //   (Quote "__cursor_placeholder__" ::java.lang.String)
        //   (Quote "get__cursor_placeholder__" ::java.lang.String)
        //   (Quote "is__cursor_placeholder__" ::java.lang.String)
        //   (Quote kawa.standard.Scheme@17bffc17 ::kawa.standard.Scheme)))
        //
        // Note the (Quote #t ::boolean): a #t argument to
        // gnu.kawa.reflect.SlotGet.getSlotValue means `isStatic'.

        boolean isMatch = ExprMatchUtil.isParentApplyOfSlotGet(
                cursorFinder, true);

        if (isMatch) {
            Class ownerClass = ExprMatchUtil.getClassForGetSlotValue(cursorFinder).get();
            Function<Field, Boolean> filterFunc;
            if (ownerClass.equals(Class.class)) {
                filterFunc = (Field f) -> true;
            } else {
                filterFunc = (Field f) -> f.getDeclaringClass() != Class.class;
            }
            return Optional.of(
                    new CompletionForField(
                            ownerClass,
                            new ModifierMask(
                                    Arrays.asList(ModifierEnum.PUBLIC, ModifierEnum.STATIC),
                                    Collections.emptyList()
                            ),
                            cursorFinder,
                            filterFunc,
                            Thread.currentThread().getStackTrace()
                    ));
        } else {
            return Optional.empty();
        }
    }

    public static Optional<CompletionForClassMember>
    matchColonNotationDynamicallyClassloaded(CursorFinder cursorFinder) {
        Optional<ExprWrap> maybePreviousSibling = cursorFinder.getCursorMatch().getCursorExprWrap().getPreviousSibling();
        boolean isAstMatch =
                ExprMatchUtil.isCursorOnColonNotationInstance(cursorFinder)
                        && ExprMatchUtil.isGrandParentApplyToArgs(cursorFinder)
                        && maybePreviousSibling.isPresent()
                        && (
                        (
                                ExprMatchUtil.isReferenceExp(maybePreviousSibling.get())
                                        && ExprMatchUtil.isReferenceExpValueNull((ReferenceExp) maybePreviousSibling.get().getExpr())
                        )
                        // TODO: consider QuoteExp?
                        // || ....

                );

        Compilation compilation = cursorFinder.getRootExprWrap().getCompileResult().getCompilation();
        Optional<CompletionForClassMember> complData = Optional.empty();
        if (isAstMatch && compilation != null) {

            ReferenceExp previousSiblingElem =
                    (ReferenceExp) cursorFinder
                            .getCursorMatch()
                            .getCursorExprWrap()
                            .getPreviousSibling()
                            .get()
                            .getExpr();
            Language lang = compilation.getLanguage();
            Object value = lang.getEnvironment().get(previousSiblingElem.getBinding().getSymbol());

            if (value != null) {
                Class classToComplete;
                ModifierMask modifierMask;
                if (value.getClass().equals(Class.class)) {
                    classToComplete = (Class) value;
                    modifierMask = new ModifierMask(
                            Arrays.asList(ModifierEnum.PUBLIC, ModifierEnum.STATIC),
                            Collections.emptyList()
                    );
                } else {
                    classToComplete = value.getClass();
                    modifierMask = new ModifierMask(
                            Collections.singletonList(ModifierEnum.PUBLIC),
                            Collections.singletonList(ModifierEnum.STATIC)
                    );
                }
                complData = Optional.of(
                        new CompletionForMethod(
                                classToComplete,
                                modifierMask,
                                cursorFinder,
                                (Method m) -> m.getDeclaringClass() != Class.class,
                                Thread.currentThread().getStackTrace()
                        )
                );
            }
        }
        return complData;
    }

    public static Optional<CompletionForClassMember>
    matchColonNotationInstanceMethod(CursorFinder cursorFinder) {

        // Since the first part of a named part can also be the result
        // of a computation that returns a class, This does not
        // match only instance methods. Name probably will have to be changed.

        boolean case1 = ExprMatchUtil.isCursorOnColonNotationInstance(cursorFinder)
                && ExprMatchUtil.isGrandParentApplyToArgs(cursorFinder);

        // "((let ((x (java.lang.String 'hi))) x):__cursor_placeholder__)"

        // (Module/<string>/1/ (Declarations: :__cursor_placeholder__#379/fl:5082b)
        //   (Let#59 line:1:2-38
        //     ((x#377/fl:40044::java.lang.String =
        //       (Apply => Type java.lang.String line:1:11-33
        //         (Quote Type java.lang.String java.lang.String.valueOf(java.lang.Object)
        //           ::gnu.expr.PrimProcedure)
        //         (Quote hi ::gnu.mapping.SimpleSymbol))))
        //     (Apply [tailcall] line:1:1-62 (Ref/149/applyToArgs#2)
        //       (Ref/148/x#377)
        //       (Ref/150/:__cursor_placeholder__#379))))


        // (Module/<string>/85/ ()
        //   (Apply [tailcall] line:1:1-56 (Ref/259/applyToArgs#2)
        //     (Apply line:1:2-55
        //       (Quote #<procedure gnu.kawa.functions.GetNamedPart>
        //         ::gnu.kawa.functions.GetNamedPart)
        //       (Let#90 line:1:2-55
        //         ((x#421/fl:8000040040(ignorable)::class =
        //           (Quote class java.lang.String ::class)))
        //         (Quote class java.lang.String ::class))
        //       (Quote __cursor_placeholder__ ::gnu.mapping.SimpleSymbol))))

        // (Module/<string>/71/ ()
        //   (Apply [tailcall] line:1:1-62 (Ref/224/applyToArgs#2)
        //     (Apply line:1:2-61
        //       (Quote #<procedure gnu.kawa.functions.GetNamedPart>
        //         ::gnu.kawa.functions.GetNamedPart)
        //       (Let#77 line:1:2-61
        //         ((x#419/fl:8000040042::java.lang.String =
        //           (Apply => Type java.lang.String line:1:11-33
        //             (Quote
        //               Type java.lang.String java.lang.String.valueOf(java.lang.Object)
        //               ::gnu.expr.PrimProcedure)
        //             (Quote hi ::gnu.mapping.SimpleSymbol))))
        //         (Ref/222/x#419))
        //       (Quote __cursor_placeholder__ ::gnu.mapping.SimpleSymbol))))

        ExprWrap cursorWrap = cursorFinder.getCursorMatch().getCursorExprWrap();
        boolean case2 =
                ExprMatchUtil.isParentApplyToArgs(cursorWrap)
                        && cursorWrap.getSiblingIndex() == 2;

        boolean isMatch = case1 || case2;

        // Even if we know at compile time that the result of an expression is
        // of type Class we still don't know which class it is. Object.class
        // is our way to say "it's a class but we don't know which".
        Class ownerClass = null;
        if (isMatch) {
            ownerClass = ExprMatchUtil
                    .getPreviousSiblingTypeReflectClass(cursorFinder)
                    .get();
        }

        if (isMatch && !ownerClass.equals(Class.class)) {


            Class completionForClass =
                    ownerClass == Class.class
                            ? Object.class
                            : ownerClass;

            // ModifierMask is because of Kawa's behavior when there is a class
            // object before the colon. See the "Note" paragraph inside
            // "Part lookup rules" at:
            // https://www.gnu.org/software/kawa/Colon-notation.html

            return Optional.of(
                    new CompletionForMethod(
                            // unchecked get(): if get() didn't work we would be in the other condition of the if
                            completionForClass,
                            new ModifierMask(
                                    Collections.singletonList(ModifierEnum.PUBLIC),
                                    Collections.emptyList()
                            ),
                            cursorFinder,
                            (Method m) -> true,
                            Thread.currentThread().getStackTrace()
                    ));

        } else {
            return Optional.empty();
        }
    }

    public static Optional<CompletionForClassMember>
    matchColonNotationInstanceField(CursorFinder cursorFinder) {
        boolean isMatch
                = ExprMatchUtil.isCursorOnColonNotationInstance(cursorFinder)
                && !ExprMatchUtil.isGrandParentApplyToArgs(cursorFinder);

        if (isMatch) {

            Class ownerClass = ExprMatchUtil.getPreviousSiblingTypeReflectClass(cursorFinder).get();

            // See matchColonNotationInstanceMethod for why we are doing this.
            Class completionForClass = ownerClass != Class.class
                    ? ownerClass
                    : Object.class;
            return Optional.of(
                    new CompletionForField(
                            // unchecked get(): if get() didn't work we would be in
                            // the other condition of the if
                            ExprMatchUtil.getPreviousSiblingTypeReflectClass(cursorFinder).get(),
                            new ModifierMask(
                                    Collections.singletonList(ModifierEnum.PUBLIC),
                                    Collections.emptyList()
                            ),
                            cursorFinder,
                            (Field f) -> true,
                            Thread.currentThread().getStackTrace()
                    ));

        } else {
            return Optional.empty();
        }
    }

    public static Optional<CompletionForClassMember>
    matchColonNotationInstanceMethodDynamicClassloader(CursorFinder cursorFinder) {
        // TODO?
        return Optional.empty();
    }

}
