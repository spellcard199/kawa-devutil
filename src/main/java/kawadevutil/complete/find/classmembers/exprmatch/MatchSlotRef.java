/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.find.classmembers.exprmatch;

import kawadevutil.complete.find.classmembers.ModifierEnum;
import kawadevutil.complete.find.classmembers.ModifierMask;
import kawadevutil.complete.result.abstractdata.CompletionForClassMember;
import kawadevutil.complete.result.concretedata.CompletionForField;
import kawadevutil.exprtree.CursorFinder;
import kawadevutil.exprtree.ExprMatchUtil;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class MatchSlotRef {
    public static Optional<CompletionForClassMember>
    matchField(CursorFinder cursorFinder) {

        boolean isMatch = ExprMatchUtil.isParentApplyOfSlotGet(
                cursorFinder, false);

        if (isMatch) {
            Class ownerClass
                    = ExprMatchUtil
                    .getPreviousSiblingTypeReflectClass(cursorFinder)
                    .orElse(Object.class);
            return Optional.of(
                    new CompletionForField(
                            ownerClass,
                            new ModifierMask(
                                    Collections.singletonList(ModifierEnum.PUBLIC),
                                    Collections.emptyList()),
                            cursorFinder,
                            (java.lang.reflect.Field f) -> true,
                            Thread.currentThread().getStackTrace()
                    )
            );
        } else {
            return Optional.empty();
        }

    }
}
