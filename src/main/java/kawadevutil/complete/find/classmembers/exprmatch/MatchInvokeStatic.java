/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.find.classmembers.exprmatch;

import kawadevutil.complete.find.classmembers.ModifierEnum;
import kawadevutil.complete.find.classmembers.ModifierMask;
import kawadevutil.complete.result.abstractdata.CompletionForClassMember;
import kawadevutil.complete.result.concretedata.CompletionForMethod;
import kawadevutil.exprtree.CursorFinder;
import kawadevutil.exprtree.ExprMatchUtil;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class MatchInvokeStatic {
    public static Optional<CompletionForClassMember>
    matchInvokeStatic(CursorFinder cursorFinder) {

        boolean isPreviousSiblingAClass =
                ExprMatchUtil
                        .getPreviousSiblingTypeReflectClass(cursorFinder)
                        .map(y -> y.equals(Class.class))
                        .orElse(false);

        boolean isMatch =
                ExprMatchUtil.isParentApplyOfInvoke(cursorFinder, true)
                        && isPreviousSiblingAClass;

        if (isMatch) {

            Class complForClass
                    = (Class)    // If this was not a class isMatch would've been false
                    ExprMatchUtil
                            .getPreviousSiblingValueInsideExpr(cursorFinder)
                            .get();    // If this was empty isMatch would've been false

            return Optional.of(
                    new CompletionForMethod(
                            complForClass,
                            new ModifierMask(
                                    Arrays.asList(ModifierEnum.PUBLIC, ModifierEnum.STATIC),
                                    Collections.emptyList()
                            ),
                            cursorFinder,
                            (Method m) -> true,
                            Thread.currentThread().getStackTrace()
                    )
            );
        } else {
            return Optional.empty();
        }
    }
}
