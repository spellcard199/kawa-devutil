/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.find.classmembers.exprmatch;

import kawadevutil.complete.find.classmembers.ModifierEnum;
import kawadevutil.complete.find.classmembers.ModifierMask;
import kawadevutil.complete.result.abstractdata.CompletionForClassMember;
import kawadevutil.complete.result.concretedata.CompletionForMethod;
import kawadevutil.exprtree.CursorFinder;
import kawadevutil.exprtree.ExprMatchUtil;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class MatchInvoke {

    public static Optional<CompletionForClassMember>
    matchInvoke(CursorFinder cursorFinder) {

        boolean isMatch
                = ExprMatchUtil.isParentApplyOfInvoke(cursorFinder, false);

        if (isMatch) {
            Class complForClass
                    = ExprMatchUtil
                    .getPreviousSiblingTypeReflectClass(cursorFinder)
                    .orElse(Object.class);
            return Optional.of(
                    new CompletionForMethod(
                            complForClass,
                            new ModifierMask(
                                    Collections.singletonList(ModifierEnum.PUBLIC),
                                    Collections.emptyList()
                            ),
                            cursorFinder,
                            (Method m) -> true,
                            Thread.currentThread().getStackTrace()
                    )
            );
        } else {
            return Optional.empty();
        }
    }
}
