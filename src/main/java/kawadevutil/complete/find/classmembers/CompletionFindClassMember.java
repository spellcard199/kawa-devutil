/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.find.classmembers;

import gnu.expr.Language;
import gnu.mapping.Environment;
import kawadevutil.complete.find.classmembers.exprmatch.MatchColonNotation;
import kawadevutil.complete.find.classmembers.exprmatch.MatchInvoke;
import kawadevutil.complete.find.classmembers.exprmatch.MatchInvokeStatic;
import kawadevutil.complete.find.classmembers.exprmatch.MatchSlotRef;
import kawadevutil.complete.result.abstractdata.CompletionForClassMember;
import kawadevutil.exprtree.CursorFinder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;

public class CompletionFindClassMember {

    // A "matcher" is a BiFunction that:
    // - takes:
    //     - CursorFinder
    //     - Function filtering names:
    //         - takes: a name (String)
    //         - returns:
    //             - true : if Field/Method should be included in completion data
    //             - false: if Field/Method should be filtered out
    // - returns:
    //     - If AST matches: CompletionData
    //     - If AST does not match: Optional.empty().

    public static
    ArrayList<Function<CursorFinder, Optional<CompletionForClassMember>>>
            defaultMatchers = new ArrayList<>(
            Arrays.asList(
                    MatchColonNotation::matchColonNotationStaticMethod
                    , MatchColonNotation::matchColonNotationStaticField
                    , MatchColonNotation::matchColonNotationDynamicallyClassloaded
                    , MatchColonNotation::matchColonNotationInstanceMethod
                    , MatchColonNotation::matchColonNotationInstanceField
                    , MatchInvokeStatic::matchInvokeStatic
                    , MatchInvoke::matchInvoke
                    , MatchSlotRef::matchField
                    // TODO: delete this when things start working.
                    // , PackageDotNotation::matchPackage
            )
    );

    public static Optional<CompletionForClassMember>
    find(String codeStr, int cursorIndex,
         Language lang, Environment env,
         boolean tryToFixSyntax) throws IOException {

        CursorFinder cursorFinder
                = CursorFinder.make(codeStr, cursorIndex, tryToFixSyntax, lang, env);

        Optional<CompletionForClassMember> completionDataMaybe = Optional.empty();
        for (Function<CursorFinder, Optional<CompletionForClassMember>>
                complMatcher : getDefaultMatchers()) {
            completionDataMaybe = complMatcher.apply(cursorFinder);
            if (completionDataMaybe.isPresent()) {
                break;
            }
        }

        return completionDataMaybe;
    }

    public static ArrayList<Function
            <CursorFinder, Optional<CompletionForClassMember>>>
    getDefaultMatchers() {
        return defaultMatchers;
    }
}
