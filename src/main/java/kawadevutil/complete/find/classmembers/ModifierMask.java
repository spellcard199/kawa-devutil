/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.find.classmembers;

import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.List;

public class ModifierMask {
    /*
     °  Make Modifier Masks more readable.
     */

    private List<ModifierEnum> required;
    private List<ModifierEnum> excluded;

    public ModifierMask(List<ModifierEnum> required, List<ModifierEnum> excluded) {
        this.required = required != null
                ? required
                : Collections.emptyList();
        this.excluded = excluded != null
                ? excluded
                : Collections.emptyList();
    }

    public List<ModifierEnum> getRequired() {
        return required;
    }

    public List<ModifierEnum> getExcluded() {
        return excluded;
    }

    public int modifierEnumToModifierMask(ModifierEnum modEnum) {
        int modifierMask = 0x0;
        if (modEnum.equals(ModifierEnum.PUBLIC)) modifierMask |= Modifier.PUBLIC;
        else if (modEnum.equals(ModifierEnum.PROTECTED)) modifierMask |= Modifier.PROTECTED;
        else if (modEnum.equals(ModifierEnum.PRIVATE)) modifierMask |= Modifier.PRIVATE;
        else if (modEnum.equals(ModifierEnum.STATIC)) modifierMask |= Modifier.STATIC;
        else if (modEnum.equals(ModifierEnum.FINAL)) modifierMask |= Modifier.FINAL;
        else if (modEnum.equals(ModifierEnum.TRANSIENT)) modifierMask |= Modifier.TRANSIENT;
        else if (modEnum.equals(ModifierEnum.VOLATILE)) modifierMask |= Modifier.VOLATILE;
        return modifierMask;
    }

    public static boolean hasModifier(int haystackMask, int needleMask) {
        return (haystackMask & needleMask) == needleMask;
    }

    public boolean matches(int actualModifiersMask) {
        // TODO: this can probably be simplified by knowing how
        //  "masks" work in general.

        boolean requiredModifiersMatch = true;
        for (ModifierEnum reqMod : this.required) {
            int reqMask = modifierEnumToModifierMask(reqMod);
            if (!hasModifier(actualModifiersMask, reqMask)) {
                requiredModifiersMatch = false;
                break;
            }
        }

        boolean excludedModifiersMatch = true;
        for (ModifierEnum excMod : this.excluded) {
            int excMask = modifierEnumToModifierMask(excMod);
            if (hasModifier(actualModifiersMask, excMask)) {
                excludedModifiersMatch = false;
                break;
            }
        }

        return requiredModifiersMatch && excludedModifiersMatch;
    }
}
