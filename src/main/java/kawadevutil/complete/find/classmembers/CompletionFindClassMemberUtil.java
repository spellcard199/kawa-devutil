/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.find.classmembers;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CompletionFindClassMemberUtil {

    public static List<Method>
    findAllMethods(Class clz) {
        ArrayList<Method> allMethods = new ArrayList<>();
        List<Method> allPublicMethods = Arrays.asList(clz.getMethods());
        List<Method> declaredMethods = Arrays.asList(clz.getDeclaredMethods());
        allMethods.addAll(allPublicMethods);
        for (Method declaredMethod : declaredMethods) {
            if (!allMethods.contains(declaredMethod)) {
                allMethods.add(declaredMethod);
            }
        }
        return allMethods;
    }

    public static List<Method>
    findMethods(Class clz,
                ModifierMask modifierMask,
                Function<Method, Boolean> filterFuncMaybe) {

        final Function<Method, Boolean> filterFunc = filterFuncMaybe != null
                ? filterFuncMaybe
                : (Method m) -> true;

        return findAllMethods(clz)
                .stream()
                .filter((Method method) ->
                        modifierMask.matches(method.getModifiers())
                                && (filterFunc.apply(method)))
                .collect(Collectors.toList());
    }

    public static List<Field>
    findAllFields(Class clz) {
        List<Field> allPublicFields = Arrays.asList(clz.getFields());
        Field[] declaredFields = clz.getDeclaredFields();

        ArrayList<Field> allFields = new ArrayList<>(allPublicFields);
        for (Field declaredField : declaredFields) {
            if (!allFields.contains(declaredField)) {
                allFields.add(declaredField);
            }
        }
        return allFields;
    }

    public static List<Field>
    findFields(Class clz,
               ModifierMask modifierMask,
               Function<Field, Boolean> filterFuncMaybe) {

        final Function<Field, Boolean> filterFunc = filterFuncMaybe != null
                ? filterFuncMaybe
                : (Field m) -> true;

        return findAllFields(clz)
                .stream()
                .filter((Field field) ->
                        modifierMask.matches(field.getModifiers())
                                && filterFunc.apply(field))
                .collect(Collectors.toList());
    }
}
