/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.complete.find.symbols;

import gnu.expr.CommandCompleter;
import kawadevutil.complete.result.concretedata.CompletionForSymbol;

import java.util.Optional;

public class CompletionFindSymbols {

    public static Optional<CompletionForSymbol>
    find(CommandCompleter commandCompleter) {
        if (commandCompleter.candidates.size() != 0) {
            return Optional.of(new CompletionForSymbol(commandCompleter));
        } else {
            return Optional.empty();
        }
    }

}
