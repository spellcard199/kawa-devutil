/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.bytecode.ClassType;
import gnu.bytecode.Method;
import gnu.bytecode.Type;

import java.util.ArrayList;

public class GnuExprPrimProcedure {

    public static Method
    getMethod(gnu.expr.PrimProcedure proc) {
        // Adapted from Kawa's code in gnu.expr.PrimProcedure.disassemble

        // Each PrimProcedure corresponds to only 1 method.
        // When a primitive java method has more than one signature Kawa
        // maps it to gnu.expr.GenericProc, which is not a concern of this
        // procedure.

        // The reason for which just doing (proc:findMethods) doesn't work is
        // that we need a ClassType object that has a `code' field
        // populated. `gnu.bytecode.ClassFileInput' in
        // `ClassToClassType' does that, but it means we have to find
        // the Method again in the newly made ClassType object.

        String methodName = proc.getMethod().getName();
        Type[] paramTypes = proc.getMethod().getParameterTypes();
        ClassType classType =
                // Populate ClassType fields
                GnuBytecodeClassType.makeFrom(proc.getDeclaringClass().getReflectClass());

        // ArrayList of superclasses to search the method in
        ArrayList<ClassType> searchClasses = new ArrayList<>();
        for (ClassType curClass = classType;
             curClass != null;
             curClass = curClass.getSuperclass()) {
            searchClasses.add(curClass);
        }

        // Search in the class and its superclasses
        Method gbMethod = null;
        for (int i = 0; gbMethod == null; i++) {
            try {
                gbMethod = searchClasses.get(i).getMethod(methodName, paramTypes);
            } catch (NullPointerException e) {
                // it's not the class we are searching for
            }
        }
        // If the method is declared by a superclass we need to:
        // 1. make another ClassType for the newly found class
        //    (with a populated `code' field)
        // 3. get the wanted gnu.bytecode.Method in the superclass' ClassType
        ClassType declaringClass = gbMethod.getDeclaringClass();
        return (declaringClass == classType)
                ? gbMethod
                :
                GnuBytecodeClassType
                        .makeFrom(declaringClass.getReflectClass())
                        .getMethod(methodName, paramTypes);
    }
}
