/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.bytecode.CodeAttr;
import gnu.bytecode.LocalVarsAttr;
import gnu.bytecode.Type;
import gnu.bytecode.Variable;
import kawadevutil.data.ParamData;

import java.lang.reflect.Modifier;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class GnuBytecodeMethod {

    public static List<ParamData>
    getParamData(gnu.bytecode.Method m) {
        // Adapted from Kawa's code in gnu.bytecode.ClassTypeWriter.

        // This is the best I could do. Still many times param names
        // don't appear as local vars.

        // https://stackoverflow.com/questions/14888209/asm-bytecode-method-parameter-values
        //   int off = (access | Opcodes.ACC_STATIC) == 0 ? 0 : 1;
        //   int opcode = Type.getArgumentTypes(desc)[param + off].getOpcode(Opcodes.IALOAD);
        //   mv.visitVarIns(opcode, param);
        //   // Note (may be useful?): opcode 46: IALOAD

        Type[] paramTypes = m.getParameterTypes();
        Optional<ArrayList<Variable>> maybeLocalVars = getLocalVars(m);
        Optional<ArrayList<String>> maybeParamNames = Optional.empty();

        HashMap<Integer, Variable> offsetToVarMap = new HashMap<>();
        if (maybeLocalVars.isPresent() &&
                maybeLocalVars.get().size() > 0) {
            ArrayList<Variable> localVars = maybeLocalVars.get();
            for (Variable lvar : localVars) {
                offsetToVarMap.put(lvar.offset, lvar);
            }
        }

        ArrayList<ParamData> paramDataList = new ArrayList<>();
        int off = Modifier.isStatic(m.getModifiers()) ? 0 : 1;
        for (int i = 0; i < paramTypes.length; i++) {
            Type pt = paramTypes[i];
            Variable var = offsetToVarMap.get(i + off);
            if (var != null) {
                if (var.getType() != pt) {
                    // This then clause shouldn't happen, but I'm not sure.
                    Logger.getLogger(GnuBytecodeMethod.class.toString())
                            .log(Level.ALL, "FIXME");
                }
                paramDataList.add(new ParamData(var.getName(), pt));
            } else {
                paramDataList.add(new ParamData("arg" + i, pt));
            }
        }

        // System.out.println("-----");
        // System.out.println(m);
        // for (ParamData pd : paramDataList) {
        //     System.out.println(
        //             pd.getName() + ":" + pd.getType()
        //     );
        // }
        return paramDataList;
    }

    public static List<ParamData>
    getParamData(java.lang.reflect.Method m) {
        // Utility function.
        return getParamData(
                GnuBytecodeClassType
                        .makeFrom(m.getDeclaringClass())
                        .getMethod(m)
        );
    }

    public static Optional<LocalVarsAttr>
    getLocalVarsAttr(gnu.bytecode.Method m) {
        // Adapted from Kawa's code in gnu.bytecode.ClassTypeWriter.printAttributes

        // LocalVarsAttr is subclass of gnu.bytecode.Attribute
        LocalVarsAttr lva = null;
        CodeAttr codeAttr = m.getCode();
        if (codeAttr != null) {
            for (gnu.bytecode.Attribute cattr = codeAttr.getAttributes();
                 (cattr != null);
                 cattr = cattr.getNext()) {
                if (cattr.getClass().equals(LocalVarsAttr.class)) {
                    lva = (LocalVarsAttr) cattr;
                    break;
                }
            }
        }
        return lva != null
                ? Optional.of(lva)
                : Optional.empty();
    }

    public static Optional<ArrayList<Variable>>
    getLocalVars(gnu.bytecode.Method m) {
        return getLocalVarsAttr(m)
                .map(GnuBytecodeMethod::getLocalVars);
    }

    public static ArrayList<Variable>
    getLocalVars(LocalVarsAttr lva) {
        // Adapted from Kawa's code in gnu.bytecode.LocalVarsAttr.java.

        // Using a for loop instead of using Collections.list to avoid the
        // "Unchecked assignment" warning (VarEnumerator is not parameterized).
        ArrayList<Variable> vars = new ArrayList<>();
        for (Variable var = lva.allVars().nextVar();
             var != null;
             var = var.nextVar()) {
            vars.add(var);
        }
        return vars;
    }

    // Keeping for remembering I tried this. This is not really useful for us,
    // since rmethod is often null: I haven't found a reliable way to get
    // from a gnu.bytecode.Method to the declaring reflect java.lang.Class.
    // public static java.lang.reflect.Method
    // getReflectMethod(gnu.bytecode.Method m) {
    //     Reflecting<Method> reflectingOnReflectMethod = new Reflecting<Method>();
    //     java.lang.reflect.Method rmethod = null;
    //     try {
    //         rmethod = reflectingOnReflectMethod.getDeclaredFieldValue(m, "rmethod");
    //     } catch (NoSuchFieldException | IllegalAccessException e) {
    //         e.printStackTrace();
    //     }
    //     Objects.requireNonNull(rmethod);
    //     return rmethod;
    // }
    // public static Class
    // getDeclaringReflectClass(gnu.bytecode.Method m) {
    //     return getReflectMethod(m).getDeclaringClass();
    // }


    public static java.lang.reflect.Executable
    getReflectMethod(gnu.bytecode.Method m) throws NoSuchMethodException {
        // Gets a java.lang.reflect.Method from a gnu.bytecode.Method.
        // Second attempt after the one commented above.
        // TODO: fails with java.lang.Byte.<init>().
        //  See the test that's commented out.
        // Problem may be that kawa names constructors ...<init>,
        // while java.lang.reflect wants the same name of the class.
        // However, this does not explain why this method works for
        // constructors of the class java.lang.String while it
        // does not work for constructors of the class java.lang.Byte.
        // Maybe it has to do with the fact that java.lang.Byte has a
        // <clinit> constructor?

        Class rclass = m.getDeclaringClass().getReflectClass();
        String mname = m.getName();
        Type[] paramGBTypes = m.getParameterTypes();
        Class<?>[] paramTypes = Arrays.stream(paramGBTypes)
                .map(Type::getReflectClass)
                .collect(Collectors.toList())
                .toArray(new Class<?>[]{});

        NoSuchMethodException noSuchMethodException;
        try {
            return rclass.getDeclaredMethod(mname, paramTypes);
        } catch (NoSuchMethodException nsme) {
            noSuchMethodException = nsme;
        }
        try {
            return rclass.getMethod(mname, paramTypes);
        } catch (NoSuchMethodException nsme) {
            noSuchMethodException = nsme;
        }

        try {
            System.out.println(m);
            return rclass.getDeclaredConstructor(paramTypes);
        } catch (NoSuchMethodException nsme) {
            noSuchMethodException = nsme;
        }
        try {
            return rclass.getConstructor(paramTypes);
        } catch (NoSuchMethodException nsme) {
            noSuchMethodException = nsme;
        }

        throw noSuchMethodException;
    }
}
