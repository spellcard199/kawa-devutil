/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.bytecode.ClassFileInput;
import gnu.bytecode.ClassType;
import gnu.bytecode.Method;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GnuBytecodeClassType {

    public static gnu.bytecode.ClassType
    makeFrom(Class clazz) {
        // Adapted from Kawa's code in gnu.expr.PrimProcedure.java.
        // slightly changed findMethods disassemble
        // TODO: think about how to handle exception

        // The relevent difference between this method and
        // gnu.bytecode.ClassType.make is that the latter doesn't populate
        // the `code' field in the ClassType object.

        ClassLoader classloader = clazz.getClassLoader() != null
                ? clazz.getClassLoader()
                : ClassLoader.getSystemClassLoader();
        String resourcePath = clazz.getName().replace(".", "/").concat(".class");
        java.io.InputStream resourceIs = classloader.getResourceAsStream(resourcePath);
        if (resourceIs == null) {
            throw new RuntimeException("missing resource ".concat(resourcePath));
        }
        ClassType classType = new ClassType();
        try {
            // Has side effect of pupulating classType fields
            new ClassFileInput(classType, resourceIs);
        } catch (IOException e) {
            throw new Error(e);
        }
        return classType;
    }

    public static List<Method>
    getMethodsMatchingNames(Class clazz, List<String> names) {
        ArrayList<Method> methodsMatchingName = new ArrayList<>();
        for (Method m = makeFrom(clazz).getMethods(); m != null; m = m.getNext()) {
            if (names.contains(m.getName())) {
                methodsMatchingName.add(m);
            }
        }
        return methodsMatchingName;
    }
}
