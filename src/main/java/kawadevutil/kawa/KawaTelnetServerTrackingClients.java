/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.expr.Language;
import kawa.TelnetRepl;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;
import java.util.function.Function;

public class KawaTelnetServerTrackingClients {

    /*
     *  The reason for this class is just to be able to stop a Kawa
     *  Telnet server any time from server side, without having to
     *  wait for all clients to be disconnected.
     *  The only way I found to forcibly interrupt a Kawa server is
     *  to close both the ServerSocket and the clients' Sockets.
     *  That's why we have to keep track of connected clients.
     *
     */

    private ServerSocket serverSocket;
    private ArrayList<Socket> clients = new ArrayList<>();
    private HashMap<Socket, Thread> clientThreads = new HashMap<>();
    private Thread serverThread;
    private Language language;
    private boolean suppressSocketClosedExceptionStackTrace;

    public KawaTelnetServerTrackingClients(
            int port,
            Language lang,
            boolean suppressSocketClosedExceptionStackTrace) throws IOException {
        this.language = lang;
        this.suppressSocketClosedExceptionStackTrace = suppressSocketClosedExceptionStackTrace;
        this.serverSocket = new ServerSocket();
        this.serverSocket.setReuseAddress(true);
        this.serverSocket.bind(new InetSocketAddress(port));
    }

    public void startServer() {
        mkServerThread().start();
    }

    public void stopServer() {
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Socket client : this.clients) {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void waitUntilNextClientConnection() throws InterruptedException {
        for (long n = this.clientThreads.size();
             n == this.clientThreads.values().size(); ) {
            Thread.sleep(5);
        }
    }

    public void waitUntilAllClientsAreDisconnected() throws InterruptedException {
        for (Map.Entry<Socket, Thread> st : this.clientThreads.entrySet()) {
            for (; st.getValue().isAlive(); ) {
                Thread.sleep(5);
            }
        }
    }

    public void waitUntilNextClientDisconnection() throws InterruptedException {
        Function<Collection<Thread>, Integer> countAliveThreads =
                (Collection<Thread> ts) -> {
                    Integer nAliveThreads = 0;
                    for (Thread t : ts) {
                        if (t.isAlive()) {
                            nAliveThreads += 1;
                        }
                    }
                    return nAliveThreads;
                };
        Collection<Thread> threads = this.clientThreads.values();
        Integer nAlive = countAliveThreads.apply(threads);
        for (; ; ) {
            if (countAliveThreads.apply(threads) < nAlive) {
                break;
            } else {
                Thread.sleep(5);
            }
        }
    }

    private boolean isSuppressSocketClosedExceptionStackTrace() {
        return this.suppressSocketClosedExceptionStackTrace;
    }

    private Optional<Socket> accept() throws IOException {
        // If ServerSocket gets closed, client remains null
        Optional<Socket> clientMaybe = Optional.empty();
        try {
            clientMaybe = Optional.of(this.serverSocket.accept());
            this.clients.add(clientMaybe.get());
        } catch (SocketException se) {
            if (!isSuppressSocketClosedExceptionStackTrace()) {
                se.printStackTrace();
            }
        }
        return clientMaybe;
    }

    private Thread mkServerThread() {
        this.serverThread = new Thread(
                () -> {
                    try {
                        serve();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        return this.serverThread;
    }

    private void serve() throws IOException {
        // Adapted from Kawa's code in kawa/repl.java
        System.err.println("Listening on port " + this.serverSocket.getLocalPort());
        for (; !this.serverSocket.isClosed(); ) {
            System.err.print("waiting ... ");
            System.err.flush();
            // accept() is wrapping ServerSocket.accept()
            // For example, it may be empty if serverSocket has been closed.
            Optional<java.net.Socket> clientMaybe = accept();
            if (clientMaybe.isPresent()) {
                Socket client = clientMaybe.get();
                System.err.println("got connection from "
                        + client.getInetAddress()
                        + " port:" + client.getPort());
                Language saveLang = Language.getDefaultLanguage();
                try {
                    Language lang = this.language;
                    Language.setCurrentLanguage(lang);
                    Thread clientThread = TelnetRepl.serve(lang, client);
                    this.clientThreads.put(client, clientThread);
                } catch (SocketException se) {
                    if (!isSuppressSocketClosedExceptionStackTrace()) {
                        se.printStackTrace();
                    }
                } finally {
                    Language.setCurrentLanguage(saveLang);
                }
            }
        }
    }

}
