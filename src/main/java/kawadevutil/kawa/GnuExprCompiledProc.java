/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.bytecode.Method;
import gnu.expr.CompiledProc;

import java.lang.invoke.MethodHandleInfo;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;

public class GnuExprCompiledProc {

    public static java.util.List<Method>
    getMethods(CompiledProc proc) {
        // Adapted from Kawa's code in gnu.expr.PrimProcedure.disassemble.

        // Differently findMethods a PrimProcedure, to support scheme procedures
        // a CompiledProc has to wrap potentially different signatures,

        MethodHandles.Lookup mhLookup = MethodHandles.lookup();
        // Changed because does not always work (eg. `eval' CompiledProc)
        // MethodHandleInfo mhInfo = mhLookup.revealDirect(proc.getApplyToObjectMethod());
        MethodHandleInfo mhInfo = mhLookup.revealDirect(proc.getApplyMethod());
        Class<?> ownerClass = mhInfo.getDeclaringClass();
        String checkMethodName = mhInfo.getName();
        // We need the actual method, not the "...$check" one
        String methodName = checkMethodName.substring(0, checkMethodName.length() - 6);

        // gnu.bytecode.ClassType.make does not work for us,
        // because it does not populate the `code' field
        ArrayList<String> possibleNames = new ArrayList<>();
        possibleNames.add(methodName);
        possibleNames.add(methodName.concat("$V"));
        possibleNames.add(methodName.concat("$X"));
        possibleNames.add(methodName.concat("$V$X"));

        List<Method> methodsMatchingNames
                = GnuBytecodeClassType.getMethodsMatchingNames(ownerClass, possibleNames);
        return methodsMatchingNames;
    }

}
