/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.bytecode.Method;
import gnu.mapping.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GnuMappingProcedureX {

    private static List<Method> getMethods(Procedure proc, List<String> names) {
        return GnuBytecodeClassType.getMethodsMatchingNames(
                proc.getClass(), names);
    }

    private static List<Method> getMethods(Procedure proc, String name) {
        return getMethods(proc, Collections.singletonList(name));
    }

    public static List<Method> getMethods(Procedure0or1 proc) {
        return getMethods(proc, Arrays.asList("apply0", "apply1"));
    }

    public static List<Method> getMethods(Procedure1or2 proc) {
        return getMethods(proc, Arrays.asList("apply1", "apply2"));
    }

    public static List<Method> getMethods(Procedure0 proc) {
        return getMethods(proc, "apply0");
    }

    public static List<Method> getMethods(Procedure1 proc) {
        return getMethods(proc, "apply1");
    }

    public static List<Method> getMethods(Procedure2 proc) {
        return getMethods(proc, "apply2");
    }

    public static List<Method> getMethods(Procedure3 proc) {
        return getMethods(proc, "apply3");
    }

    public static List<Method> getMethods(Procedure4 proc) {
        return getMethods(proc, "apply4");
    }

    public static List<Method> getMethods(ProcedureN proc) {
        return getMethods(proc, "applyN");
    }
}
