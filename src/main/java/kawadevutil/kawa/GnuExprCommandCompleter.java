/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.expr.CommandCompleter;
import gnu.expr.Compilation;
import gnu.expr.Language;
import gnu.kawa.io.CharArrayInPort;
import gnu.text.Lexer;
import gnu.text.SourceMessages;
import gnu.text.SyntaxException;

import java.io.EOFException;
import java.io.IOError;
import java.io.IOException;
import java.util.Optional;

public class GnuExprCommandCompleter {

    public static Optional<CommandCompleter>
    makeFor(String line, int cursor) {
        return makeFor(line, cursor, Language.getDefaultLanguage());
    }

    public static Optional<CommandCompleter>
    makeFor(String line, int cursor, Language lang)
            throws IOError, RuntimeException {

        // Copied and adapted from
        // gnu.kawa.io.JLineInPort.parseForComplete.
        // 2 reasons why it was copied:
        // - We only need the CommandCompleter data.
        // - The default `ant' target that jitpack uses when pulling dependencies
        //   from git repositories doesn't include `gnu.kawa.io.JLineInPort'.

        // Added: this is the data we actually need:
        Optional<CommandCompleter> commandCompleter = Optional.empty();

        int buflen = line.length();
        char[] tbuf = new char[buflen + 1];
        line.getChars(0, cursor, tbuf, 0);
        tbuf[cursor] = CommandCompleter.COMPLETE_REQUEST;
        line.getChars(cursor, buflen, tbuf, cursor + 1);
        CharArrayInPort cin = new CharArrayInPort(tbuf);
        Language saveLang = Language.setSaveCurrent(lang);
        try {
            SourceMessages messages = new SourceMessages();
            Lexer lexer = lang.getLexer(cin, messages);
            lexer.setInteractive(true);
            lexer.setTentative(true);
            Compilation comp =
                    lang.parse(lexer,
                            Language.PARSE_FOR_EVAL | Language.PARSE_INTERACTIVE_MODULE,
                            null);
            lang.resolve(comp);

            // Deleted because we need just `CommandCompleter' data:
            //   return new KawaParsedLine(this, line, cursor, comp);

            // An example where we get up to here could be...
            //   (display "HI")
            // ... where the cursor is inside the double quotes.
        } catch (SyntaxException ex) {
            if (cin.eofSeen())
                // Changed from... :
                //   throw new EOFError(-1, -1, "unexpected end-of-file", "");
                // ... so we don't need to depend on org.jline.reader.reader.EOFError
                throw new IOError(new EOFException("unexpected end-of-file"));
            throw ex;
        } catch (CommandCompleter ex) {
            commandCompleter = Optional.of(ex);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } finally {
            Language.restoreCurrent(saveLang);
        }
        return commandCompleter;
    }

}
