/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.bytecode.Label;
import gnu.bytecode.Scope;
import kawadevutil.reflect.Reflecting;

public class GnuBytecodeVariable {

    public static Integer
    getStartPos(gnu.bytecode.Variable var) {
        // Adapted from Kawa's code in gnu.bytecode.ClassFileInput.
        Scope scope = var.getScope();
        Integer startPosition = null;
        if (scope != null) {
            try {
                gnu.bytecode.Label startLabel = new Reflecting<Label>()
                        .getDeclaredFieldValue(scope, "start");
                startPosition = new Reflecting<Integer>()
                        .getDeclaredFieldValue(startLabel, "position");
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
                // NoSuchFieldException can only happen if the above code
                // contains an error. In that case we should correct it.
                throw new Error(e);
            } catch (IllegalAccessException e) {
                throw new Error(e);
            }
        }
        return startPosition;
    }
}
