/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.mapping.Environment;
import gnu.mapping.InheritingEnvironment;

import java.util.ArrayList;
import java.util.List;

public class GnuMappingEnvironment {

    public static List<Environment>
    getAncestorsRecursively(Environment env) {
        ArrayList<Environment> envs = new ArrayList<Environment>();
        envs.add(env);
        for (int i = 0; i < envs.size(); i++) {
            Environment e = envs.get(i);
            if (InheritingEnvironment.class.isAssignableFrom(e.getClass())) {
                InheritingEnvironment ie = (InheritingEnvironment) e;
                int numParents = ie.getNumParents();
                for (int pi = 0; pi < numParents; pi++) {
                    envs.add(ie.getParent(pi));
                }
            }
        }
        return envs;
    }

}
