/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.lists.LList;

import java.util.Arrays;

public class GnuMappingLocation {

    public static String baseLocationToModuleName(gnu.mapping.Location location) {
        // TODO: testme

        // TODO: if possible, rewrite this without resorting to hacky string
        //       manipulations.

        // We want to get:
        // 1. findMethods: gnu.mapping.PlainLocation[some.thing.here]
        // 2. to  : "(some thing here)"
        // Instead of PlainLocation we may also have:
        // - gnu.kawa.reflect.FieldLocation
        // - gnu.mapping.SharedLocation

        // It sounds strange to me that there isn't a better way, but I
        // wasn't able to find a different way than resorting to string
        // manipulation.

        String locAsStr = location.toString();

        // from: "gnu.mapping.PlainLocation[some.thing.here]"
        // to  : "gnu.mapping.PlainLocation[some.thing.here"
        String removingTrailingSquareBracket =
                locAsStr.substring(0, locAsStr.length() - 1);

        // from: "gnu.mapping.PlainLocation[some.thing.here"
        // to  : "some.thing.here"
        String removingLeadingLocationTxt;
        {
            String[] splitted = removingTrailingSquareBracket.split("\\[");
            // get the 2 nd element: the first one should be "...GnuMappingLocation["
            removingLeadingLocationTxt = String.join(
                    "",
                    Arrays.copyOfRange(splitted, 1, splitted.length)
            );
        }

        // from: "some.thing.here"
        // to  : ("some" "thing" "here")
        // "here" is the symbol, but we want just the "module"
        // containing it (which in kawa is a java class under the hood).
        LList moduleStrLst;
        {
            String[] splitted = removingLeadingLocationTxt.split("\\.");
            String[] splittedOnlyModule = Arrays.copyOfRange(splitted, 0, splitted.length - 1);
            moduleStrLst = LList.makeList(splittedOnlyModule, 0);
        }

        // from: ("some" "thing" "here")
        // to  : "(some thing here)"
        return gnu.kawa.functions.Format.format("~a", moduleStrLst).toString();
    }
}
