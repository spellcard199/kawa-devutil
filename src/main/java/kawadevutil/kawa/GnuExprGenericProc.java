/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.bytecode.Method;
import gnu.mapping.MethodProc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GnuExprGenericProc {

    public static List<gnu.mapping.MethodProc>
    getMethodProcs(gnu.expr.GenericProc proc) {
        // Adapted from Kawa's code in gnu.expr.PrimProcedure.getMethodFor
        ArrayList<MethodProc> methodProcs = new ArrayList<>();
        for (int i = 0; i < proc.getMethodCount(); i++) {
            methodProcs.add(proc.getMethod(i));
        }
        return methodProcs;
    }

    public static HashMap<MethodProc, Method>
    getProcToMethodMap(gnu.expr.GenericProc proc) {
        // TODO
        return null;
    }

    public static HashMap<MethodProc, List<MethodProc>>
    getProcToProcsMap(gnu.expr.GenericProc proc) {
        // TODO : Return an hashmap representing a tree of GenericProc(s), in case they call other GenericProc(s)
        return null;
    }


}
