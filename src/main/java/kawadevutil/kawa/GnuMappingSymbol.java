/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.expr.*;
import gnu.mapping.Environment;
import gnu.mapping.Symbol;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class GnuMappingSymbol {

    public static Optional<Declaration>
    getDeclaration(Symbol sym, Language lang, Environment startingEnv) {
        // Unused
        // Should be a better way to go from sym to decl, however,
        // this doesn't work because the NameLookup for the
        // builtin-environment in the private field
        // `gnu.expr.NameLookup:instanceMap' is not pupulated.
        // Maybe the reason is that builtins (eg. display, cdddr, ...)
        // have not been "Declared" but are loaded as compiled java.
        List<Environment> envsToSearchInto = GnuMappingEnvironment.getAncestorsRecursively(startingEnv);

        Optional<Declaration> decl = Optional.empty();
        for (Environment env : envsToSearchInto) {

            NameLookup nl = NameLookup.getInstance(env, lang);
            Declaration declNullable = nl.get(sym);
            // System.out.println(nl.get(sym));
            // System.out.println(nl.lookup(sym, Language.FUNCTION_NAMESPACE));
            // System.out.println(nl.lookup(sym, Language.VALUE_NAMESPACE));

            if (declNullable != null) {
                decl = Optional.of(declNullable);
                break;
            }
        }
        return decl;
    }

    public static gnu.bytecode.Type
    getDeclaredType(String symName, Language lang, Environment env) throws IOException {
        // TODO:

        Expression expr;
        try {
            expr = kawadevutil.compile.Compile
                    .compileToState13(symName, lang, env)
                    .getCompilation()
                    .getModule()
                    .body;
        } catch (java.lang.NullPointerException e) {
            String codeToCompile = "(let ((x " + symName + ")) x)";
            LetExp letExp = (LetExp) kawadevutil.compile.Compile
                    .compileToState13(codeToCompile, lang, env)
                    .getCompilation()
                    .getModule()
                    .body;
            expr = letExp.getBody();
        }

        Class exprClass = expr.getClass();

        gnu.bytecode.Type declType;
        if (exprClass == ReferenceExp.class) {
            declType = ((ReferenceExp) expr).getBinding().getType();
        } else if (exprClass == QuoteExp.class) {
            declType = expr.getType();
        } else if (ApplyExp.class.isAssignableFrom(exprClass)) {
            declType = expr.getType();
        } else {
            throw new Error(
                    java.lang.String.format(
                            "[DBG - sym-name->declared-type]\n"
                                    + "unexpected condition.\n"
                                    + "- sym-name: %s\n"
                                    + "- expr    : %s\n",
                            symName,
                            expr
                    ));
        }
        return declType;
    }
}
