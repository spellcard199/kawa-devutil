/*
 * Copyright (C) 2020 spellcard199 <spellcard199@protonmail.com>
 * This is free software;  for terms and warranty disclaimer see ./COPYING.
 *
 */

package kawadevutil.kawa;

import gnu.expr.Language;
import kawa.standard.Scheme;

import java.util.Optional;

public class GnuKawaFunctionsFormat {
    public static String formatSafely(Object... args) {
        Optional<Language> saveLang;
        if (Language.getDefaultLanguage() == null) {
            Scheme scheme = new Scheme();
            saveLang = Optional.of(Language.setSaveCurrent(scheme));
        } else {
            saveLang = Optional.empty();
        }

        String formatted;
        try {
            formatted = gnu.kawa.functions.Format.format(args).toString();
        } finally {
            saveLang.ifPresent(Language::restoreCurrent);
        }
        return formatted;
    }
}
